package com.sxt.mc2.database.base;

import com.sxt.mc.util.task.CallEx;
import com.sxt.mc.util.type.Builder;

public abstract class DatabaseCall<B extends DatabaseCall, C extends AutoCloseable, D extends Database<D, ?, C>> implements Runnable, CallEx<C, Exception>, Builder<B> {

    private String name;

    public DatabaseCall(String name) {
        this.name = name;
    }

    public DatabaseCall() {
        this.name = thisClass().getClass().getSimpleName().replace("Call", "");
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void run() {
        try (C data = database().resource()) {
            call(data);
        } catch (Exception e) {
            fail(e);
            e.printStackTrace();
        } finally {
            try {
                onFinally();
            } catch (Exception ignored) {
            }
        }
    }

    public abstract D database();

    public B go() {
        database().execute(this);
        return thisClass();
    }

    public abstract void onFinally() throws Exception;
}
