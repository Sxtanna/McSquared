package com.sxt.mc.base;

import com.sxt.mc.util.type.Builder;
import com.sxt.mc.util.type.MaterialTypeData;

public class MaterialData<M extends MaterialData, D extends MaterialTypeData> implements Builder<M> {

    private final Material type;
    private int data;

    public MaterialData(Material type, int data) {
        this.type = type;
        this.data = data;
    }

    public MaterialData(Material material) {
        this(material, 0);
    }

    public MaterialData(Material material, D data) {
        this(material, data.data());
    }

    public Material type() {
        return type;
    }

    public int data() {
        return data;
    }

    public M Data(int data) {
        this.data = data;
        return thisClass();
    }

    public M Data(D data) {
        return Data(data.data());
    }
}
