package com.sxt.mc.server;

import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.base.Attributes;
import com.sxt.mc.server.base.Observable;
import com.sxt.mc.server.base.Observer;
import com.sxt.mc.server.components.Transform;
import com.sxt.mc.util.F;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc.util.uU.ReflectionU;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Main Holder for Game Objects
 */
public class GameObject implements Observer, Observable, Logging  {

    private String name;

    private final List<Observer> Observers = new ArrayList<>();
    private final List<Component> Components = new ArrayList<>();

    private final Attributes Attributes = new Attributes();

    public GameObject() {
        Add(Transform.class);
    }

    @Override
    public String name() {
        return name;
    }

    public GameObject Name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public void observe(Packet packet) {
        observers().forEach(observer -> {
            info("Observing " + packet.name() + " with " + observer.getClass().getSimpleName());
            observer.observe(packet);
        });
    }

    @Override
    public void start(Observer observer) {
        Observers.add(observer);
        observables().forEach(observable -> observable.start(observer));
    }

    @Override
    public void stop(Observer observer) {
        Observers.remove(observer);
        observables().forEach(observable -> observable.stop(observer));
    }

    @Override
    public Attributes attributes() {
        return Attributes;
    }

    @Override
    public List<Observer> observers() {
        return Observers;
    }

    public List<Component> components() {
        return Components;
    }

    public <C extends Component> C Add(Class<C> cClass, Object... args) {
        C component = ReflectionU.newInstance(cClass, args);
        if (component == null) throw new RuntimeException("Component " + cClass.getSimpleName() + " is null");
        else {
            component.GameObject(this);
            component.init();

            Components.add(component);
            Observers.forEach(component::start);
        }
        return component;
    }

    public void Remove(Component component) {
        if (Components.remove(component)) Observers.forEach(component::stop);
    }

    public <C extends Component> C component(Class<C> cClass) {
        for (Component component : Components) if (cClass.isInstance(component)) return cClass.cast(component);
        return null;
    }

    public Transform transform() {
        return component(Transform.class);
    }

    public GameObject parent() {
        Transform transform = transform();
        if (transform == null) return null;

        Transform parent = transform.parent();
        if (parent == null) return null;

        return parent.GameObject;
    }

    public GameObject Parent(GameObject gameObject) {
        Transform transform = transform();
        if (transform == null) throw new NullPointerException("Transform is null");

        Transform newParent = gameObject == null ? null : gameObject.transform();

        transform.Parent(newParent);
        return this;
    }

    public GameObject root() {
        Transform transform = transform();
        if (transform == null) return null;

        return transform.root().GameObject;
    }

    public List<Observable> observables() {
        return F.mapFilter(Components, component -> component instanceof Observable, component -> ((Observable) component)).collect(Collectors.toList());
    }

    public boolean hasObserver(Observer observer) {
        return Observers.contains(observer);
    }

    public void clearComponents() {
        Components.forEach(this::Remove);
    }

    public void clearObservers() {
        Observers.forEach(this::stop);
    }

    public void forEachComponent(Consumer<? super Component> consumer) {
        Components.forEach(consumer);
    }

    public void forEachObserver(Consumer<? super Observer> consumer) {
        Observers.forEach(consumer);
    }

    public void forEachChild(Consumer<GameObject> consumer) {
        final Transform transform = transform();
        if (transform != null) {
            for (Transform child : transform.Children()) consumer.accept(child.GameObject);
        }
    }
}
