package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntityLookMove;

@PacketInfo(target = Target.Client, state = State.Play, id = 23, codec = CodecEntityLookMove.class)
public class PacketEntityLookMove extends Packet {

    private int entityId;
    private double deltaX, deltaY, deltaZ, yaw, pitch;
    private boolean onGround;

    public PacketEntityLookMove(int entityId, double dX, double dY, double dZ, double yaw, double pitch, boolean onGround) {
        this.entityId = entityId;
        this.deltaX = dX;
        this.deltaY = dY;
        this.deltaZ = dZ;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    public PacketEntityLookMove() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketEntityLookMove EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public double deltaX() {
        return deltaX;
    }

    public PacketEntityLookMove DeltaX(double deltaX) {
        this.deltaX = deltaX;
        return this;
    }

    public double deltaY() {
        return deltaY;
    }

    public PacketEntityLookMove DeltaY(double deltaY) {
        this.deltaY = deltaY;
        return this;
    }

    public double deltaZ() {
        return deltaZ;
    }

    public PacketEntityLookMove DeltaZ(double deltaZ) {
        this.deltaZ = deltaZ;
        return this;
    }

    public double yaw() {
        return yaw;
    }

    public PacketEntityLookMove Yaw(double yaw) {
        this.yaw = yaw;
        return this;
    }

    public double pitch() {
        return pitch;
    }

    public PacketEntityLookMove Pitch(double pitch) {
        this.pitch = pitch;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketEntityLookMove OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }

    public PacketEntityLookMove Delta(double dX, double dY, double dZ) {
        return DeltaX(dX).DeltaY(dY).DeltaZ(dZ);
    }

    public PacketEntityLookMove Rotation(double yaw, double pitch) {
        return Yaw(yaw).Pitch(pitch);
    }
}
