package com.sxt.mc.base;

import com.sxt.mc.util.type.MaterialTypeData;

public enum DyeColor implements MaterialTypeData<DyeColor> {

    White   (0, 15),
    Orange  (1, 14),
    Magenta (2, 13),
    Aqua    (3, 12),
    Yellow  (4, 11),
    Lime    (5, 10),
    Pink    (6, 9),
    Gray    (7, 8),
    Silver  (8, 7),
    Cyan    (9, 6),
    Purple  (10, 5),
    Blue    (11, 4),
    Brown   (12, 3),
    Green   (13, 2),
    Red     (14, 1),
    Black   (15, 0);

    private final byte woolData, dyeData;

    DyeColor(int woolData, int dyeData) {
        this.woolData = (byte) woolData;
        this.dyeData = (byte) dyeData;
    }

    public byte woolData() {
        return woolData;
    }

    public byte dyeData() {
        return dyeData;
    }

    @Override
    public int data() {
        return woolData;
    }

    @Override
    public DyeColor byData(int data) {
        if (data < 0 || data > 15) return White;
        return values()[data];
    }
}
