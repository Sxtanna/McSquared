package com.sxt.mc.server.entity;

import com.sxt.mc.base.ChatPosition;
import com.sxt.mc.base.Gamemode;
import com.sxt.mc.base.PlayerInfoAction;
import com.sxt.mc.base.chat.BaseComponent;
import com.sxt.mc.base.chat.TextComponent;
import com.sxt.mc.network.handlers.PlayHandler;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.GameObject;
import com.sxt.mc.server.base.AttributeKey;
import com.sxt.mc.server.base.Observer;
import com.sxt.mc.server.components.Transform;
import com.sxt.mc.session.profile.Profile;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.packet.play.PacketChatMessage;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntity;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityDestroy;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityHeadLook;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityLookMove;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityMove;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityTeleport;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerInfo;
import com.sxt.mc.v1_8.packet.play.player.PacketSpawnPlayer;

public class Player extends LivingEntity<Player> {

    public static final AttributeKey<GameObject> PlayerAttribute = AttributeKey.create();

    private Profile profile;
    private Gamemode gamemode = Gamemode.Creative;


    public Profile profile() {
        return profile;
    }

    public Player Profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public Gamemode gamemode() {
        return gamemode;
    }

    public Player Gamemode(Gamemode gamemode) {
        this.gamemode = gamemode;
        return this;
    }

    @Override
    public void init() {
        metadata().Byte(10, 255);
    }

    @Override
    public void update() {
        Transform transform = GameObject.transform();
        Vector pos = transform.Position(), delta = pos.subtract(prevPosition());

        double deltaX = delta.x(), deltaY = delta.y(), deltaZ = delta.z();

        Quaternion rotation = transform.Rotation();
        Vector euler = rotation.euler();

        double pitch = euler.x(), yaw = euler.y(), headYaw = yaw;

        boolean relative = Math.min(Math.min(deltaX, deltaY), deltaZ) >= -4 && Math.max(Math.max(deltaX, deltaY), deltaY) <= 4;
        boolean look = prevPitch() != pitch || prevYaw() != yaw, headLook = prevHeadYaw() != headYaw;
        boolean idle = (deltaX == 0 && deltaY == 0 && deltaZ == 0 && !look), onGround = true;

        if (nextTick-- == 0) {
            nextTick = PlayHandler.Tps;

            look = true;
            idle = relative = false;

        }

        if (idle) {
            GameObject.observe(new PacketEntity(this));
        }
        else if (relative) {
            GameObject.observe(look ? new PacketEntityLookMove(entityId(), deltaX, deltaY, deltaZ, yaw, pitch, onGround) : new PacketEntityMove(entityId(), deltaX, deltaY, deltaZ, onGround));

        }
        else {
            GameObject.observe(new PacketEntityTeleport(entityId(), pos.x(), pos.y(), pos.z(), yaw, pitch, onGround));
        }

        if (headLook) GameObject.observe(new PacketEntityHeadLook(entityId(), headYaw));

        prevPosition().Set(pos);
        PrevPitch(pitch);
        PrevYaw(yaw);
        PrevHeadYaw(headYaw);

        //info("Update called");
    }

    @Override
    public String name() {
        return profile == null ? super.name() : profile.name();
    }

    @Override
    public void start(Observer observer) {
        PacketPlayerInfo packetPlayerInfo = new PacketPlayerInfo(PlayerInfoAction.Add, this);
        PacketSpawnPlayer packetSpawnPlayer = new PacketSpawnPlayer(this);

        observer.observe(packetPlayerInfo);
        observer.observe(packetSpawnPlayer);
    }

    @Override
    public void stop(Observer observer) {
        PacketPlayerInfo packetPlayerInfo = new PacketPlayerInfo(PlayerInfoAction.Remove, this);
        PacketEntityDestroy packetEntityDestroy = new PacketEntityDestroy(this);

        observer.observe(packetEntityDestroy);
        observer.observe(packetPlayerInfo);
    }

    public Transform transform() {
        return GameObject.transform();
    }

    public void send(Packet packet) {
        info("Observing "+packet.name()+" from GameObject");
        GameObject.observe(packet);
    }
}
