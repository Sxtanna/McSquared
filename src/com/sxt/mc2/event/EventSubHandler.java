package com.sxt.mc2.event;

import com.sxt.mc.event.subscribe.SubscribeHandler;
import com.sxt.mc2.event.base.Event;
import com.sxt.mc2.event.base.EventMethod;
import com.sxt.mc2.event.base.EventWatcher;

import java.lang.reflect.Method;

public class EventSubHandler extends SubscribeHandler<EventWatcher, EventMethod> {

    public EventSubHandler() {
        super(EventWatcher.class);
    }

    @Override
    public boolean validate(Class<?> type) {
        return Event.class.isAssignableFrom(type);
    }

    @Override
    public EventMethod from(Method method, EventWatcher annotation) {
        return new EventMethod(method, annotation.priority());
    }
}
