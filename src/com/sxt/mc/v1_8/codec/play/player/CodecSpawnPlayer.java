package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.player.PacketSpawnPlayer;

public class CodecSpawnPlayer implements Codec<PacketSpawnPlayer> {

    @Override
    public void encode(Buffer buffer, PacketSpawnPlayer packet) {
        
    }

    @Override
    public void decode(Buffer buffer, PacketSpawnPlayer packet) {

    }
}
