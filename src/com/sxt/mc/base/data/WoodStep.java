package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;

public class WoodStep extends MaterialData<WoodStep, WoodType> {

    public WoodStep(int data) {
        super(Material.WoodStep, data);
    }

    public WoodStep() {
        super(Material.WoodStep);
    }

    public WoodStep(WoodType data) {
        super(Material.WoodStep, data);
    }
}
