package test;

import com.sxt.Main;
import com.sxt.mc.base.component.StatusComponent;
import com.sxt.mc.protocol.AdaptingProtocol;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.util.C;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc.v1_8.packet.play.PacketChatMessage;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityTeleport;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayer;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerTeleport;
import com.sxt.mc2.event.base.EventWatcher;
import com.sxt.mc2.event.types.packet.PacketInEvent;
import com.sxt.mc2.event.types.packet.PacketOutEvent;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;

public class CustomEvents implements Logging {

    @EventWatcher
    public void onPacketOut(PacketOutEvent e) {
        if (!e.is(PacketStatusResponse.class)) return;

        Protocol protocol = e.packet().session().protocol();
        if (!(protocol instanceof AdaptingProtocol)) return;

        PacketStatusResponse response = e.get(PacketStatusResponse.class);
        StatusComponent status = response.status();

        status.Description(status.description() + C.Line + C.BWhite + Main.ProtocolNames().find(protocol.version()));
    }

    @EventWatcher
    public void onOut(PacketOutEvent e) {
    }

    @EventWatcher
    public void onIn(PacketInEvent e) {
        if (e.is(PacketPlayer.class)) return;
    }

}
