package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerPosition;

public class CodecPlayerPosition implements Codec<PacketPlayerPosition> {

    @Override
    public void encode(Buffer buffer, PacketPlayerPosition packet) {
        buffer.writeDouble(packet.x());
        buffer.writeDouble(packet.y());
        buffer.writeDouble(packet.z());
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketPlayerPosition packet) {
        packet.X(buffer.readDouble());
        packet.Y(buffer.readDouble());
        packet.Z(buffer.readDouble());
        packet.OnGround(buffer.readBoolean());
    }
}
