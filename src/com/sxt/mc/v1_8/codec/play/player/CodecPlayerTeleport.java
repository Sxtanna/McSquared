package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerTeleport;

public class CodecPlayerTeleport implements Codec<PacketPlayerTeleport> {

    @Override
    public void encode(Buffer buffer, PacketPlayerTeleport packet) {
        buffer.writeDouble(packet.x());
        buffer.writeDouble(packet.y());
        buffer.writeDouble(packet.z());
        buffer.writeFloat((float) packet.yaw());
        buffer.writeFloat((float) packet.pitch());
        buffer.writeByte(packet.flags());
    }

    @Override
    public void decode(Buffer buffer, PacketPlayerTeleport packet) {
        packet.X(buffer.readDouble());
        packet.Y(buffer.readDouble());
        packet.Z(buffer.readDouble());
        packet.Yaw(buffer.readFloat());
        packet.Pitch(buffer.readFloat());
        packet.Flags(buffer.readByte());
    }
}
