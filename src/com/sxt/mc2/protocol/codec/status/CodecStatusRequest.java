package com.sxt.mc2.protocol.codec.status;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc2.protocol.packet.status.PacketStatusRequest;

public class CodecStatusRequest implements Codec<PacketStatusRequest> {

    @Override
    public void encode(Buffer buffer, PacketStatusRequest packet) {
    }

    @Override
    public void decode(Buffer buffer, PacketStatusRequest packet) {
    }
}
