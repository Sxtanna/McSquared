package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntityHeadLook;

@PacketInfo(target = Target.Client, state = State.Play, id = 25, codec = CodecEntityHeadLook.class)
public class PacketEntityHeadLook extends Packet {

    private int entityId;
    private double headYaw;

    public PacketEntityHeadLook(int entityId, double headYaw) {
        this.entityId = entityId;
        this.headYaw = headYaw;
    }

    public PacketEntityHeadLook() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketEntityHeadLook EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public double headYaw() {
        return headYaw;
    }

    public PacketEntityHeadLook HeadYaw(double headYaw) {
        this.headYaw = headYaw;
        return this;
    }
}
