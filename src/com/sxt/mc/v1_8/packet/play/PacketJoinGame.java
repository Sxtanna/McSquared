package com.sxt.mc.v1_8.packet.play;

import com.sxt.mc.base.Difficulty;
import com.sxt.mc.base.Dimension;
import com.sxt.mc.base.Gamemode;
import com.sxt.mc.base.LevelType;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.CodecJoinGame;

@PacketInfo(target = Target.Client, state = State.Play, id = 1, codec = CodecJoinGame.class)
public class PacketJoinGame extends Packet {

    private int entityId, maxPlayers;
    private Boolean hardcore, reducedDebugInfo;
    private LevelType levelType;
    private Gamemode gamemode;
    private Dimension dimension;
    private Difficulty difficulty;

    public PacketJoinGame(int entityId, int maxPlayers, boolean hardcore, boolean reducedDebugInfo, LevelType levelType, Gamemode gamemode, Dimension dimension, Difficulty difficulty) {
        this.entityId = entityId;
        this.maxPlayers = maxPlayers;
        this.hardcore = hardcore;
        this.reducedDebugInfo = reducedDebugInfo;
        this.levelType = levelType;
        this.gamemode = gamemode;
        this.dimension = dimension;
        this.difficulty = difficulty;
    }

    public PacketJoinGame() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketJoinGame EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public int maxPlayers() {
        return maxPlayers;
    }

    public PacketJoinGame MaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        return this;
    }

    public boolean hardcore() {
        return hardcore == null ? false : hardcore;
    }

    public PacketJoinGame Hardcore(boolean hardcore) {
        this.hardcore = hardcore;
        return this;
    }

    public boolean reducedDebugInfo() {
        return reducedDebugInfo == null ? false : reducedDebugInfo;
    }

    public PacketJoinGame ReducedDebugInfo(boolean reducedDebugInfo) {
        this.reducedDebugInfo = reducedDebugInfo;
        return this;
    }

    public LevelType levelType() {
        return levelType;
    }

    public PacketJoinGame LevelType(LevelType levelType) {
        this.levelType = levelType;
        return this;
    }

    public Gamemode gamemode() {
        return gamemode;
    }

    public PacketJoinGame Gamemode(Gamemode gamemode) {
        this.gamemode = gamemode;
        return this;
    }

    public Dimension dimension() {
        return dimension;
    }

    public PacketJoinGame Dimension(Dimension dimension) {
        this.dimension = dimension;
        return this;
    }

    public Difficulty difficulty() {
        return difficulty;
    }

    public PacketJoinGame Difficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        return this;
    }
}
