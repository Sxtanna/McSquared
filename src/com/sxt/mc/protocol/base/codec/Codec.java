package com.sxt.mc.protocol.base.codec;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.type.Logging;

public interface Codec<P extends Packet> extends Logging {

    void encode(Buffer buffer, P packet);

    void decode(Buffer buffer, P packet);

}
