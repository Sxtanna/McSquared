package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.PrismarineType;

public class Prismarine extends MaterialData<Prismarine, PrismarineType> {

    public Prismarine(int data) {
        super(Material.Prismarine, data);
    }

    public Prismarine() {
        super(Material.Prismarine);
    }

    public Prismarine(PrismarineType data) {
        super(Material.Prismarine, data);
    }
}
