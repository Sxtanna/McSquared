package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum StoneBrickType implements MaterialTypeData<StoneBrickType> {

    Plain,
    Mossy,
    Cracked,
    Chiseled;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public StoneBrickType byData(int data) {
        if (data < 0 || data > 3) return Plain;
        return values()[data];
    }
}
