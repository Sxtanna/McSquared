package com.sxt.mc.event.subscribe;

import com.sxt.mc.exception.EventException;
import com.sxt.mc.util.type.Named;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public abstract class SubscribedMethod<S extends SubscribedMethod> implements Named, Comparable<S> {

    private final Method method;

    public SubscribedMethod(Method method) {
        this.method = method;
    }

    @Override
    public String name() {
        return getClass().getSimpleName() + ":" + named(method);
    }

    public Method method() {
        return method;
    }

    public boolean isFor(Class<? extends Subscribable> type) {
        if (type == null) return false;

        Parameter param = method.getParameters()[0];
        return type.isAssignableFrom(param.getType());
    }

    public void call(Object subscribed, Subscribable event) {
        if (!isFor(event.getClass())) return;

        try {
            method.invoke(subscribed, event);
        } catch (Exception e) {
            throw new EventException(event.name() + " > " + named(method), e.getCause());
        }
    }

    private String named(Method method) {
        return method.getName() + ":" + method.getDeclaringClass().getSimpleName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SubscribedMethod<?> that = (SubscribedMethod<?>) o;

        return new EqualsBuilder()
                .append(method, that.method)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(method)
                .toHashCode();
    }
}
