package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.entity.Entity;
import com.sxt.mc.util.F;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntityDestroy;

import java.util.List;
import java.util.stream.Collectors;

@PacketInfo(target = Target.Client, state = State.Play, id = 19, codec = CodecEntityDestroy.class)
public class PacketEntityDestroy extends Packet {

    private List<Integer> entityIds;

    public PacketEntityDestroy(List<Integer> entityIds) {
        this.entityIds = entityIds;
    }

    public PacketEntityDestroy(Entity... entities) {
        this.entityIds = F.map(entities, Entity::entityId).collect(Collectors.toList());
    }

    public PacketEntityDestroy() {}


    public List<Integer> entityIds() {
        return entityIds;
    }

    public PacketEntityDestroy EntityIds(List<Integer> entityIds) {
        this.entityIds = entityIds;
        return this;
    }
}
