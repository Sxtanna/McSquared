package com.sxt.mc2.protocol.codec.status;

import com.sxt.mc.base.component.StatusComponent;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.util.type.Gsonable;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;

public class CodecStatusResponse implements Codec<PacketStatusResponse> {

    @Override
    public void encode(Buffer buffer, PacketStatusResponse packet) {
        buffer.writeString(packet.status().to());
    }

    @Override
    public void decode(Buffer buffer, PacketStatusResponse packet) {
        packet.Status(Gsonable.from(buffer.readString(), StatusComponent.class));
    }
}
