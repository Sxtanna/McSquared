package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.SandType;
import com.sxt.mc.util.type.MaterialTypeData;

public class Sand extends MaterialData<Sand, SandType> {

    public Sand(int data) {
        super(Material.Sand, data);
    }

    public Sand() {
        super(Material.Sand);
    }

    public Sand(SandType data) {
        super(Material.Sand, data);
    }
}
