package com.sxt.mc.base.component;

import java.util.List;

public class PlayersComponent {

    private int max, online;
    private List<PlayerComponent> sample;

    public PlayersComponent(int max, int online, List<PlayerComponent> sample) {
        this.max = max;
        this.online = online;
        this.sample = sample;
    }

    public int max() {
        return max;
    }

    public PlayersComponent Max(int max) {
        this.max = max;
        return this;
    }

    public int online() {
        return online;
    }

    public PlayersComponent Online(int online) {
        this.online = online;
        return this;
    }

    public List<PlayerComponent> sample() {
        return sample;
    }
}
