package com.sxt.mc.server.base;

import java.util.List;

public interface Observable {

    List<Observer> observers();

    void start(Observer observer);

    void stop(Observer observer);

    default void updateAll() {
    }

}
