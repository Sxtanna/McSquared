package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityMove;

public class CodecEntityMove implements Codec<PacketEntityMove> {

    @Override
    public void encode(Buffer buffer, PacketEntityMove packet) {
        buffer.writeVarInt(packet.entityId());
        buffer.writeFixedPointByte(packet.deltaX());
        buffer.writeFixedPointByte(packet.deltaY());
        buffer.writeFixedPointByte(packet.deltaZ());
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketEntityMove packet) {
        packet.EntityId(buffer.readVarInt());
        packet.DeltaX(buffer.readFixedPointByte());
        packet.DeltaY(buffer.readFixedPointByte());
        packet.DeltaZ(buffer.readFixedPointByte());
        packet.OnGround(buffer.readBoolean());
    }
}
