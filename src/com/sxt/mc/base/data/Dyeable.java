package com.sxt.mc.base.data;

import com.sxt.mc.base.DyeColor;
import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;

public class Dyeable extends MaterialData<Dyeable, DyeColor> {

    public Dyeable(Material material, int data) {
        super(material, material.dyeable() ? data : 0);
    }

    public Dyeable(Material material) {
        super(material);
    }

    public Dyeable(Material material, DyeColor data) {
        super(material, material.dyeable() ? data : DyeColor.White);
    }

    @Override
    public Dyeable Data(int data) {
        return super.Data(type().dyeable() ? data : 0);
    }
}
