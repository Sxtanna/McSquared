package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.SandstoneType;

public class Sandstone extends MaterialData<Sandstone, SandstoneType> {

    public Sandstone(int data) {
        super(Material.Sandstone, data);
    }

    public Sandstone() {
        super(Material.Sandstone);
    }

    public Sandstone(SandstoneType data) {
        super(Material.Sandstone, data);
    }
}
