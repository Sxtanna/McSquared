package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntityTeleport;

@PacketInfo(target = Target.Client, state = State.Play, id = 24, codec = CodecEntityTeleport.class)
public class PacketEntityTeleport extends Packet {

    private int entityId;
    private double x, y, z, yaw, pitch;
    private boolean onGround;

    public PacketEntityTeleport(int entityId, double x, double y, double z, double yaw, double pitch, boolean onGround) {
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    /*public PacketEntityTeleport(Player player, PacketPlayerTeleport teleport) {
        this(player.entityId(), teleport.x(), teleport.y(), teleport.z(), teleport.yaw(), teleport.pitch(), true);
    }*/

    public PacketEntityTeleport() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketEntityTeleport EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public double x() {
        return x;
    }

    public PacketEntityTeleport X(double x) {
        this.x = x;
        return this;
    }

    public double y() {
        return y;
    }

    public PacketEntityTeleport Y(double y) {
        this.y = y;
        return this;
    }

    public double z() {
        return z;
    }

    public PacketEntityTeleport Z(double z) {
        this.z = z;
        return this;
    }

    public double yaw() {
        return yaw;
    }

    public PacketEntityTeleport Yaw(double yaw) {
        this.yaw = yaw;
        return this;
    }

    public double pitch() {
        return pitch;
    }

    public PacketEntityTeleport Pitch(double pitch) {
        this.pitch = pitch;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketEntityTeleport OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }

    public PacketEntityTeleport Position(Vector position) {
        return X(position.x()).Y(position.y()).Z(position.z());
    }

    public PacketEntityTeleport Rotation(double yaw, double pitch) {
        return Yaw(yaw).Pitch(pitch);
    }
}
