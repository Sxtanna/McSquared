package com.sxt.mc.protocol.base;

import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.util.Registry;

public class ProtocolRegistry extends Registry<Protocol> {

    public void register(Protocol protocol) {
        super.register(protocol.version(), protocol);
    }
}
