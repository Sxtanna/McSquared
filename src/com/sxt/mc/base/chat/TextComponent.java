package com.sxt.mc.base.chat;

import com.sxt.mc.util.F;

public class TextComponent extends BaseComponent {

    private String text;

    public TextComponent(TextComponent old) {
        super(old);
        Text(old.text());
    }

    public TextComponent(BaseComponent... components) {
        Text("");
        Extra(F.list(components));
    }

    public TextComponent(String text) {
        Text(F.color(text));
    }

    public String text() {
        return text;
    }

    public TextComponent Text(String text) {
        this.text = text;
        return this;
    }

    @Override
    public BaseComponent copy() {
        return new TextComponent(this);
    }
}
