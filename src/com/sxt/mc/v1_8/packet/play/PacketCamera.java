package com.sxt.mc.v1_8.packet.play;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.CodecCamera;

@PacketInfo(target = Target.Client, state = State.Play, id = 67, codec = CodecCamera.class)
public class PacketCamera extends Packet {

    private int entityId;

    public PacketCamera(int entityId) {
        this.entityId = entityId;
    }

    /*public PacketCamera(Entity entity) {
        this.entityId = entity.entityId();
    }*/

    public PacketCamera() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketCamera EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }
}
