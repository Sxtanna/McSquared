package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum CobbleWallType implements MaterialTypeData<CobbleWallType> {

    Plain,
    Mossy;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public CobbleWallType byData(int data) {
        if (data < 0 || data > 1) return Plain;
        return values()[data];
    }
}
