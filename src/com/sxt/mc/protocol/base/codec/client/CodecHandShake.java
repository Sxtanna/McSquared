package com.sxt.mc.protocol.base.codec.client;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.protocol.base.packet.client.PacketHandShake;

public class CodecHandShake implements Codec<PacketHandShake> {

    @Override
    public void encode(Buffer buffer, PacketHandShake packet) {
        buffer.writeVarInt(packet.version());
        buffer.writeString(packet.address());
        buffer.writeShort(packet.port());
        buffer.writeVarInt(packet.state().id());
    }

    @Override
    public void decode(Buffer buffer, PacketHandShake packet) {
        packet.Version(buffer.readVarInt());
        packet.Address(buffer.readString());
        packet.Port(buffer.readUnsignedShort());
        packet.State(State.fromID(buffer.readVarInt()));
    }
}
