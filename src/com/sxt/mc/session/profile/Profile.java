package com.sxt.mc.session.profile;

import com.sxt.mc.network.auth.Property;

import java.util.List;
import java.util.UUID;

public class Profile {

    private final String name;
    private final UUID uuid;
    private final List<Property> properties;

    public Profile(String name, UUID uuid, List<Property> properties) {
        this.name = name;
        this.uuid = uuid;
        this.properties = properties;
    }

    public String name() {
        return name;
    }

    public UUID uuid() {
        return uuid;
    }

    public List<Property> properties() {
        return properties;
    }
}
