package com.sxt.mc2.database;

import com.sxt.mc.util.task.CallEx;
import com.sxt.mc2.database.base.Database;
import com.sxt.mc2.database.call.RedisCall;
import com.sxt.mc2.database.config.RedisConfig;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Redis extends Database<Redis, JedisPool, Jedis> {

    private JedisPool Pool;
    private int defaultDB = 0;
    private final RedisConfig Config;

    public Redis(RedisConfig config) {
        Config = config;
    }

    public Redis() {
        Config = RedisConfig.get();
    }

    public int defaultDB() {
        return defaultDB;
    }

    public Redis DefaultDB(int defaultDB) {
        this.defaultDB = defaultDB;
        return this;
    }

    @Override
    protected void load() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(Config.MaxTotal());
        config.setMaxIdle(Config.MaxIdle());
        config.setTestOnBorrow(true);

        Pool = new JedisPool(config, Config.Address(), Config.Port(), Config.Timeout(), Config.Auth());

        loaded = true;
        execute(data -> connected = data.isConnected());
    }

    @Override
    protected void poison() throws Exception {
        Pool.destroy();
    }

    @Override
    public Jedis resource() {
        Jedis jedis = pool().getResource();
        jedis.select(defaultDB());

        return jedis;
    }

    @Override
    public JedisPool pool() {
        return Pool;
    }

    @Override
    public void execute(CallEx<Jedis, Exception> call) {
        try {
            Executor.execute(new RedisCall() {

                @Override
                public void callEx(Jedis data) throws Exception {
                    call.call(data);
                }

                @Override
                public Redis database() {
                    return Redis.this;
                }
            });
        } catch (Exception e) {
            call.fail(e);
        }

    }
}
