package com.sxt.mc.v1_8.codec.login.encrypt;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionRequest;

public class CodecEncryptionRequest implements Codec<PacketEncryptionRequest> {

    @Override
    public void encode(Buffer buffer, PacketEncryptionRequest packet) {
        buffer.writeString(packet.sessionId());
        buffer.writeByteArray(packet.publicKey());
        buffer.writeByteArray(packet.vToken());
    }

    @Override
    public void decode(Buffer buffer, PacketEncryptionRequest packet) {
        packet.SessionId(buffer.readString());
        packet.PublicKey(buffer.readByteArray());
        packet.vToken(buffer.readByteArray());
    }
}
