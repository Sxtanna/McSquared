package com.sxt.mc.protocol.base.codec.client;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.protocol.base.packet.client.PacketKeepAlive;

public class CodecKeepAlive implements Codec<PacketKeepAlive> {

    @Override
    public void encode(Buffer buffer, PacketKeepAlive packet) {
        buffer.writeVarInt(packet.keepAliveId());
    }

    @Override
    public void decode(Buffer buffer, PacketKeepAlive packet) {
        packet.KeepAliveId(buffer.readVarInt());
    }
}
