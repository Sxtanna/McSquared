package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum QuartsBlockType implements MaterialTypeData<QuartsBlockType> {

    Plain,
    Chiseled,
    Pillar;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public QuartsBlockType byData(int data) {
        if (data < 0 || data > 2) return Plain;
        return values()[data];
    }
}
