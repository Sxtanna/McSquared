package com.sxt.mc.base;

public class BlockPosition {

    private int x, y, z;

    public BlockPosition(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public BlockPosition() {}


    public int x() {
        return x;
    }

    public BlockPosition X(int x) {
        this.x = x;
        return this;
    }

    public int y() {
        return y;
    }

    public BlockPosition Y(int y) {
        this.y = y;
        return this;
    }

    public int z() {
        return z;
    }

    public BlockPosition Z(int z) {
        this.z = z;
        return this;
    }

    public BlockPosition Position(int x, int y, int z) {
        return X(x).Y(y).Z(z);
    }
}
