package com.sxt.mc.base;

import com.sxt.mc.util.F;

public enum GameRule {

    CommandBlockOutput          (true),
    DisableElytraMovementCheck  (true),
    DoDaylightCycle             (true),
    DoEntityDrops               (true),
    DoFireTick                  (true),
    DoMobLoot                   (true),
    DoMobSpawning               (true),
    DoTileDrops                 (true),
    KeepInventory               (true),
    LogAdminCommands            (true),
    MobGriefing                 (true),
    NaturalRegeneration         (true),
    RandomTickSpeed             (false),
    ReducedDebugInfo            (true),
    SendCommandFeedback         (true),
    ShowDeathMessages           (true),
    SpawnRadius                 (false),
    SpectatorsGenerateChunks    (true);


    private Boolean needBoolean;

    /**
     * A GameRule
     *
     * @param needBoolean Whether this GameRule is defined as a boolean or a number value
     */
    GameRule(Boolean needBoolean) {
        this.needBoolean = needBoolean;
    }

    public Boolean needBoolean() {
        return needBoolean;
    }

    public Boolean needNumber() {
        return !needBoolean();
    }

    public String gameRuleName() {
        char[] chars = name().toCharArray();
        chars[0] += 32;
        return new String(chars);
    }

    public static GameRule match(String string) {
        for (GameRule rule : values()) if (F.equals(rule.name(), string)) return rule;
        return null;
    }
}
