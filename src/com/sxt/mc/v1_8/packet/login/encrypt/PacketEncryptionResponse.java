package com.sxt.mc.v1_8.packet.login.encrypt;


import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.login.encrypt.CodecEncryptionResponse;

@PacketInfo(target = Target.Server, state = State.Login, id = 1, codec = CodecEncryptionResponse.class)
public class PacketEncryptionResponse extends Packet {

    private byte[] sharedKey, vToken;

    public PacketEncryptionResponse(byte[] sharedKey, byte[] vToken) {
        this.sharedKey = sharedKey;
        this.vToken = vToken;
    }

    public PacketEncryptionResponse() {
    }


    public byte[] sharedKey() {
        return sharedKey;
    }

    public PacketEncryptionResponse SharedKey(byte[] sharedKey) {
        this.sharedKey = sharedKey;
        return this;
    }

    public byte[] vToken() {
        return vToken;
    }

    public PacketEncryptionResponse vToken(byte[] vToken) {
        this.vToken = vToken;
        return this;
    }
}
