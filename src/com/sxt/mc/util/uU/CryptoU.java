package com.sxt.mc.util.uU;

import com.sxt.mc.util.type.Logging;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;

public class CryptoU implements Logging {

    private final SecureRandom SecureRandom;

    public CryptoU() {
        SecureRandom = new SecureRandom();
    }

    public KeyPair generateRSAKeyPair() {
        KeyPair keyPair = null;
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(1024);

            keyPair = generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            error("Unable to generate RSA key pair");
            e.printStackTrace();
        }
        return keyPair;
    }

    public byte[] generateToken(int length) {
        byte[] token = new byte[length];
        SecureRandom.nextBytes(token);
        return token;
    }

    public Key toX509(Key base) {
        Key key = null;
        try {
            X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(base.getEncoded());
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");

            key = keyFactory.generatePublic(encodedKeySpec);
        } catch (Exception e) {
            error("Unable to generate X509 encoded key");
            e.printStackTrace();
        }
        return key;
    }
}
