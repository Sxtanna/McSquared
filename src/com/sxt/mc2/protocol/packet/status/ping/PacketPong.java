package com.sxt.mc2.protocol.packet.status.ping;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc2.protocol.codec.status.ping.CodecPong;

@PacketInfo(target = Target.Client, state = State.Status, id = 1, codec = CodecPong.class)
public class PacketPong extends Packet {

    private long time;

    public PacketPong(long time) {
        this.time = time;
    }

    public PacketPong() {
    }


    public long time() {
        return time;
    }

    public PacketPong Time(long time) {
        this.time = time;
        return this;
    }
}
