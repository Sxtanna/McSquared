package com.sxt.mc.protocol.base;

public enum State {

    HandShake,
    Status,
    Login,
    Play;

    public int id() {
        return ordinal();
    }

    public static State fromID(int id) {
        return id < 0 || id > values().length ? null : values()[id];
    }
}
