package test;

import com.sxt.mc2.event.base.Event;

public class TestEvent extends Event {

    private String message;

    public TestEvent(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }

    public TestEvent Message(String message) {
        this.message = message;
        return this;
    }
}
