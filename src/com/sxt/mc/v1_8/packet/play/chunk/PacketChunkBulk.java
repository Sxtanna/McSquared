package com.sxt.mc.v1_8.packet.play.chunk;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.chunk.CodecChunkBulk;

import java.util.List;

@PacketInfo(target = Target.Client, state = State.Play, id = 38, codec = CodecChunkBulk.class)
public class PacketChunkBulk extends Packet {

    private boolean skylight;
    private List<PacketChunk> chunks;

    public PacketChunkBulk(boolean skylight, List<PacketChunk> chunks) {
        this.skylight = skylight;
        this.chunks = chunks;
    }

    public PacketChunkBulk() {
    }


    public boolean skylight() {
        return skylight;
    }

    public PacketChunkBulk Skylight(boolean skylight) {
        this.skylight = skylight;
        return this;
    }

    public List<PacketChunk> chunks() {
        return chunks;
    }

    public PacketChunkBulk Chunks(List<PacketChunk> chunks) {
        this.chunks = chunks;
        return this;
    }
}
