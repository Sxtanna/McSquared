package com.sxt.mc.v1_8;

import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.base.packet.client.PacketKeepAlive;
import com.sxt.mc.v1_8.packet.login.PacketLoginRequest;
import com.sxt.mc.v1_8.packet.login.PacketLoginResponse;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionRequest;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionResponse;
import com.sxt.mc.v1_8.packet.play.PacketCamera;
import com.sxt.mc.v1_8.packet.play.PacketChatMessage;
import com.sxt.mc.v1_8.packet.play.PacketJoinGame;
import com.sxt.mc.v1_8.packet.play.chunk.PacketChunk;
import com.sxt.mc.v1_8.packet.play.chunk.PacketChunkBulk;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntity;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityDestroy;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityHeadLook;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityLook;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityLookMove;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityMove;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityTeleport;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayer;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerChatMessage;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerInfo;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerLook;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerPosition;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerPositionLook;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerTeleport;
import com.sxt.mc.v1_8.packet.play.player.PacketSpawnPlayer;
import com.sxt.mc2.protocol.packet.status.PacketStatusRequest;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;
import com.sxt.mc2.protocol.packet.status.ping.PacketPing;
import com.sxt.mc2.protocol.packet.status.ping.PacketPong;

public class ProtocolV1_8 extends Protocol {

    @Override
    public int version() {
        return 47;
    }

    @Override
    public String name() {
        return "v1.8";
    }

    @Override
    protected void register() {
        super.register();

        // Status
        Register(PacketStatusRequest.class, PacketStatusResponse.class);
        Register(PacketPing.class, PacketPong.class);

        // Login
        Register(PacketLoginRequest.class, PacketLoginResponse.class);
        Register(PacketEncryptionRequest.class, PacketEncryptionResponse.class);

        // Play
        Register(PacketKeepAlive.class);
        Register(PacketJoinGame.class);
        Register(PacketPlayerChatMessage.class, PacketChatMessage.class);

        Register(PacketPlayer.class, PacketEntity.class);

        Register(PacketPlayerPosition.class, PacketPlayerLook.class, PacketPlayerPositionLook.class, PacketPlayerTeleport.class);
        Register(PacketSpawnPlayer.class);

        Register(PacketEntityDestroy.class);
        Register(PacketEntity.class);

        Register(PacketEntityMove.class);
        Register(PacketEntityLook.class);
        Register(PacketEntityLookMove.class);

        Register(PacketEntityTeleport.class);
        Register(PacketEntityHeadLook.class);

        Register(PacketChunk.class);
        Register(PacketChunkBulk.class);

        Register(PacketPlayerInfo.class);
        Register(PacketCamera.class);
    }
}
