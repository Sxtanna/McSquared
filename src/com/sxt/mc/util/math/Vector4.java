package com.sxt.mc.util.math;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Vector4<V extends Vector4> extends Vector<V> {

    protected double w;

    public Vector4(double x, double y, double z, double w) {
        super(x, y, z);
        this.w = w;
    }

    public Vector4() {}

    public Vector4(V other) {
        Set(other.x(), other.y(), other.z(), other.w());
    }

    public double w() {
        return w;
    }

    public V W(double w) {
        this.w = w;
        return thisClass();
    }

    public V Set(double x, double y, double z, double w) {
        Set(x, y, z);
        return W(w);
    }

    @Override
    public V Set(V other) {
        return Set(other.x(), other.y(), other.z(), other.w());
    }

    public Vector toVector() {
        return new Vector<>(x(), y(), z());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Vector4<?> vector4 = (Vector4<?>) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(w, vector4.w)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(super.hashCode())
                .append(w)
                .toHashCode();
    }

    @Override
    public Class<? extends Vector> type() {
        return Vector4.class;
    }
}
