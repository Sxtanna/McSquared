package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntity;

public class CodecEntity implements Codec<PacketEntity> {

    @Override
    public void encode(Buffer buffer, PacketEntity packet) {
        buffer.writeVarInt(packet.entityId());
    }

    @Override
    public void decode(Buffer buffer, PacketEntity packet) {
        packet.EntityId(buffer.readVarInt());
    }
}
