package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum PrismarineType implements MaterialTypeData<PrismarineType> {

    Plain,
    Bricks,
    Dark;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public PrismarineType byData(int data) {
        if (data < 0 || data > 2) return Plain;
        return values()[data];
    }
}
