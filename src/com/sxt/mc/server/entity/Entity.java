package com.sxt.mc.server.entity;

import com.sxt.mc.base.meta.Metadata;
import com.sxt.mc.network.handlers.PlayHandler;
import com.sxt.mc.server.Component;

import java.util.UUID;

public abstract class Entity<E extends Entity> extends Component<E> {

    private static int NextID = 0;

    private final int entityId;
    private final Metadata metadata = new Metadata();

    protected int nextTick = PlayHandler.Tps;

    protected UUID uuid;

    public Entity() {
        this.entityId = NextID();
        this.uuid = UUID.randomUUID();
    }

    public int entityId() {
        return entityId;
    }

    public Metadata metadata() {
        return metadata;
    }

    public UUID uuid() {
        return uuid;
    }



    public static int NextID() {
        return NextID++;
    }
}
