package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.SlabType;

public class DoubleStep extends MaterialData<DoubleStep, SlabType> {

    public DoubleStep(int data) {
        super(Material.DoubleStep, data);
    }

    public DoubleStep() {
        super(Material.DoubleStep);
    }

    public DoubleStep(SlabType data) {
        super(Material.DoubleStep, data);
    }
}
