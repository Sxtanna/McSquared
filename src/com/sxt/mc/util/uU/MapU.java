package com.sxt.mc.util.uU;

import java.util.Iterator;
import java.util.Map;

public class MapU {

    public static<K, V> Map.Entry<K, V> first(Map<K, V> map) {
        if (map == null || map.isEmpty()) return null;
        return map.entrySet().iterator().next();
    }

    public static<K, V> Iterator<Map.Entry<K, V>> iterate(Map<K, V> map) {
        if (map == null) return null;
        return map.entrySet().iterator();
    }
}
