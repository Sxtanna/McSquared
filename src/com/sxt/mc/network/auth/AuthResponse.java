package com.sxt.mc.network.auth;

import com.sxt.mc.util.type.Gsonable;

import java.util.ArrayList;
import java.util.List;

public class AuthResponse implements Gsonable<AuthResponse> {

    private String id, name;
    private List<Property> properties = new ArrayList<>();

    public String id() {
        return id;
    }

    public String name() {
        return name;
    }

    public List<Property> properties() {
        return properties;
    }

    @Override
    public Class<? extends AuthResponse> type() {
        return AuthResponse.class;
    }
}
