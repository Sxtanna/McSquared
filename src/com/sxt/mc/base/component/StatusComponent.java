package com.sxt.mc.base.component;

import com.sxt.mc.util.F;
import com.sxt.mc.util.type.Gsonable;

public class StatusComponent implements Gsonable<StatusComponent> {

    private VersionComponent version;
    private PlayersComponent players;

    private String description;

    public StatusComponent(VersionComponent version, PlayersComponent players, String description) {
        this.version = version;
        this.players = players;
        this.description = description;
    }

    public VersionComponent version() {
        return version;
    }

    public StatusComponent Version(VersionComponent version) {
        this.version = version;
        return this;
    }

    public PlayersComponent players() {
        return players;
    }

    public StatusComponent Players(PlayersComponent players) {
        this.players = players;
        return this;
    }

    public String description() {
        return description;
    }

    public StatusComponent Description(String description) {
        this.description = F.color(description);
        return this;
    }

    @Override
    public Class<? extends StatusComponent> type() {
        return StatusComponent.class;
    }
}
