package com.sxt.mc.util;

import java.util.function.Function;

public class Ticks implements Function<Long, Long> {

    private static final Long Multiplier = 50L;
    private static final Ticks Ticks = new Ticks();

    public static Long convert(int input) {
        return Ticks.apply((long) input);
    }

    @Override
    public Long apply(Long aLong) {
        return aLong * Multiplier;
    }

}
