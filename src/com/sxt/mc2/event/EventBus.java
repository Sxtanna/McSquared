package com.sxt.mc2.event;

import com.sxt.mc.event.base.Bus;
import com.sxt.mc2.event.base.EventMethod;
import com.sxt.mc2.event.base.EventWatcher;

public final class EventBus extends Bus<EventWatcher, EventMethod, EventSubHandler> {

    public EventBus() {
        super(new EventSubHandler());
    }
}
