package com.sxt.mc.v1_8.codec.play.chunk;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.chunk.PacketChunk;

public class CodecChunk implements Codec<PacketChunk> {

    @Override
    public void encode(Buffer buffer, PacketChunk packet) {
        buffer.writeInt(packet.x());
        buffer.writeInt(packet.z());

        buffer.writeBoolean(packet.continuous());
        buffer.writeShort(packet.mask());
        buffer.writeByteArray(packet.data());
    }

    @Override
    public void decode(Buffer buffer, PacketChunk packet) {
        packet.X(buffer.readInt());
        packet.Z(buffer.readInt());
        packet.Continuous(buffer.readBoolean());
        packet.Mask(buffer.readUnsignedShort());
        packet.Data(buffer.readByteArray());
    }
}
