package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.DirtType;
import com.sxt.mc.util.type.MaterialTypeData;

public class Dirt extends MaterialData<Dirt, DirtType> {

    public Dirt(int data) {
        super(Material.Dirt, data);
    }

    public Dirt() {
        super(Material.Dirt);
    }

    public Dirt(DirtType data) {
        super(Material.Dirt, data);
    }
}
