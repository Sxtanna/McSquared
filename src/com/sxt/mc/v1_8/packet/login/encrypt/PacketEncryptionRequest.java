package com.sxt.mc.v1_8.packet.login.encrypt;


import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.login.encrypt.CodecEncryptionRequest;

@PacketInfo(target = Target.Client, state = State.Login, id = 1, codec = CodecEncryptionRequest.class)
public class PacketEncryptionRequest extends Packet {

    private String sessionId;
    private byte[] publicKey, vToken;

    public PacketEncryptionRequest(String sessionId, byte[] publicKey, byte[] vToken) {
        this.sessionId = sessionId;
        this.publicKey = publicKey;
        this.vToken = vToken;
    }

    public PacketEncryptionRequest() {
    }


    public String sessionId() {
        return sessionId;
    }

    public PacketEncryptionRequest SessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public byte[] publicKey() {
        return publicKey;
    }

    public PacketEncryptionRequest PublicKey(byte[] publicKey) {
        this.publicKey = publicKey;
        return this;
    }

    public byte[] vToken() {
        return vToken;
    }

    public PacketEncryptionRequest vToken(byte[] vToken) {
        this.vToken = vToken;
        return this;
    }
}
