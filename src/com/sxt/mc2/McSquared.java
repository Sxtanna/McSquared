package com.sxt.mc2;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.sxt.Main;
import com.sxt.mc.base.Difficulty;
import com.sxt.mc.base.DyeColor;
import com.sxt.mc.base.Material;
import com.sxt.mc.base.data.Dyeable;
import com.sxt.mc.base.data.Stone;
import com.sxt.mc.base.data.types.LeafType;
import com.sxt.mc.base.data.types.StoneType;
import com.sxt.mc.exception.McException;
import com.sxt.mc.log.LogProvider;
import com.sxt.mc.network.handlers.HandShakeHandler;
import com.sxt.mc.network.handlers.LoginHandler;
import com.sxt.mc.network.handlers.PingHandler;
import com.sxt.mc.network.handlers.PlayHandler;
import com.sxt.mc.server.level.Level;
import com.sxt.mc.util.Ticks;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc.util.type.Server;
import com.sxt.mc.util.uU.CryptoU;
import com.sxt.mc.util.uU.MapU;
import com.sxt.mc2.config.server.ServerConfig;
import com.sxt.mc2.event.EventBus;
import com.sxt.mc2.task.McTask;

import org.slf4j.Logger;

import java.io.File;
import java.security.KeyPair;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class McSquared {

    private static final GsonBuilder GsonBuilder = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping();
    private static Server Server;

    private static KeyPair keyPair = new CryptoU().generateRSAKeyPair();

    private static final HandShakeHandler HandShake = new HandShakeHandler();
    private static final PingHandler Ping = new PingHandler();
    private static final PlayHandler Play = new PlayHandler();
    private static final LoginHandler Login = new LoginHandler();

    private McSquared() {} // Don't you dare create an instance of this class

    public static Server Server() {
        return Server;
    }

    public static void Server(Server server) {
        if (Server != null) throw new McException("Server is already Set");
        Server = server;
        Logger().info("Setup server " + server.name() + " using protocol " + server.Protocol().version());

        Server().Packets().register(HandShake);
        Server().Packets().register(Ping);
        Server().Packets().register(Login);
        Server().Packets().register(Play);

        Events().register(Ping);
        Events().register(Play);

        McTask.submit(Play).goRepeat(10, TimeUnit.SECONDS);

        Config().Worlds().forEach(((name, worldType) -> {
            Logger().info("Loading world " + name);
            Server().Levels().put(UUID.randomUUID(), new Level(worldType.levelType(), worldType.dimension(), Difficulty.Easy));
        }));
    }

    public static String name() {
        return Server.name();
    }

    public static Logger Logger() {
        return Server.Logger();
    }

    public static LogProvider LogProvider() {
        return Server.LogProvider();
    }

    public static EventBus Events() {
        return Server.Events();
    }

    public static File ServerFolder() {
        return Main.RootFolder;
    }

    public static File DataFolder() {
        return Main.DataFolder;
    }

    public static ServerConfig Config() {
        return Server.Config();
    }

    public static GsonBuilder GsonBuilder() {
        return GsonBuilder;
    }

    public static Gson Gson() {
        return GsonBuilder().create();
    }

    public static KeyPair KeyPair() {
        return keyPair;
    }

    public static Level MainLevel() {
        return MapU.first(Server().Levels()).getValue();
    }
}
