package com.sxt.mc.server.level;

import com.sxt.mc.base.Difficulty;
import com.sxt.mc.base.Dimension;
import com.sxt.mc.base.LevelType;
import com.sxt.mc.network.handlers.PlayHandler;
import com.sxt.mc.server.Component;
import com.sxt.mc.server.GameObject;
import com.sxt.mc.server.entity.Entity;
import com.sxt.mc.util.F;
import com.sxt.mc.util.Ticks;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.task.McTask;

import org.apache.commons.lang3.builder.Diff;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class Level implements Runnable {

    private final GameObject RootObject;
    private final Map<ChunkKey, Chunk> Chunks = new HashMap<>();

    private final Map<UUID, Entity> Entities = new HashMap<>();
    private final ChunkLoader ChunkLoader;

    private final LevelType type;
    private final Dimension dimension;

    private Difficulty difficulty;

    public Level(LevelType type, Dimension dimension, Difficulty difficulty) {
        this.type = type;
        this.dimension = dimension;
        this.difficulty = difficulty;

        RootObject = new GameObject();
        ChunkLoader = new ChunkLoader(this);

        McSquared.Events().register(ChunkLoader);

        for (int x = -10; x < 10; x++) {
            for (int z = -10; z < 10; z++) {
                Chunk at = at(x, z);
                for (int i = 0; i < 5; i++) at.fill(i, i < 4 ? 1 : 2, 0);
            }
        }

        McTask.submit(this).goRepeat(Ticks.convert(1));
    }

    public GameObject RootObject() {
        return RootObject;
    }

    @Override
    public void run() {
        try {
            RootObject().forEachComponent(Component::update);
            RootObject().forEachChild(gameObject -> gameObject.forEachComponent(Component::update));
        } catch (Exception ignored) {}
    }

    public LevelType type() {
        return type;
    }

    public Dimension dimension() {
        return dimension;
    }

    public Difficulty difficulty() {
        return difficulty;
    }

    public Level Difficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        return this;
    }

    public Chunk at(int x, int z) {
        ChunkKey key = new ChunkKey(x, z);
        if (Chunks.containsKey(key)) return Chunks.get(key);

        Chunk chunk = new Chunk(this, x, z);
        Chunks.put(key, chunk);

        return chunk;
    }

    public Map<ChunkKey, Chunk> Chunks() {
        return Chunks;
    }

    public List<Chunk> chunks() {
        return F.map(Chunks().entrySet(), Map.Entry::getValue).collect(Collectors.toList());
    }

    public ChunkLoader ChunkLoader() {
        return ChunkLoader;
    }
}
