package com.sxt.mc.protocol.base;


import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.protocol.base.packet.Packet;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PacketInfo {

    com.sxt.mc.protocol.base.Target[] target();

    State state();

    int id();

    Class<? extends Codec<? extends Packet>> codec();
}
