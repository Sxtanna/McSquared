package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.MonsterEggsType;

public class MonsterEggs extends MaterialData<MonsterEggs, MonsterEggsType> {

public MonsterEggs(int data) {
        super(Material.MonsterEggs, data);
        }

public MonsterEggs() {
        super(Material.MonsterEggs);
        }

public MonsterEggs(MonsterEggsType data) {
        super(Material.MonsterEggs, data);
        }
}
