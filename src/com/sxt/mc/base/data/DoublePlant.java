package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.DoublePlantType;

public class DoublePlant extends MaterialData<DoublePlant, DoublePlantType> {

    public DoublePlant(int data) {
        super(Material.DoublePlant, data);
    }

    public DoublePlant() {
        super(Material.DoublePlant);
    }

    public DoublePlant(DoublePlantType data) {
        super(Material.DoublePlant, data);
    }
}
