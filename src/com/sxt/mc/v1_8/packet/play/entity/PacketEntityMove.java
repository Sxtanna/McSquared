package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntityMove;

@PacketInfo(target = Target.Client, state = State.Play, id = 21, codec = CodecEntityMove.class)
public class PacketEntityMove extends Packet {

    private int entityId;
    private double deltaX, deltaY, deltaZ;
    private boolean onGround;

    public PacketEntityMove(int entityId, double deltaX, double deltaY, double deltaZ, boolean onGround) {
        this.entityId = entityId;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.deltaZ = deltaZ;
        this.onGround = onGround;
    }

    public PacketEntityMove() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketEntityMove EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public double deltaX() {
        return deltaX;
    }

    public PacketEntityMove DeltaX(double deltaX) {
        this.deltaX = deltaX;
        return this;
    }

    public double deltaY() {
        return deltaY;
    }

    public PacketEntityMove DeltaY(double deltaY) {
        this.deltaY = deltaY;
        return this;
    }

    public double deltaZ() {
        return deltaZ;
    }

    public PacketEntityMove DeltaZ(double deltaZ) {
        this.deltaZ = deltaZ;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketEntityMove OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }

    public PacketEntityMove Delta(double deltaX, double deltaY, double deltaZ) {
        return DeltaX(deltaX).DeltaY(deltaY).DeltaZ(deltaZ);
    }
}
