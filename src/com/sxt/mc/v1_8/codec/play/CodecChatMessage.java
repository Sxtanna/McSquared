package com.sxt.mc.v1_8.codec.play;
import com.sxt.mc.base.ChatPosition;
import com.sxt.mc.base.chat.ComponentSerializer;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.PacketChatMessage;

public class CodecChatMessage implements Codec<PacketChatMessage> {

    @Override
    public void encode(Buffer buffer, PacketChatMessage packet) {
        buffer.writeString(ComponentSerializer.to(packet.message()));
        buffer.writeByte(packet.position().id());
    }

    @Override
    public void decode(Buffer buffer, PacketChatMessage packet) {
        packet.Message(ComponentSerializer.from(buffer.readString()));
        packet.Position(ChatPosition.fromId(buffer.readByte()));
    }
}
