package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.base.meta.Metadata;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.entity.Player;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.codec.play.player.CodecSpawnPlayer;

import java.util.UUID;

@PacketInfo(target = Target.Client, state = State.Play, id = 12, codec = CodecSpawnPlayer.class)
public class PacketSpawnPlayer extends Packet {

    private int entityId;
    private UUID uuid;
    private double x, y, z, yaw, pitch;
    private int heldItem;
    private Metadata metadata;

    public PacketSpawnPlayer(int entityId, UUID uuid, double x, double y, double z, double yaw, double pitch, int heldItem, Metadata metadata) {
        this.entityId = entityId;
        this.uuid = uuid;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.heldItem = heldItem;
        this.metadata = metadata;
    }

    public PacketSpawnPlayer(Player player) {
        EntityId(player.entityId());
        Uuid(player.profile().uuid());
        Position(player.transform().Position());
        Rotation(player.transform().Rotation());
        HeldItem(0);
        Metadata(player.metadata());
    }

    public PacketSpawnPlayer() {}


    public int entityId() {
        return entityId;
    }

    public PacketSpawnPlayer EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public UUID uuid() {
        return uuid;
    }

    public PacketSpawnPlayer Uuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public double x() {
        return x;
    }

    public PacketSpawnPlayer X(double x) {
        this.x = x;
        return this;
    }

    public double y() {
        return y;
    }

    public PacketSpawnPlayer Y(double y) {
        this.y = y;
        return this;
    }

    public double z() {
        return z;
    }

    public PacketSpawnPlayer Z(double z) {
        this.z = z;
        return this;
    }

    public double yaw() {
        return yaw;
    }

    public PacketSpawnPlayer Yaw(double yaw) {
        this.yaw = yaw;
        return this;
    }

    public double pitch() {
        return pitch;
    }

    public PacketSpawnPlayer Pitch(double pitch) {
        this.pitch = pitch;
        return this;
    }

    public int heldItem() {
        return heldItem;
    }

    public PacketSpawnPlayer HeldItem(int heldItem) {
        this.heldItem = heldItem;
        return this;
    }

    public Metadata metadata() {
        return metadata;
    }

    public PacketSpawnPlayer Metadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }

    public PacketSpawnPlayer Position(double x, double y, double z) {
        return X(x).Y(y).Z(z);
    }

    public PacketSpawnPlayer Position(Vector position) {
        return Position(position.x(), position.y(), position.z());
    }

    public Vector position() {
        return new Vector(x, y, z);
    }

    public PacketSpawnPlayer Rotation(double yaw, double pitch) {
        return Yaw(yaw).Pitch(pitch);
    }

    public PacketSpawnPlayer Rotation(Quaternion rotation) {
        Vector euler = rotation.euler();
        return Rotation(euler.y(), euler.x());
    }
}
