package com.sxt.mc.server.base;

public interface Viewable {

    void Add(Observer observer);

    void Remove(Observer observer);
}
