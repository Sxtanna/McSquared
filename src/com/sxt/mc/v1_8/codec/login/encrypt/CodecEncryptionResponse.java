package com.sxt.mc.v1_8.codec.login.encrypt;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionResponse;

public class CodecEncryptionResponse implements Codec<PacketEncryptionResponse> {

    @Override
    public void encode(Buffer buffer, PacketEncryptionResponse packet) {
        buffer.writeByteArray(packet.sharedKey());
        buffer.writeByteArray(packet.vToken());
    }

    @Override
    public void decode(Buffer buffer, PacketEncryptionResponse packet) {
        packet.SharedKey(buffer.readByteArray());
        packet.vToken(buffer.readByteArray());
    }
}
