package com.sxt.mc.util.type;

import com.sxt.mc2.McSquared;

import org.slf4j.Logger;

public interface Logging extends Named {

    @Override
    default String name() {
        return getClass().getSimpleName();
    }

    default void info(String message) {
        logger().info(name() + " " + message);
    }

    default void warn(String message) {
        logger().warn(name() + " " + message);
    }

    default void error(String message) {
        logger().error(name() + " " + message);
    }

    static Logger logger() {
        return McSquared.Logger();
    }
}
