package com.sxt.mc.base;

import com.sxt.mc.base.chat.BaseComponent;
import com.sxt.mc.server.entity.Player;
import com.sxt.mc.session.profile.Profile;

public class PlayerInfo {

    private Profile profile;
    private Gamemode gamemode;
    private Integer ping;
    private BaseComponent displayName;

    public PlayerInfo(Profile profile, Gamemode gamemode, int ping, BaseComponent displayName) {
        this.profile = profile;
        this.gamemode = gamemode;
        this.ping = ping;
        this.displayName = displayName;
    }

    public PlayerInfo(Player player) {
        this(player.profile(), player.gamemode(), 0, null);
    }

    public PlayerInfo(){}

    public Profile profile() {
        return profile;
    }

    public PlayerInfo Profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public Gamemode gamemode() {
        return gamemode;
    }

    public PlayerInfo Gamemode(Gamemode gamemode) {
        this.gamemode = gamemode;
        return this;
    }

    public int ping() {
        return ping == null ? 0 : ping;
    }

    public PlayerInfo Ping(Integer ping) {
        this.ping = ping;
        return this;
    }

    public BaseComponent displayName() {
        return displayName;
    }

    public PlayerInfo DisplayName(BaseComponent displayName) {
        this.displayName = displayName;
        return this;
    }

    public boolean hasDisplayName() {
        return displayName != null;
    }
}
