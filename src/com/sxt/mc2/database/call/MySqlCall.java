package com.sxt.mc2.database.call;

import com.sxt.mc2.database.MySql;
import com.sxt.mc2.database.base.DatabaseCall;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public abstract class MySqlCall extends DatabaseCall<MySqlCall, Connection, MySql> {

    protected Statement statement = null;
    protected PreparedStatement preparedStatement = null;
    protected ResultSet resultSet = null;

    @Override
    public void onFinally() throws Exception {
        if (resultSet != null) resultSet.close();
        if (statement != null) statement.close();
        if (preparedStatement != null) preparedStatement.close();
    }
}
