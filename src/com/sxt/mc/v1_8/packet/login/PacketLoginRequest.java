package com.sxt.mc.v1_8.packet.login;


import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.login.CodecLoginRequest;

@PacketInfo(target = Target.Server, state = State.Login, id = 0, codec = CodecLoginRequest.class)
public class PacketLoginRequest extends Packet {

    private String username;

    public PacketLoginRequest(String username) {
        this.username = username;
    }

    public PacketLoginRequest() {
    }


    public String username() {
        return username;
    }

    public PacketLoginRequest Username(String username) {
        this.username = username;
        return this;
    }
}
