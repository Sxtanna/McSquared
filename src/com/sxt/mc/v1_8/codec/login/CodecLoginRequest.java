package com.sxt.mc.v1_8.codec.login;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.login.PacketLoginRequest;

public class CodecLoginRequest implements Codec<PacketLoginRequest> {

    @Override
    public void encode(Buffer buffer, PacketLoginRequest packet) {
        buffer.writeString(packet.username());
    }

    @Override
    public void decode(Buffer buffer, PacketLoginRequest packet) {
        packet.Username(buffer.readString());
    }
}
