package com.sxt.mc.server.base;

import com.sxt.mc.protocol.base.packet.Packet;

public interface Observer {

    void observe(Packet packet);

    Attributes attributes();
}
