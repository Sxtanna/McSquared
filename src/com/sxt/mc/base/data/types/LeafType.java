package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum  LeafType implements MaterialTypeData<LeafType> {

    Oak,
    Spruce,
    Birch,
    Jungle;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public LeafType byData(int data) {
        if (data < 0 || data > 3) return Oak;
        return values()[data];
    }
}
