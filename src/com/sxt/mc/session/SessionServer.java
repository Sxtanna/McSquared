package com.sxt.mc.session;

import com.sxt.mc2.McSquared;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;

public class SessionServer {

    private static final AttributeKey<Session> SessionKey = AttributeKey.newInstance("session");

    public Session get(Channel channel) {
        Attribute<Session> attribute = channel.attr(SessionKey);
        Session session = attribute.get();

        if (session == null) {
            attribute.setIfAbsent(new Session(channel, McSquared.Server().Protocol()));
            session = attribute.get();
        }

        return session;
    }

    public Session get(ChannelHandlerContext context) {
        return get(context.channel());
    }
}
