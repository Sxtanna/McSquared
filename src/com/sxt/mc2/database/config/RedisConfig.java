package com.sxt.mc2.database.config;

import com.sxt.mc2.config.JsonConfig;

import java.io.File;

public class RedisConfig extends JsonConfig<RedisConfig> {

    public static final RedisConfig Default = new RedisConfig(" ", " ", 6379, 0, 200, 200);

    private String Address, Auth;
    private Integer Port, Timeout, MaxTotal, MaxIdle;

    public RedisConfig(String address, String auth, int port, int timeout, int maxTotal, int maxIdle) {
        super("Redis");
        Address = address;
        Auth = auth;
        Port = port;
        Timeout = timeout;
        MaxTotal = maxTotal;
        MaxIdle = maxIdle;

        save();
    }

    public RedisConfig(File file, String address, String auth, Integer port, Integer timeout, Integer maxTotal, Integer maxIdle) {
        super(file);
        Address = address;
        Auth = auth;
        Port = port;
        Timeout = timeout;
        MaxTotal = maxTotal;
        MaxIdle = maxIdle;

        save();
    }

    public RedisConfig(File file) {
        this(file, " ", " ", 6379, 0, 200, 200);
        save();
    }

    public String Address() {
        return Address;
    }

    public String Auth() {
        return Auth;
    }

    public int Port() {
        return Port;
    }

    public int Timeout() {
        return Timeout;
    }

    public int MaxTotal() {
        return MaxTotal;
    }

    public int MaxIdle() {
        return MaxIdle;
    }

    @Override
    public Class<? extends JsonConfig> type() {
        return RedisConfig.class;
    }

    public static RedisConfig get() {
        return JsonConfig.loadConfig(Default);
    }
}
