package com.sxt.mc.server.level;

public class Chunk {

    private final com.sxt.mc.server.level.Level Level;
    private final int X, Z;
    private ChunkSection[] sections;

    public Chunk(Level level, int x, int z) {
        Level = level;
        X = x;
        Z = z;

        this.sections = new ChunkSection[16];
    }

    public Level Level() {
        return Level;
    }

    public int X() {
        return X;
    }

    public int Z() {
        return Z;
    }

    public ChunkSection[] sections() {
        return sections;
    }

    public void fill(int level, int typeId, int meta) {
        if (level < 0 || level > 15) return;

        ChunkSection section = sections()[level];
        if (section == null) sections[level] = section = new ChunkSection(level, true);

        int block = (typeId << 4) | (meta & 15);

        for (int i = 0; i < section.blocks().length; i++) {
            section.blocks()[i] = ((char) block);
        }
    }


    // TODO: 7/29/2016 Add Block API
}
