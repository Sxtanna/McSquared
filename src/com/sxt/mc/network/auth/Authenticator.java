package com.sxt.mc.network.auth;

import com.sxt.mc.base.chat.TextComponent;
import com.sxt.mc.session.Session;
import com.sxt.mc.session.profile.Profile;
import com.sxt.mc.util.C;
import com.sxt.mc.util.F;
import com.sxt.mc.util.type.Gsonable;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.event.types.player.PlayerLoginEvent;
import com.sxt.mc2.protocol.packet.login.PacketKick;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

public class Authenticator implements Runnable, Logging {

    private static final String AuthSessionUrl = "https://sessionserver.mojang.com/session/minecraft/hasJoined";

    private final Session Session;
    private final String Hash;

    public Authenticator(Session sessions, String hash) {
        Session = sessions;
        Hash = hash;
    }

    @Override
    public void run() {
        try {
            String username = Session.vUsername(), url = AuthSessionUrl + "?username=" + username + "&serverId="+ Hash;

            URLConnection connection = new URL(url).openConnection();

            String output = "";

            try(BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                String input;
                while ((input = in.readLine()) != null) output += input;
            }
            catch (Exception e) {
                warn("Failed to read Auth Input");
                e.printStackTrace();
            }

            AuthResponse response = Gsonable.from(output, AuthResponse.class);

            UUID uuid = F.uuid(response.id());
            if (uuid == null) Session.disconnect(new PacketKick(new TextComponent("Invalid UUID")));
            else {
                Profile profile = new Profile(response.name(), uuid, response.properties());
                Session.Profile(profile);

                PlayerLoginEvent loginEvent = McSquared.Events().call(new PlayerLoginEvent(Session));
                if (!loginEvent.cancelled()) return;

                Session.disconnect(new PacketKick(new TextComponent(F.color(loginEvent.kickMessage()))));
            }
        }
        catch (Exception e) {
            error("Failed to Authenticate " + Session.vUsername());
            e.printStackTrace();
            Session.disconnect(new PacketKick(new TextComponent("Internal Error " + e.getMessage())));
        }
    }
}
