package com.sxt.mc.server.level;

public class ChunkSection {

    private int position;
    private char[] blocks;
    private boolean skylight;

    public ChunkSection(int position, boolean skylight) {
        this.position = position;
        this.skylight = skylight;

        this.blocks = new char[4096];
    }

    public int position() {
        return position;
    }

    public char[] blocks() {
        return blocks;
    }

    public boolean skylight() {
        return skylight;
    }

    public byte atPos(int x, int y, int z) {
        char c = blocks()[y << 8 | z << 4 | x];
        return ((byte) c);
    }
}
