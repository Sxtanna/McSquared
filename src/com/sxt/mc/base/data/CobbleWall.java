package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.CobbleWallType;

public class CobbleWall extends MaterialData<CobbleWall, CobbleWallType> {

    public CobbleWall(int data) {
        super(Material.CobbleWall, data);
    }

    public CobbleWall() {
        super(Material.CobbleWall);
    }

    public CobbleWall(CobbleWallType data) {
        super(Material.CobbleWall, data);
    }
}
