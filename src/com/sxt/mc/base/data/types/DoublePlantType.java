package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum DoublePlantType implements MaterialTypeData<DoublePlantType> {

    Lilac,
    TallGrass,
    Fern,
    RoseBush,
    Peony;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public DoublePlantType byData(int data) {
        if (data < 0 || data > 3) return Lilac;
        return values()[data];
    }
}
