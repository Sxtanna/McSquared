package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.FlowerType;

public class Flower extends MaterialData<Sand, FlowerType> {

    public Flower(int data) {
        super(Material.RedRose, data);
    }

    public Flower() {
        super(Material.RedRose);
    }

    public Flower(FlowerType data) {
        super(Material.RedRose, data);
    }
}
