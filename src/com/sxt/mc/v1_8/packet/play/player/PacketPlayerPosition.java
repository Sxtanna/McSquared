package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayerPosition;

@PacketInfo(target = Target.Server, state = State.Play, id = 4, codec = CodecPlayerPosition.class)
public class PacketPlayerPosition extends Packet {

    private double x, y, z;
    private boolean onGround;

    public PacketPlayerPosition(double x, double y, double z, boolean onGround) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.onGround = onGround;
    }

    public PacketPlayerPosition() {}


    public double x() {
        return x;
    }

    public PacketPlayerPosition X(double x) {
        this.x = x;
        return this;
    }

    public double y() {
        return y;
    }

    public PacketPlayerPosition Y(double y) {
        this.y = y;
        return this;
    }

    public double z() {
        return z;
    }

    public PacketPlayerPosition Z(double z) {
        this.z = z;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketPlayerPosition OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }

    public Vector position() {
        return new Vector(x(), y(), z());
    }
    
    
}
