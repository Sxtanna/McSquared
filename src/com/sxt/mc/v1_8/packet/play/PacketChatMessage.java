package com.sxt.mc.v1_8.packet.play;
import com.sxt.mc.base.ChatPosition;
import com.sxt.mc.base.chat.BaseComponent;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.CodecChatMessage;

@PacketInfo(target = Target.Client, state = State.Play, id = 2, codec = CodecChatMessage.class)
public class PacketChatMessage extends Packet {

    private BaseComponent message;
    private ChatPosition position;

    public PacketChatMessage(BaseComponent message, ChatPosition position) {
        this.message = message;
        this.position = position;
    }

    public PacketChatMessage() {}


    public BaseComponent message() {
        return message;
    }

    public PacketChatMessage Message(BaseComponent message) {
        this.message = message;
        return this;
    }

    public ChatPosition position() {
        return position;
    }

    public PacketChatMessage Position(ChatPosition position) {
        this.position = position;
        return this;
    }
}
