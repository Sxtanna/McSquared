package com.sxt.mc.util.uU;


import com.sxt.mc.util.F;

public class MathU {

    public static final double DoubleRoundingError = 0.00000000001;
    public static final double RadiansToDegrees = 180. / Math.PI, DegreesToRadians = Math.PI / 180.;

    static public boolean isZero(double value) {
        return Math.abs(value) <= DoubleRoundingError;
    }

    public static float clamp(float value, float min, float max) {
        return F.clamp(value, min, max);
    }
}
