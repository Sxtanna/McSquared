package com.sxt.mc2.config.server;

import com.sxt.mc.base.Gamemode;
import com.sxt.mc.util.F;

public enum Property {

    ShowThread(PropertyType.Boolean, false),
    ShowDate(PropertyType.Boolean, false),
    ShowTime(PropertyType.Boolean, true),
    DateFormat(PropertyType.String, "MM-dd-yy"),
    TimeFormat(PropertyType.String, "HH:mm:ss"),
    DefaultGamemode(PropertyType.GameMode, Gamemode.Survival.name());

    private PropertyType type;
    private Object defaultValue;

    Property(PropertyType type, Object defaultValue) {
        this.type = type;
        this.defaultValue = defaultValue;
    }

    public Object decode(String input) {
        return type.decode(input);
    }

    public Object defaultValue() {
        return defaultValue;
    }

    enum PropertyType {
        Boolean() {
            @Override
            public Object decode(String ser) {
                return F.bool(ser);
            }
        },
        String() {
            @Override
            public Object decode(String ser) {
                return ser;
            }
        },
        GameMode() {
            @Override
            public Object decode(String ser) {
                if (F.numeric(ser)) return Gamemode.fromId(F.number(ser).intValue());

                for (Gamemode gamemode : Gamemode.values())
                    if (F.equals(gamemode.name(), ser)) return gamemode;
                return Gamemode.Survival;
            }
        };

        public abstract Object decode(String ser);
    }

}
