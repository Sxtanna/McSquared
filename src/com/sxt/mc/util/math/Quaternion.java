package com.sxt.mc.util.math;

import com.sxt.mc.util.uU.MathU;

import static com.sxt.mc.util.uU.MathU.DegreesToRadians;
import static com.sxt.mc.util.uU.MathU.RadiansToDegrees;
import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Quaternion extends Vector4<Quaternion> {

    public Quaternion(double x, double y, double z, double w) {
        super(x, y, z, w);
    }

    public Quaternion() {}

    public Quaternion(Quaternion other) {
        super(other);
    }

    public void Axis(double x, double y, double z, double angle) {
        angle *= DegreesToRadians;

        double length = Vector.length(x, y, z);
        if (length < MathU.DoubleRoundingError) {
            this.x = 0.0f;
            this.y = 0.0f;
            this.z = 0.0f;
            this.w = 1.0f;
        } else {
            double sin_half_angle = sin(angle / 2.);
            this.x = sin_half_angle * x / length;
            this.y = sin_half_angle * y / length;
            this.z = sin_half_angle * z / length;
            this.w = cos(angle / 2.);
        }
    }

    public void Euler(double pitch, double yaw, double roll) {
        Quaternion qx = fromAxis(1, 0, 0, pitch);
        Quaternion qy = fromAxis(0, 1, 0, yaw);
        Quaternion qz = fromAxis(0, 0, 1, roll);
        Set(qy);
        multiply(qz);
        multiply(qx);
    }

    @Override
    public Quaternion Set(Quaternion other) {
        return super.Set(other);
    }

    public Vector euler() {
        double pitch, yaw, roll;
        double test = x * y + z * w;
        if (test > 0.499) {
            yaw = 2 * atan2(x, w);
            roll = PI / 2;
            pitch = 0;
            return new Vector(pitch * RadiansToDegrees, yaw * RadiansToDegrees, roll * RadiansToDegrees);
        }
        if (test < -0.499) {
            yaw = -2 * atan2(x, w);
            roll = -PI / 2;
            pitch = 0;
            return new Vector(pitch * RadiansToDegrees, yaw * RadiansToDegrees, roll * RadiansToDegrees);
        }
        double sqx = x * x, sqy = y * y, sqz = z * z;

        yaw = atan2(2 * y * w - 2 * x * z, 1 - 2 * sqy - 2 * sqz);
        roll = asin(2 * test);
        pitch = atan2(2 * x * w - 2 * y * z, 1 - 2 * sqx - 2 * sqz);
        return new Vector(pitch * RadiansToDegrees, yaw * RadiansToDegrees, roll * RadiansToDegrees);
    }

    public double pitch() {
        return euler().x();
    }

    public double yaw() {
        return euler().y();
    }

    public void multiply(Quaternion other) {
        final double newX = this.w * other.x + this.x * other.w + this.y * other.z - this.z * other.y;
        final double newY = this.w * other.y + this.y * other.w + this.z * other.x - this.x * other.z;
        final double newZ = this.w * other.z + this.z * other.w + this.x * other.y - this.y * other.x;
        final double newW = this.w * other.w - this.x * other.x - this.y * other.y - this.z * other.z;
        this.x = newX;
        this.y = newY;
        this.z = newZ;
        this.w = newW;
    }

    public final double normalize() {
        double norm = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
        if (norm > 0.0f) {
            this.x /= norm;
            this.y /= norm;
            this.z /= norm;
            this.w /= norm;
        } else {
            this.x = 0.0;
            this.y = 0.0;
            this.z = 0.0;
            this.w = 1.0;
        }
        return norm;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Quaternion)) return false;

        return ((Quaternion) o).euler().equals(euler());
    }

    @Override
    public Class<? extends Vector> type() {
        return Quaternion.class;
    }

    public static Quaternion fromEuler(double pitch, double yaw, double roll) {
        Quaternion quaternion = new Quaternion();
        quaternion.Euler(pitch, yaw, roll);
        return quaternion;
    }

    public static Quaternion fromAxis(double x, double y, double z, double angle) {
        Quaternion quaternion = new Quaternion();
        quaternion.Axis(x, y, z, angle);
        return quaternion;
    }

    public static Quaternion identity() {
        return new Quaternion(0, 0, 0, 1);
    }
}
