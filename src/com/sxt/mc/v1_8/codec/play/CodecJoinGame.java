package com.sxt.mc.v1_8.codec.play;

import com.sxt.mc.base.Difficulty;
import com.sxt.mc.base.Dimension;
import com.sxt.mc.base.Gamemode;
import com.sxt.mc.base.LevelType;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.PacketJoinGame;

public class CodecJoinGame implements Codec<PacketJoinGame> {

    @Override
    public void encode(Buffer buffer, PacketJoinGame packet) {
        buffer.writeInt(packet.entityId());
        buffer.writeByte(packet.gamemode().id() | (packet.hardcore() ? 0x8 : 0));
        buffer.writeByte(packet.dimension().id());
        buffer.writeByte(packet.difficulty().id());
        buffer.writeByte(packet.maxPlayers());
        buffer.writeString(packet.levelType().levelName());
        buffer.writeBoolean(packet.reducedDebugInfo());
    }

    @Override
    public void decode(Buffer buffer, PacketJoinGame packet) {
        packet.EntityId(buffer.readInt());

        int gamemode = buffer.readByte();
        packet.Gamemode(Gamemode.fromId(gamemode & 0x7));
        packet.Hardcore((gamemode & 0x8) != 0);

        packet.Dimension(Dimension.fromId(buffer.readByte()));
        packet.Difficulty(Difficulty.fromId(buffer.readByte()));
        packet.MaxPlayers(buffer.readByte());
        packet.LevelType(LevelType.byName(buffer.readString()));
        packet.ReducedDebugInfo(buffer.readBoolean());
    }
}
