package com.sxt.mc2.event.types.packet;

import com.sxt.mc.protocol.base.packet.Packet;

public class PacketInEvent extends PacketEvent<PacketInEvent> {

    private boolean cancelled = false;

    public PacketInEvent(Packet packet) {
        super(packet);
    }

    @Override
    public PacketInEvent Cancelled(boolean state) {
        this.cancelled = state;
        return thisClass();
    }

    @Override
    public boolean cancelled() {
        return cancelled;
    }
}
