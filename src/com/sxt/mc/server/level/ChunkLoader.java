package com.sxt.mc.server.level;

import com.sxt.mc.v1_8.packet.play.chunk.PacketChunk;
import com.sxt.mc.v1_8.packet.play.chunk.PacketChunkBulk;
import com.sxt.mc2.event.base.EventPriority;
import com.sxt.mc2.event.base.EventWatcher;
import com.sxt.mc2.event.types.player.PlayerLoginEvent;
import com.sxt.mc2.task.McTask;

import java.util.ArrayList;
import java.util.List;

public class ChunkLoader {

    private static final int ChunksPerTask = 8;

    private final Level Level;

    public ChunkLoader(Level level) {
        Level = level;
    }

    public Level Level() {
        return Level;
    }

    @EventWatcher(priority = EventPriority.Monitor)
    public void onJoin(PlayerLoginEvent e) {
        if (e.cancelled()) return;

        new McTask() {

            List<Chunk> chunks = Level().chunks();
            int runs = chunks.size() / ChunksPerTask + chunks.size() % ChunksPerTask, on = 0;

            @Override
            public void run() {
                if (on >= runs) Cancel();

                PacketChunkBulk bulk = new PacketChunkBulk();
                bulk.Skylight(true);

                List<PacketChunk> packetChunks = new ArrayList<>();

                int current = on++ * ChunksPerTask;

                for (int i = 0; i < ChunksPerTask; i++) {
                    if (current + i >= chunks.size()) break;
                    Chunk chunk = chunks.get(current + i);
                    packetChunks.add(new PacketChunk(chunk));
                }

                bulk.Chunks(packetChunks);
                e.session().send(bulk);
            }
        }.goRepeat(50);
    }
}
