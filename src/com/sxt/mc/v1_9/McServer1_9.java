package com.sxt.mc.v1_9;

import com.sxt.mc.base.Version;
import com.sxt.mc.log.LogProvider;
import com.sxt.mc.network.NetworkServer;
import com.sxt.mc.protocol.event.PacketBus;
import com.sxt.mc.server.level.Level;
import com.sxt.mc.session.SessionServer;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.config.server.ServerConfig;
import com.sxt.mc2.event.EventBus;
import org.slf4j.Logger;

import java.io.File;
import java.util.Map;
import java.util.UUID;

final class McServer1_9 extends Version {

    private final LogProvider logProvider;
    private final Logger logger;

    public McServer1_9(ServerConfig config) {
        super(config);

        this.logProvider = new LogProvider();
        this.logger = logProvider.get(name());

    }

    @Override
    public String name() {
        return getClass().getSimpleName();
    }

    @Override
    public LogProvider LogProvider() {
        return logProvider;
    }

    @Override
    public Logger Logger() {
        return logger;
    }

    @Override
    public EventBus Events() {
        return null;
    }

    @Override
    public PacketBus Packets() {
        return null;
    }

    @Override
    public File ServerFolder() {
        return McSquared.ServerFolder();
    }

    @Override
    public File DataFolder() {
        return McSquared.DataFolder();
    }

    @Override
    public NetworkServer Network() {
        return null;
    }

    @Override
    public SessionServer Sessions() {
        return null;
    }

    @Override
    public Map<UUID, Level> Levels() {
        return null;
    }
}
