package com.sxt.mc.util.uU;

import com.sxt.mc.base.Color;
import com.sxt.mc.util.F;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class RandomU {

    public static final Random Random = new Random(System.nanoTime());

    public static double angle() {
        return Random.nextDouble() * 2.0D * 3.141592653589793D;
    }

    public static float gFloat(float range) {
        return 0.0f + (range) * Random.nextFloat();
    }

    public static float gFloat(float min, float max) {
        return min + (max - min) * Random.nextFloat();
    }

    public static double gDouble(double range) {
        return 0.0d + (range) * Random.nextDouble();
    }

    public static double gDouble(double min, double max) {
        return min + (max - min) * Random.nextDouble();
    }

    public static int gInt(int range) {
        return Random.nextInt(range);
    }

    public static int gInt(int min, int max) {
        return Random.nextInt((max - min) + 1) + min;
    }

    public static long gLong(long range) {
        return (range) * Random.nextLong();
    }

    public static long gLong(long min, long max) {
        return min + (max - min) * Random.nextLong();
    }

    public static Color color() {
        return element(F.enumStream(Color.class).filter(Color::color).collect(Collectors.toList()));
    }

    /**
     * Get a random chance boolean
     *
     * @param chance chance 0 - 100
     * @return The Choice
     */
    public static Boolean choice(int chance) {
        return chance > 100 || gInt(100) < chance;
    }

    public static <T> T element(Set<T> elements) {
        int index = gInt(elements.size());
        Iterator<T> iter = elements.iterator();
        for (int i = 0; i < index; i++) iter.next();
        return iter.next();
    }

    public static <T> T element(List<T> elements) {
        return elements.get(gInt(elements.size()));
    }

    public static <T> T element(T[] elements) {
        return elements[gInt(elements.length)];
    }
}
