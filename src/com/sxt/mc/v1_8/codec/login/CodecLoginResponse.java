package com.sxt.mc.v1_8.codec.login;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.util.F;
import com.sxt.mc.v1_8.packet.login.PacketLoginResponse;

public class CodecLoginResponse implements Codec<PacketLoginResponse> {

    @Override
    public void encode(Buffer buffer, PacketLoginResponse packet) {
        buffer.writeString(F.uuid(packet.uuid(), false));
        buffer.writeString(packet.username());
    }

    @Override
    public void decode(Buffer buffer, PacketLoginResponse packet) {
        packet.Uuid(F.uuid(buffer.readString()));
        packet.Username(buffer.readString());
    }
}
