package com.sxt.mc2.database;

import com.sxt.mc.util.task.CallEx;
import com.sxt.mc2.database.base.Database;
import com.sxt.mc2.database.call.MySqlCall;
import com.sxt.mc2.database.config.MySqlConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class MySql extends Database<MySql, HikariDataSource, Connection> {

    private HikariDataSource Pool;
    private final MySqlConfig Config;

    public MySql(MySqlConfig config) {
        Config = config;
    }

    public MySql() {
        Config = MySqlConfig.get();
    }

    @Override
    protected void load() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://" + Config.Address() + ":" + Config.Port() + "/" + Config.Database());
        config.setUsername(Config.Username());
        config.setPassword(Config.Password());
        config.setMaximumPoolSize(Config.MaxPoolSize());
        config.setConnectionTimeout(Config.ConnectionTimeout());
        config.setIdleTimeout(Config.IdleTimeout());

        Pool = new HikariDataSource(config);

        loaded = true;
        execute(data -> connected = true);
    }

    @Override
    protected void poison() throws Exception {
        Pool.close();
    }

    @Override
    public Connection resource() {
        try {
            return pool().getConnection();
        } catch (SQLException e) {
            error("Failed to get SQL Connection");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public HikariDataSource pool() {
        return Pool;
    }

    @Override
    public void execute(CallEx<Connection, Exception> call) {
        try {
            Executor.execute(new MySqlCall() {

                @Override
                public void callEx(Connection data) throws Exception {
                    call.call(data);
                }

                @Override
                public MySql database() {
                    return MySql.this;
                }
            });
        } catch (Exception e) {
            call.fail(e);
        }
    }
}
