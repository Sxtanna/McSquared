package com.sxt.mc.protocol.base.packet.client;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.codec.client.CodecKeepAlive;
import com.sxt.mc.protocol.base.packet.Packet;

@PacketInfo(target = {Target.Client, Target.Server}, state = State.Play, id = 0, codec = CodecKeepAlive.class)
public class PacketKeepAlive extends Packet {

    private int keepAliveId;

    public PacketKeepAlive(int keepAliveId) {
        this.keepAliveId = keepAliveId;
    }

    public PacketKeepAlive() {}


    public int keepAliveId() {
        return keepAliveId;
    }

    public PacketKeepAlive KeepAliveId(int keepAliveId) {
        this.keepAliveId = keepAliveId;
        return this;
    }
}
