package com.sxt.mc.protocol.base.codec;

import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.Registry;

public class CodecRegistry extends Registry<Codec<? extends Packet>> {

    private final State state;

    public CodecRegistry(State state) {
        this.state = state;
    }

    public State state() {
        return state;
    }
}
