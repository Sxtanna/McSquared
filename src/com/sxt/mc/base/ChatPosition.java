package com.sxt.mc.base;

public enum ChatPosition {

    Chat,
    System,
    ActionBar;

    public int id() {
        return ordinal();
    }

    public static ChatPosition fromId(int id) {
        if (id < 0 || id >= values().length) return null;
        return values()[id];
    }

}
