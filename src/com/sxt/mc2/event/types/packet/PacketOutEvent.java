package com.sxt.mc2.event.types.packet;

import com.sxt.mc.protocol.base.packet.Packet;

public class PacketOutEvent extends PacketEvent<PacketOutEvent> {

    private boolean cancelled = false;

    public PacketOutEvent(Packet packet) {
        super(packet);
    }

    @Override
    public PacketOutEvent Cancelled(boolean state) {
        this.cancelled = state;
        return thisClass();
    }

    @Override
    public boolean cancelled() {
        return cancelled;
    }
}
