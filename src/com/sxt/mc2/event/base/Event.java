package com.sxt.mc2.event.base;

import com.sxt.mc.event.subscribe.Subscribable;

public abstract class Event implements Subscribable {

    @Override
    public String name() {
        return getClass().getSimpleName();
    }
}
