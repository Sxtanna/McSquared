package com.sxt.mc.base.component;

import com.sxt.mc.util.F;

import java.util.UUID;

public class StringComponent extends PlayerComponent {

    public StringComponent(String info) {
        super(F.color(info), UUID.randomUUID().toString());
    }
}
