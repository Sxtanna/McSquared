package com.sxt.mc2.protocol.packet.login;

import com.sxt.mc.base.chat.BaseComponent;
import com.sxt.mc.base.chat.TextComponent;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.codec.client.CodecKick;
import com.sxt.mc.protocol.base.packet.Packet;

@PacketInfo(target = Target.Client, state = State.Login, id = 0, codec = CodecKick.class)
public class PacketKick extends Packet {

    private BaseComponent message;

    public PacketKick(BaseComponent message) {
        this.message = message;
    }

    public PacketKick(String message) {
        this(new TextComponent(message));
    }

    public PacketKick() {
    }


    public BaseComponent message() {
        return message;
    }

    public PacketKick Message(BaseComponent message) {
        this.message = message;
        return this;
    }
}
