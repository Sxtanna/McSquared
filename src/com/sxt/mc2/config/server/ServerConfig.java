package com.sxt.mc2.config.server;

import com.sxt.mc.base.Dimension;
import com.sxt.mc.base.Gamemode;
import com.sxt.mc.base.LevelType;
import com.sxt.mc.base.WorldType;
import com.sxt.mc.util.F;
import com.sxt.mc2.config.JsonConfig;

import java.util.HashMap;
import java.util.Map;

public final class ServerConfig extends JsonConfig<ServerConfig> {

    private static transient final ServerConfig Default = new ServerConfig(25565, 47, 100, "", "An McSquared Server", true);

    private Integer Port, Protocol, MaxPlayers;
    private String ServerName, ServerMotd;
    private Boolean DefaultServer;
    private Map<String, WorldType> Worlds = new HashMap<>();
    private Map<Property, Object> Properties = new HashMap<>();

    public ServerConfig(int port, int protocol, int maxPlayers, String name, String motd, boolean defaultServer) {
        super("Server");
        this.Port = port;
        this.Protocol = protocol;
        this.MaxPlayers = maxPlayers;
        this.ServerName = name;
        this.ServerMotd = motd;
        this.DefaultServer = defaultServer;

        Worlds.put("World", new WorldType(Dimension.Overworld, LevelType.Default));
        for (Property property : Property.values())
            Properties.put(property, property.defaultValue());
    }

    public int port() {
        if (Port == null) Port(Default.port());
        return Port;
    }

    public ServerConfig Port(int port) {
        this.Port = port;
        return save();
    }

    public int protocol() {
        if (Protocol == null) Protocol(Default.protocol());
        return Protocol;
    }

    public ServerConfig Protocol(int protocol) {
        this.Protocol = protocol;
        return save();
    }

    public int maxPlayers() {
        if (MaxPlayers == null) MaxPlayers(Default.maxPlayers());
        return MaxPlayers;
    }

    public ServerConfig MaxPlayers(int maxPlayers) {
        this.MaxPlayers = maxPlayers;
        return save();
    }

    @Override
    public String name() {
        if (ServerName == null) Name(Default.name());
        return ServerName;
    }

    public ServerConfig Name(String name) {
        this.ServerName = name;
        return save();
    }

    public String motd() {
        if (ServerMotd == null) Motd(Default.motd());
        return F.color(ServerMotd);
    }

    public ServerConfig Motd(String motd) {
        this.ServerMotd = motd;
        return save();
    }

    public boolean defaultServer() {
        if (DefaultServer == null) DefaultServer(Default.defaultServer());
        return DefaultServer;
    }

    public ServerConfig DefaultServer(boolean state) {
        this.DefaultServer = state;
        return save();
    }

    public Map<String, WorldType> Worlds() {
        if (Worlds == null) Worlds = Default.Worlds();
        return Worlds;
    }

    public Gamemode gamemode() {
        return ((Gamemode) Properties.get(Property.DefaultGamemode));
    }

    public ServerConfig Gamemode(Gamemode gamemode) {
        Properties.put(Property.DefaultGamemode, gamemode);
        return save();
    }

    public Map<Property, Object> Properties() {
        if (Properties == null) Properties = Default.Properties();
        return Properties;
    }

    public Object property(Property property) {
        return Properties().get(property);
    }

    private ServerConfig decodeProperties() {
        F.enumStream(Property.class).forEach(sProp -> Properties().compute(sProp, (prop, obj) -> obj == null ? sProp.defaultValue() : sProp.decode(F.string(obj))));
        return thisClass();
    }

    @Override
    public Class<? extends ServerConfig> type() {
        return ServerConfig.class;
    }

    public static ServerConfig get() {
        return JsonConfig.loadConfig(Default).decodeProperties();
    }
}
