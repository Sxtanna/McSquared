package com.sxt.mc.util.type;

public interface MaterialTypeData<M extends MaterialTypeData> extends Builder<M> {

    int data();

    M byData(int data);
}
