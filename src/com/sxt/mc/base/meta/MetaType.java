package com.sxt.mc.base.meta;

import com.sxt.mc.base.BlockPosition;
import com.sxt.mc.base.ItemSlot;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.util.math.Vector;

import java.io.IOException;

public enum MetaType {

    Byte          ((buf, obj) -> buf.writeByte(((byte) obj))     , Buffer::readByte),
    Short         ((buf, obj) -> buf.writeShort(((short) obj))   , Buffer::readShort),
    Int           ((buf, obj) -> buf.writeInt(((int) obj))       , Buffer::readInt),
    Float         ((buf, obj) -> buf.writeFloat(((float) obj))   , Buffer::readFloat),
    String        ((buf, obj) -> buf.writeString(((String) obj)) , Buffer::readString),
    ItemSlot      ((buf, obj) -> ((ItemSlot) obj).write(buf)     , buf -> new ItemSlot().read(buf)),
    BlockPosition ((buf, obj) -> {
        BlockPosition pos = ((BlockPosition) obj);
        buf.writeInt(pos.x());
        buf.writeInt(pos.y());
        buf.writeInt(pos.z());
    }, buf -> new BlockPosition(buf.readInt(), buf.readInt(), buf.readInt())),
    Rotation      ((buf, obj) -> {
        Vector vector = ((Vector) obj);

        buf.writeFloat((float) vector.x());
        buf.writeFloat((float) vector.y());
        buf.writeFloat((float) vector.z());
    }, buf -> new Vector(buf.readFloat(), buf.readFloat(), buf.readFloat()));


    private WriteHandler writeHandler;
    private ReadHandler readHandler;

    MetaType(WriteHandler writeHandler, ReadHandler readHandler) {
        this.writeHandler = writeHandler;
        this.readHandler = readHandler;
    }

    public void write(Buffer buf, Object obj) {
        try {
            writeHandler.write(buf, obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object read(Buffer buf) {
        try {
            return readHandler.read(buf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int id() {
        return ordinal();
    }

    public static MetaType fromId(int id) {
        return values()[id];
    }

    private interface WriteHandler {
        void write(Buffer buf, Object obj) throws IOException;
    }

    private interface ReadHandler {
        Object read(Buffer buf) throws IOException;
    }
}
