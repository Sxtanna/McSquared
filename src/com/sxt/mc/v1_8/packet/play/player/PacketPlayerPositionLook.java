package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayerPositionLook;

@PacketInfo(target = Target.Server, state = State.Play, id = 6, codec = CodecPlayerPositionLook.class)
public class PacketPlayerPositionLook extends Packet {

    private double x, y, z, yaw, pitch;
    private boolean onGround;

    public PacketPlayerPositionLook(double x, double y, double z, double yaw, double pitch, boolean onGround) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    public PacketPlayerPositionLook() {}


    public double x() {
        return x;
    }

    public PacketPlayerPositionLook X(double x) {
        this.x = x;
        return this;
    }

    public double y() {
        return y;
    }

    public PacketPlayerPositionLook Y(double y) {
        this.y = y;
        return this;
    }

    public double z() {
        return z;
    }

    public PacketPlayerPositionLook Z(double z) {
        this.z = z;
        return this;
    }

    public double yaw() {
        return yaw;
    }

    public PacketPlayerPositionLook Yaw(double yaw) {
        this.yaw = yaw;
        return this;
    }

    public double pitch() {
        return pitch;
    }

    public PacketPlayerPositionLook Pitch(double pitch) {
        this.pitch = pitch;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketPlayerPositionLook OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }

    public Vector position() {
        return new Vector(x(), y(), z());
    }

    public Quaternion rotation() {
        return Quaternion.fromEuler(pitch(), yaw(), 0);
    }
    
    
}
