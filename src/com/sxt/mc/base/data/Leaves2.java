package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;
import com.sxt.mc.util.F;

public class Leaves2 extends MaterialData<Leaves2, WoodType> {

    public Leaves2(int data) {
        super(Material.Leaves2, F.clamp(data, 0,  1));
    }

    public Leaves2() {
        super(Material.Leaves2);
    }

    public Leaves2(WoodType data) {
        super(Material.Leaves2, data);
    }
}
