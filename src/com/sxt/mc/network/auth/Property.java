package com.sxt.mc.network.auth;

public class Property {

    private String signature, name, value;

    public Property(String signature, String name, String value) {
        this.signature = signature;
        this.name = name;
        this.value = value;
    }

    public String signature() {
        return signature;
    }

    public String name() {
        return name;
    }

    public String value() {
        return value;
    }

    public boolean signed() {
        return signature() != null;
    }
}
