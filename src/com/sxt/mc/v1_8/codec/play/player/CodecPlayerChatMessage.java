package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerChatMessage;

public class CodecPlayerChatMessage implements Codec<PacketPlayerChatMessage> {

    @Override
    public void encode(Buffer buffer, PacketPlayerChatMessage packet) {
        buffer.writeString(packet.message());
    }

    @Override
    public void decode(Buffer buffer, PacketPlayerChatMessage packet) {
        packet.Message(buffer.readString());
    }
}
