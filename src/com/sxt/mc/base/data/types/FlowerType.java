package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum FlowerType implements MaterialTypeData<FlowerType> {

    Poppy,
    BlueOrchid,
    Allium,
    AzureBluet,
    RedTulip,
    OrangeTulip,
    WhiteTulip,
    PinkTulip,
    OxeyeDaisy;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public FlowerType byData(int data) {
        if (data < 0 || data > 8) return Poppy;
        return values()[data];
    }
}
