package com.sxt.mc2.event.base;

import com.sxt.mc.util.type.Builder;

public interface Cancellable<B> extends Builder<B> {

    B Cancelled(boolean state);

    boolean cancelled();
}
