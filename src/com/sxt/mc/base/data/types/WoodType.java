package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum WoodType implements MaterialTypeData<WoodType> {

    Oak,
    Spruce,
    Birch,
    Jungle,
    Acacia,
    DarkOak;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public WoodType byData(int data) {
        if (data < 0 || data > 5) return Oak;
        return values()[data];
    }
}
