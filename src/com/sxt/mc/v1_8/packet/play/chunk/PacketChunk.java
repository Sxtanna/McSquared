package com.sxt.mc.v1_8.packet.play.chunk;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.level.Chunk;
import com.sxt.mc.server.level.ChunkSection;
import com.sxt.mc.v1_8.codec.play.chunk.CodecChunk;

import java.util.ArrayList;
import java.util.List;

@PacketInfo(target = Target.Client, state = State.Play, id = 33, codec = CodecChunk.class)
public class PacketChunk extends Packet {

    private int x, z, mask;
    private boolean continuous = true;
    private byte[] data;

    public PacketChunk(int x, int z, int mask, boolean continuous, byte[] data) {
        this.x = x;
        this.z = z;
        this.mask = mask;
        this.continuous = continuous;
        this.data = data;
    }

    public PacketChunk(Chunk chunk) {
        this(chunk.X(), chunk.Z(), 0, true, null);

        List<ChunkSection> chunkSections = new ArrayList<>();
        ChunkSection[] sections = chunk.sections();

        int i;

        //info("Looping over sections");
        for (i = 0; i < sections.length; i++) {
            //info("On section " + i);
            ChunkSection section = sections[i];

            if (section != null) {
                mask |= 1 << i;
                //info("Section " + i + " isn't null, adding with new mask value " + mask);

                chunkSections.add(section);
            }
        }

        int sectionCount;
        if (chunkSections.size() == 0) {
            mask = 0;
            sectionCount = 0;
        } else {
            int maxBit = (1 << chunkSections.size()) - 1;

            mask = maxBit;
            sectionCount = chunkSections.size();
        }

        //info("Count is " + sectionCount);

        int size = 0;
        if (chunkSections.size() > 0) {
            int numB = 16 * 16 * 16;
            //info("NumB = " + numB);
            int secSize = numB * 5 / 2;
            //info("SecSize = " + secSize);

            secSize += numB / 2;
            //info("With Sky = " + secSize);

            size += sectionCount * secSize;
            //info("Final = " + size);
        }

        /*int l = Integer.bitCount(mask) * 2 * 16 * 16 * 16;
        int m = Integer.bitCount(mask) * 16 * 16 * 16 / 2;
        int n = Integer.bitCount(mask) * 16 * 16 * 16 / 2;*/

        byte[] blocks = new byte[size + 256];
        i = 0;

        //info("data size is " + data.length);

        for (ChunkSection chunkSection : chunkSections) {
            char[] types = chunkSection.blocks();
            int count = types.length;

            for (int j = 0; j < count; j++) {
                char on = types[j];

                blocks[i++] = ((byte) (on & 255));
                blocks[i++] = ((byte) (on >> 8));
            }
        }

        data = blocks;
    }

    public PacketChunk() {
    }


    public int x() {
        return x;
    }

    public PacketChunk X(int x) {
        this.x = x;
        return this;
    }

    public int z() {
        return z;
    }

    public PacketChunk Z(int z) {
        this.z = z;
        return this;
    }

    public int mask() {
        return mask;
    }

    public PacketChunk Mask(int mask) {
        this.mask = mask;
        return this;
    }

    public boolean continuous() {
        return continuous;
    }

    public PacketChunk Continuous(boolean continuous) {
        this.continuous = continuous;
        return this;
    }

    public byte[] data() {
        return data;
    }

    public PacketChunk Data(byte[] data) {
        this.data = data;
        return this;
    }
}
