package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.TallGrassType;

public class TallGrass extends MaterialData<Stone, TallGrassType> {

    public TallGrass(int data) {
        super(Material.TallGrass, data);
    }

    public TallGrass() {
        super(Material.TallGrass);
    }

    public TallGrass(TallGrassType data) {
        super(Material.TallGrass, data);
    }
}
