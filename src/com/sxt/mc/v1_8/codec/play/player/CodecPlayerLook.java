package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerLook;

public class CodecPlayerLook implements Codec<PacketPlayerLook> {

    @Override
    public void encode(Buffer buffer, PacketPlayerLook packet) {
        buffer.writeFloat(packet.yaw());
        buffer.writeFloat(packet.pitch());
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketPlayerLook packet) {
        packet.Yaw(buffer.readFloat());
        packet.Pitch(buffer.readFloat());
        packet.OnGround(buffer.readBoolean());
    }
}
