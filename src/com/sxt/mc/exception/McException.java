package com.sxt.mc.exception;

public class McException extends RuntimeException {

    public McException(String message) {
        super(message);
    }

    public McException(String message, Throwable cause) {
        super(message, cause);
    }
}
