package com.sxt.mc.base;

public enum Difficulty {

    Peaceful,
    Easy,
    Normal,
    Hard;

    public int id() {
        return ordinal();
    }

    public static Difficulty fromId(int id) {
        if (id < 0 || id >= values().length) return null;
        return values()[id];
    }
}
