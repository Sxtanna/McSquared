package com.sxt.mc2.database.config;

import com.sxt.mc2.config.JsonConfig;

import java.io.File;

public class MySqlConfig extends JsonConfig<MySqlConfig> {

    public static final MySqlConfig Default = new MySqlConfig(" ", " ", " ", " ", 3306, 10, 1000, 10000);

    private String Address, Database, Username, Password;
    private Integer Port, MaxPoolSize, ConnectionTimeout, IdleTimeout;

    public MySqlConfig(String address, String database, String username, String password, Integer port, Integer maxPoolSize, Integer connectionTimeout, Integer idleTimeout) {
        super("MySql");
        Address = address;
        Database = database;
        Username = username;
        Password = password;
        Port = port;
        MaxPoolSize = maxPoolSize;
        ConnectionTimeout = connectionTimeout;
        IdleTimeout = idleTimeout;

        save();
    }

    public MySqlConfig(File file, String address, String database, String username, String password, Integer port, Integer maxPoolSize, Integer connectionTimeout, Integer idleTimeout) {
        super(file);
        Address = address;
        Database = database;
        Username = username;
        Password = password;
        Port = port;
        MaxPoolSize = maxPoolSize;
        ConnectionTimeout = connectionTimeout;
        IdleTimeout = idleTimeout;

        save();
    }

    public MySqlConfig(File file) {
        this(file, " ", " ", " ", " ", 3306, 10, 1000, 10000);
        save();
    }

    public String Address() {
        return Address;
    }

    public String Database() {
        return Database;
    }

    public String Username() {
        return Username;
    }

    public String Password() {
        return Password;
    }

    public Integer Port() {
        return Port;
    }

    public Integer MaxPoolSize() {
        return MaxPoolSize;
    }

    public Integer ConnectionTimeout() {
        return ConnectionTimeout;
    }

    public Integer IdleTimeout() {
        return IdleTimeout;
    }

    @Override
    public Class<? extends JsonConfig> type() {
        return MySqlConfig.class;
    }

    public static MySqlConfig get() {
        return JsonConfig.loadConfig(Default);
    }
}
