package com.sxt.mc.event.base;

import com.sxt.mc.event.subscribe.Subscribable;
import com.sxt.mc.event.subscribe.SubscribeHandler;
import com.sxt.mc.event.subscribe.SubscribedMethod;
import com.sxt.mc.util.type.Logging;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class Bus<A extends Annotation, M extends SubscribedMethod, S extends SubscribeHandler<A, M>> implements Logging {

    private final S subHandler;

    public Bus(S subHandler) {
        this.subHandler = subHandler;
    }

    public S subHandler() {
        return subHandler;
    }

    public <O> O register(O listener) {
        Map<Class<? extends Subscribable>, List<M>> scan = subHandler().scan(listener.getClass());
        subHandler().store(listener, scan);

        return listener;
    }

    public <O, E extends Subscribable> O unRegister(O listener, Class<E> eClass) {
        subHandler().clear(listener, eClass);
        return listener;
    }

    public <O> O unRegister(O listener) {
        subHandler().clear(listener);
        return listener;
    }

    public <E extends Subscribable> void unRegister(Class<E> eClass) {
        subHandler().clear(eClass);
    }

    public <E extends Subscribable> E call(E event) {
        Map<Object, TreeSet<M>> listeners = subHandler().listeners(event.getClass());
        if (listeners == null || listeners.size() == 0) return event;

        listeners.forEach(((listener, methods) -> methods.forEach(method -> method.call(listener, event))));
        return event;
    }
}
