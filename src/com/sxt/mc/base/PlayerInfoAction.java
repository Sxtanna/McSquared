package com.sxt.mc.base;

public enum PlayerInfoAction {

    Add,
    UpdateGamemode,
    UpdatePing,
    UpdateName,
    Remove;

    public int id() {
        return ordinal();
    }

    public static PlayerInfoAction fromId(int id) {
        if (id < 0 || id > 4) return null;
        return values()[id];
    }
}
