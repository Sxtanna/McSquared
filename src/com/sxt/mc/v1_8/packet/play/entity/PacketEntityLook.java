package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntityLook;

@PacketInfo(target = Target.Client, state = State.Play, id = 22, codec = CodecEntityLook.class)
public class PacketEntityLook extends Packet {

    private int entityId;
    private float yaw, pitch;
    private boolean onGround;

    public PacketEntityLook(int entityId, float yaw, float pitch, boolean onGround) {
        this.entityId = entityId;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    public PacketEntityLook() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketEntityLook EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }

    public float yaw() {
        return yaw;
    }

    public PacketEntityLook Yaw(float yaw) {
        this.yaw = yaw;
        return this;
    }

    public float pitch() {
        return pitch;
    }

    public PacketEntityLook Pitch(float pitch) {
        this.pitch = pitch;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketEntityLook OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }
}
