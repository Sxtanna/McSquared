package com.sxt.mc.base.component;

public class VersionComponent {

    private String name;
    private int protocol;

    public VersionComponent(String name, int protocol) {
        this.name = name;
        this.protocol = protocol;
    }

    public String name() {
        return name;
    }

    public VersionComponent Name(String name) {
        this.name = name;
        return this;
    }

    public int protocol() {
        return protocol;
    }

    public VersionComponent Protocol(int protocol) {
        this.protocol = protocol;
        return this;
    }
}
