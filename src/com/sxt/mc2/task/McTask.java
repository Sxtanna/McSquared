package com.sxt.mc2.task;

import com.sxt.mc.util.type.Logging;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public abstract class McTask implements Runnable, Logging {

    private static final ScheduledExecutorService Scheduler = Executors.newScheduledThreadPool(50);

    private static final Map<Long, McTask> CurrentTasks = new ConcurrentHashMap<>();
    private static Long nextID = 0L;

    private final long id;
    private long only = -1, runs = 0, delay = 0;
    private boolean repeating = false;
    private ScheduledFuture future;

    public McTask() {
        this.id = nextID++;
    }

    public McTask Only(long only) {
        this.only = only;
        return this;
    }

    public McTask Delay(long delay) {
        this.delay = delay;
        return this;
    }

    public McTask Delay(long delay, TimeUnit unit) {
        return Delay(TimeUnit.MILLISECONDS.convert(delay, unit));
    }

    public long id() {
        return id;
    }

    private boolean done() {
        if (only != -1 && runs++ == only) return true;
        return false;
    }

    private Runnable backend() {
        return () -> {
            McTask.this.run();
            if (done() || !repeating) Cancel();
        };
    }

    public McTask Cancel() {
        future.cancel(true);
        CurrentTasks.remove(id());
        return this;
    }

    private ScheduledFuture Future(ScheduledFuture future) {
        CurrentTasks.putIfAbsent(id(), this);
        return future;
    }

    public McTask go() {
        future = Future(Scheduler.scheduleWithFixedDelay(backend(), delay, 1, TimeUnit.MILLISECONDS));
        return this;
    }

    public McTask goRepeat(long time) {
        repeating = true;
        future = Future(Scheduler.scheduleAtFixedRate(backend(), delay, time, TimeUnit.MILLISECONDS));
        return this;
    }

    public McTask goRepeat(long time, TimeUnit unit) {
        return goRepeat(TimeUnit.MILLISECONDS.convert(time, unit));
    }

    public McTask goLater(long time) {
        future = Future(Scheduler.scheduleWithFixedDelay(backend(), time, delay, TimeUnit.MILLISECONDS));
        return this;
    }

    public McTask goLater(long time, TimeUnit unit) {
        return goLater(TimeUnit.MILLISECONDS.convert(time, unit));
    }

    public static McTask submit(Runnable runnable) {
        return new McTask() {
            @Override
            public void run() {
                runnable.run();
            }
        };
    }

    public static ScheduledExecutorService Executor() {
        return Scheduler;
    }

    public static void clear() {
        CurrentTasks.values().forEach(McTask::Cancel);
        CurrentTasks.clear();
    }
}
