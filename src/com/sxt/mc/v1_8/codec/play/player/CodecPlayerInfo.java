package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.base.Gamemode;
import com.sxt.mc.base.PlayerInfo;
import com.sxt.mc.base.PlayerInfoAction;
import com.sxt.mc.base.chat.ComponentSerializer;
import com.sxt.mc.network.auth.Property;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.session.profile.Profile;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CodecPlayerInfo implements Codec<PacketPlayerInfo> {

    @Override
    public void encode(Buffer buffer, PacketPlayerInfo packet) {
        PlayerInfoAction action = packet.action();
        List<PlayerInfo> info = packet.infos();

        buffer.writeVarInt(action.id());
        buffer.writeVarInt(info.size());

        for (PlayerInfo entry : info) {
            Profile profile = entry.profile();
            boolean hasDisplayName = entry.hasDisplayName();
            List<Property> properties = profile.properties();

            buffer.writeUuid(profile.uuid());

            switch (action) {
                case Add:
                    buffer.writeString(profile.name());
                    buffer.writeVarInt(properties.size());

                    for (Property property : properties) {
                        boolean isSigned = property.signed();
                        buffer.writeString(property.name());
                        buffer.writeString(property.value());
                        buffer.writeBoolean(isSigned);

                        if (isSigned) buffer.writeString(property.signature());
                    }

                    buffer.writeVarInt(entry.gamemode().id());
                    buffer.writeVarInt(entry.ping());

                    buffer.writeBoolean(hasDisplayName);
                    if (hasDisplayName) buffer.writeString(ComponentSerializer.to(entry.displayName()));
                    break;

                case UpdateGamemode:
                    buffer.writeVarInt(entry.gamemode().id());
                    break;

                case UpdatePing:
                    buffer.writeVarInt(entry.ping());
                    break;

                case UpdateName:
                    buffer.writeBoolean(hasDisplayName);
                    if (hasDisplayName) buffer.writeString(ComponentSerializer.to(entry.displayName()));
                    break;

                case Remove:
                    break;
            }
        }
    }

    @Override
    public void decode(Buffer buffer, PacketPlayerInfo packet) {
        PlayerInfoAction action = PlayerInfoAction.fromId(buffer.readVarInt());

        int infoSize = buffer.readVarInt();
        List<PlayerInfo> info = new ArrayList<>(infoSize);

        for (int i = 0; i < infoSize; i++) {
            PlayerInfo entry = new PlayerInfo();

            UUID playerId = buffer.readUuid();

            switch (action) {
                case Add:
                    String name = buffer.readString();
                    int propertiesSize = buffer.readVarInt();
                    List<Property> properties = new ArrayList<>(propertiesSize);
                    for (int j = 0; j < propertiesSize; j++) {
                        String propertyName = buffer.readString();
                        String propertyValue = buffer.readString();
                        String signature = null;

                        if (buffer.readBoolean()) signature = buffer.readString();

                        properties.add(new Property(propertyName, propertyValue, signature));
                    }
                    entry.Profile(new Profile(name, playerId, properties));

                    entry.Gamemode(Gamemode.fromId(buffer.readVarInt()));
                    entry.Ping(buffer.readVarInt());

                    if (buffer.readBoolean()) entry.DisplayName(ComponentSerializer.from(buffer.readString()));
                    break;

                case UpdateGamemode:
                    entry.Gamemode(Gamemode.fromId(buffer.readVarInt()));
                    break;

                case UpdatePing:
                    entry.Ping(buffer.readVarInt());
                    break;

                case UpdateName:
                    if (buffer.readBoolean()) entry.DisplayName(ComponentSerializer.from(buffer.readString()));
                    break;

                case Remove:
                    break;
            }
            info.add(entry);
        }

        packet.Infos(info);
    }
}
