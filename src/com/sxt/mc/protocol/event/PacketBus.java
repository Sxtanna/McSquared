package com.sxt.mc.protocol.event;

import com.sxt.mc.event.base.Bus;
import com.sxt.mc.protocol.event.base.PacketMethod;
import com.sxt.mc.protocol.event.base.PacketWatcher;

public class PacketBus extends Bus<PacketWatcher, PacketMethod, PacketSubHandler> {

    public PacketBus() {
        super(new PacketSubHandler());
    }
}
