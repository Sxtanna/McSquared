package com.sxt.mc2.protocol.packet.status.ping;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc2.protocol.codec.status.ping.CodecPing;

@PacketInfo(target = Target.Server, state = State.Status, id = 1, codec = CodecPing.class)
public class PacketPing extends Packet {

    private long time;

    public PacketPing(long time) {
        this.time = time;
    }

    public PacketPing() {
    }


    public long time() {
        return time;
    }

    public PacketPing Time(long time) {
        this.time = time;
        return this;
    }
}
