package com.sxt.mc2.event.types.player;

import com.sxt.mc.session.Session;
import com.sxt.mc2.event.base.Event;

public class PlayerLogoutEvent extends Event {

    private Session session;
    private String reason;

    public PlayerLogoutEvent(Session session) {
        this.session = session;
    }

    public PlayerLogoutEvent(Session session, String reason) {
        this.session = session;
        this.reason = reason;
    }

    public Session session() {
        return session;
    }

    public String reason() {
        return reason == null ? "Disconnected" : reason;
    }
}
