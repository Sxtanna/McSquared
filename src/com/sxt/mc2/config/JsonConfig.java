package com.sxt.mc2.config;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import com.sxt.mc.util.JsonFile;
import com.sxt.mc.util.type.Builder;
import com.sxt.mc.util.type.Gsonable;
import com.sxt.mc2.McSquared;

import java.io.File;

public class JsonConfig<J extends JsonConfig> extends JsonFile implements Gsonable<JsonConfig>, Builder<J> {

    private JsonObject backend;

    public JsonConfig(String name) {
        super(new File(McSquared.ServerFolder(), name + ".json"));
    }

    public JsonConfig(File file) {
        super(file);
    }

    private JsonObject backend() {
        if (backend == null) backend = new JsonObject();
        return backend;
    }

    public J set(String property, Object value) {
        JsonElement element = McSquared.Gson().toJsonTree(value, value.getClass());
        backend().add(property, element);

        return thisClass();
    }

    public <O> O get(String property, Class<O> type) {
        JsonElement element = backend().get(property);
        return McSquared.Gson().fromJson(element, type);
    }

    public J save() {
        save(thisClass());
        return thisClass();
    }

    @Override
    public Class<? extends JsonConfig> type() {
        return thisClass().getClass();
    }

    public static JsonConfig loadConfig(String name) {
        return new JsonConfig(name);
    }

    @SuppressWarnings("unchecked")
    public static <T extends JsonConfig> T loadConfig(T defClass) {
        return ((T) new JsonFile(defClass.file(), defClass).load(((Class<T>) defClass.type())).File(defClass.file()));
    }
}
