package com.sxt.mc.server.components;

import com.sxt.mc.server.Component;
import com.sxt.mc.util.math.Matrix4;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Transform extends Component<Transform> {

    private final List<Transform> Children = new CopyOnWriteArrayList<>();

    private Transform parent;

    private final Matrix4    Matrix   = Matrix4.identity();
    private final Vector     Position = Vector.zero(), Scale = Vector.one();
    private final Quaternion Rotation = Quaternion.identity();


    public Vector LocalPosition() {
        return new Vector<Vector>(Position);
    }

    public Transform LocalPosition(Vector position) {
        this.Position.Set(position);
        this.Matrix.Translation(position);

        return this;
    }

    public Quaternion LocalRotation() {
        return new Quaternion(Rotation);
    }

    public Transform LocalRotation(Quaternion rotation) {
        this.Rotation.Set(rotation);
        this.Matrix.set(Position, rotation, Scale);

        return this;
    }

    public Vector LocalScale() {
        return new Vector<Vector>(Scale);
    }

    public Transform LocalScale(Vector scale) {
        this.Scale.Set(scale);
        this.Matrix.set(Position, Rotation, scale);
        return this;
    }

    public Matrix4 Matrix() {
        return Matrix;
    }

    public Vector Position() {
        return localToWorldMatrix().translation();
    }

    public Transform Position(Vector position) {
        Matrix4 parentWorldToLocalMatrix = parent != null ? parent.worldToLocalMatrix() : Matrix4.identity();
        this.LocalPosition(parentWorldToLocalMatrix.multiply(position));

        return this;
    }

    public Quaternion Rotation() {
        return localToWorldMatrix().rotation();
    }

    public Transform Rotation(Quaternion rotation) {
        Matrix4 parentWorldToLocalMatrix = parent != null ? parent.worldToLocalMatrix() : Matrix4.identity();
        this.LocalRotation(parentWorldToLocalMatrix.multiply(rotation));

        return this;
    }

    public Vector Scale() {
        return localToWorldMatrix().scale();
    }

    public Matrix4 localToWorldMatrix() {
        if (parent == null) return Matrix4.identity();
        else {
            Matrix4 newMa = parent().localToWorldMatrix();
            newMa.multiply(Matrix());
            return newMa;
        }
    }

    public Matrix4 worldToLocalMatrix() {
        Matrix4 local = localToWorldMatrix();
        local.invert();
        return local;
    }

    public Transform root() {
        Transform root = this;

        while (true) {
            Transform parent = root.parent();
            if (parent == null) break;
            root = parent;
        }

        return root;
    }

    public Transform parent() {
        return parent;
    }

    public Transform Parent(Transform parent) {
        if (this.parent != null) this.parent.Children.remove(this);

        this.parent = parent;
        if (parent != null) parent.Children.add(this);

        GameObject.forEachComponent(component -> component.Parent(parent == null ? null : parent.GameObject));
        GameObject.clearObservers();
        return this;
    }

    public Collection<Transform> Children() {
        return Collections.unmodifiableCollection(Children);
    }

    public double distance(Transform transform) {
        return Position().distance(transform.Position());
    }

    public double distanceSquared(Transform transform) {
        return Position().distanceSquared(transform.Position());
    }
}
