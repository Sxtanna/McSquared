package com.sxt.mc.protocol.base;

import com.sxt.mc.util.Registry;

public class ProtocolNameRegistry extends Registry<String> {

    public ProtocolNameRegistry() {
        for (int i = 0; i < 6; i++) register(i, "1.7");
        for (int i = 6; i < 48; i++) register(i, "1.8");
        for (int i = 48; i < 110; i++) register(i, "1.9");
        for (int i = 201; i < 211; i++) register(i, "1.10");
    }
}
