package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.SlabType;

public class Step extends MaterialData<Step, SlabType> {

    public Step(int data) {
        super(Material.Step, data);
    }

    public Step() {
        super(Material.Step);
    }

    public Step(SlabType data) {
        super(Material.Step, data);
    }
}
