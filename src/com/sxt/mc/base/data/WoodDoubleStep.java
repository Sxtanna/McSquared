package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;

public class WoodDoubleStep extends MaterialData<WoodDoubleStep, WoodType> {

    public WoodDoubleStep(int data) {
        super(Material.WoodDoubleStep, data);
    }

    public WoodDoubleStep() {
        super(Material.WoodDoubleStep);
    }

    public WoodDoubleStep(WoodType data) {
        super(Material.WoodDoubleStep, data);
    }
}
