package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.QuartsBlockType;

public class QuartsBlock extends MaterialData<QuartsBlock, QuartsBlockType> {

    public QuartsBlock(int data) {
        super(Material.QuartsBlock, data);
    }

    public QuartsBlock() {
        super(Material.QuartsBlock);
    }

    public QuartsBlock(QuartsBlockType data) {
        super(Material.QuartsBlock, data);
    }
}
