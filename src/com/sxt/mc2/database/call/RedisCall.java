package com.sxt.mc2.database.call;

import com.sxt.mc2.database.Redis;
import com.sxt.mc2.database.base.DatabaseCall;

import redis.clients.jedis.Jedis;

public abstract class RedisCall extends DatabaseCall<RedisCall, Jedis, Redis> {

    @Override
    public void onFinally() throws Exception {
    }
}
