package com.sxt.mc.protocol.base.packet.client;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.codec.client.CodecHandShake;
import com.sxt.mc.protocol.base.packet.Packet;

@PacketInfo(target = Target.Server, state = State.HandShake, id = 0, codec = CodecHandShake.class)
public class PacketHandShake extends Packet {

    private String address;
    private int version, port;
    private State state;

    public PacketHandShake(String address, int version, int port, State state) {
        this.address = address;
        this.version = version;
        this.port = port;
        this.state = state;
    }

    public PacketHandShake() {
    }


    public String address() {
        return address;
    }

    public PacketHandShake Address(String address) {
        this.address = address;
        return this;
    }

    public int version() {
        return version;
    }

    public PacketHandShake Version(int version) {
        this.version = version;
        return this;
    }

    public int port() {
        return port;
    }

    public PacketHandShake Port(int port) {
        this.port = port;
        return this;
    }

    public State state() {
        return state;
    }

    public PacketHandShake State(State state) {
        this.state = state;
        return this;
    }
}
