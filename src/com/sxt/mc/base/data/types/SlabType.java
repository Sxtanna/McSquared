package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum SlabType implements MaterialTypeData<SlabType> {

    Stone,
    Sandstone,
    Wooden,
    Cobblestone,
    Brick,
    StoneBrick,
    NetherBrick,
    Quartz;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public SlabType byData(int data) {
        if (data < 0 || data > 7) return Stone;
        return values()[data];
    }
}
