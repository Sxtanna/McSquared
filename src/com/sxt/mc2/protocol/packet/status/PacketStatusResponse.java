package com.sxt.mc2.protocol.packet.status;

import com.sxt.mc.base.component.StatusComponent;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc2.protocol.codec.status.CodecStatusResponse;

@PacketInfo(target = Target.Client, state = State.Status, id = 0, codec = CodecStatusResponse.class)
public class PacketStatusResponse extends Packet {

    private StatusComponent status;

    public PacketStatusResponse(StatusComponent status) {
        this.status = status;
    }

    public PacketStatusResponse() {
    }


    public StatusComponent status() {
        return status;
    }

    public PacketStatusResponse Status(StatusComponent status) {
        this.status = status;
        return this;
    }
}
