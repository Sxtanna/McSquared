package com.sxt.mc.network.base;

import com.sun.istack.internal.NotNull;
import com.sxt.mc.exception.BufferException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.ScatteringByteChannel;
import java.nio.charset.Charset;
import java.util.UUID;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufProcessor;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Buffer extends ByteBuf {

    private final ByteBuf back;

    public Buffer(ByteBuf back) {
        this.back = back;
    }


    public int readVarInt() {
        int result = 0, count = 0;
        while (true) {
            byte in = readByte();
            result |= (in & 0x7f) << (count++ * 7);

            if (count > 5) throw new BufferException("VarInt byte count > 5");

            if ((in & 0x80) != 0x80) break;
        }
        return result;
    }

    public void writeVarInt(int value) {
        while (true) {
            byte part = ((byte) (value & 0x7f));
            value >>>= 7;

            if (value != 0) part |= 0x80;
            writeByte(part);

            if (value == 0) break;
        }
    }

    public long readVarLong() {
        long result = 0, count = 0;
        while (true) {
            byte in = readByte();
            result |= (in & 0x7f) << (count++ * 7);

            if (count > 10) throw new BufferException("VarLong byte count > 10");

            if ((in & 0x80) != 0x80) break;
        }
        return result;
    }

    public void writeVarLong(long value) {
        while (true) {
            byte part = (byte) (value & 0x7f);
            value >>>= 7;

            if (value != 0) part |= 0x80;
            writeByte(part);

            if (value == 0) break;
        }
    }

    public byte[] readByteArray() {
        byte[] result = new byte[readVarInt()];
        readBytes(result);

        return result;
    }

    public void writeByteArray(@NotNull byte[] value) {
        writeVarInt(value.length);
        writeBytes(value);
    }

    public String readString() {
        return new String(readByteArray(), UTF_8);
    }

    public void writeString(String value) {
        writeByteArray(value.getBytes(UTF_8));
    }

    public void writeUuid(UUID uuid) {
        writeLong(uuid.getMostSignificantBits());
        writeLong(uuid.getLeastSignificantBits());
    }

    public UUID readUuid() {
        long mostSignificantBits = readLong();
        long leastSignificantBits = readLong();

        return new UUID(mostSignificantBits, leastSignificantBits);
    }

    public double readFixedPointInt() {
        return (double) readInt() / 32;
    }

    public void writeFixedPointInt(double value) {
        writeInt((int) (value * 32));
    }

    public double readFixedPointShort() {
        return (double) readShort() / 8000;
    }

    public void writeFixedPointShort(double value) {
        writeShort((short) (value * 8000));
    }

    public double readFixedPointByte() {
        return (double) readByte() / 32;
    }

    public void writeFixedPointByte(double value) {
        writeByte((int) (value * 32));
    }

    public void writeAngle(float angle) {
        writeByte((int) (angle * 256f / 360f));
    }

    public float readAngle() {
        return readByte() * 360f / 256f;
    }


    @Override
    public int capacity() {
        return back.capacity();
    }

    @Override
    public ByteBuf capacity(int newCapacity) {
        return back.capacity(newCapacity);
    }

    @Override
    public int maxCapacity() {
        return back.maxCapacity();
    }

    @Override
    public ByteBufAllocator alloc() {
        return back.alloc();
    }

    @Override
    public ByteOrder order() {
        return back.order();
    }

    @Override
    public ByteBuf order(ByteOrder endianness) {
        return back.order(endianness);
    }

    @Override
    public ByteBuf unwrap() {
        return back.unwrap();
    }

    @Override
    public boolean isDirect() {
        return back.isDirect();
    }

    @Override
    public int readerIndex() {
        return back.readerIndex();
    }

    @Override
    public ByteBuf readerIndex(int readerIndex) {
        return back.readerIndex(readerIndex);
    }

    @Override
    public int writerIndex() {
        return back.writerIndex();
    }

    @Override
    public ByteBuf writerIndex(int writerIndex) {
        return back.writerIndex(writerIndex);
    }

    @Override
    public ByteBuf setIndex(int readerIndex, int writerIndex) {
        return back.setIndex(readerIndex, writerIndex);
    }

    @Override
    public int readableBytes() {
        return back.readableBytes();
    }

    @Override
    public int writableBytes() {
        return back.writableBytes();
    }

    @Override
    public int maxWritableBytes() {
        return back.maxWritableBytes();
    }

    @Override
    public boolean isReadable() {
        return back.isReadable();
    }

    @Override
    public boolean isReadable(int size) {
        return back.isReadable(size);
    }

    @Override
    public boolean isWritable() {
        return back.isWritable();
    }

    @Override
    public boolean isWritable(int size) {
        return back.isWritable(size);
    }

    @Override
    public ByteBuf clear() {
        return back.clear();
    }

    @Override
    public ByteBuf markReaderIndex() {
        return back.markReaderIndex();
    }

    @Override
    public ByteBuf resetReaderIndex() {
        return back.resetReaderIndex();
    }

    @Override
    public ByteBuf markWriterIndex() {
        return back.markWriterIndex();
    }

    @Override
    public ByteBuf resetWriterIndex() {
        return back.resetWriterIndex();
    }

    @Override
    public ByteBuf discardReadBytes() {
        return back.discardReadBytes();
    }

    @Override
    public ByteBuf discardSomeReadBytes() {
        return back.discardSomeReadBytes();
    }

    @Override
    public ByteBuf ensureWritable(int minWritableBytes) {
        return back.ensureWritable(minWritableBytes);
    }

    @Override
    public int ensureWritable(int minWritableBytes, boolean force) {
        return back.ensureWritable(minWritableBytes, force);
    }

    @Override
    public boolean getBoolean(int index) {
        return back.getBoolean(index);
    }

    @Override
    public byte getByte(int index) {
        return back.getByte(index);
    }

    @Override
    public short getUnsignedByte(int index) {
        return back.getUnsignedByte(index);
    }

    @Override
    public short getShort(int index) {
        return back.getShort(index);
    }

    @Override
    public int getUnsignedShort(int index) {
        return back.getUnsignedShort(index);
    }

    @Override
    public int getMedium(int index) {
        return back.getMedium(index);
    }

    @Override
    public int getUnsignedMedium(int index) {
        return back.getUnsignedMedium(index);
    }

    @Override
    public int getInt(int index) {
        return back.getInt(index);
    }

    @Override
    public long getUnsignedInt(int index) {
        return back.getUnsignedInt(index);
    }

    @Override
    public long getLong(int index) {
        return back.getLong(index);
    }

    @Override
    public char getChar(int index) {
        return back.getChar(index);
    }

    @Override
    public float getFloat(int index) {
        return back.getFloat(index);
    }

    @Override
    public double getDouble(int index) {
        return back.getDouble(index);
    }

    @Override
    public ByteBuf getBytes(int index, ByteBuf dst) {
        return back.getBytes(index, dst);
    }

    @Override
    public ByteBuf getBytes(int index, ByteBuf dst, int length) {
        return back.getBytes(index, dst, length);
    }

    @Override
    public ByteBuf getBytes(int index, ByteBuf dst, int dstIndex, int length) {
        return back.getBytes(index, dst, dstIndex, length);
    }

    @Override
    public ByteBuf getBytes(int index, byte[] dst) {
        return back.getBytes(index, dst);
    }

    @Override
    public ByteBuf getBytes(int index, byte[] dst, int dstIndex, int length) {
        return back.getBytes(index, dst, dstIndex, length);
    }

    @Override
    public ByteBuf getBytes(int index, ByteBuffer dst) {
        return back.getBytes(index, dst);
    }

    @Override
    public ByteBuf getBytes(int index, OutputStream out, int length) throws IOException {
        return back.getBytes(index, out, length);
    }

    @Override
    public int getBytes(int index, GatheringByteChannel out, int length) throws IOException {
        return back.getBytes(index, out, length);
    }

    @Override
    public ByteBuf setBoolean(int index, boolean value) {
        return back.setBoolean(index, value);
    }

    @Override
    public ByteBuf setByte(int index, int value) {
        return back.setByte(index, value);
    }

    @Override
    public ByteBuf setShort(int index, int value) {
        return back.setShort(index, value);
    }

    @Override
    public ByteBuf setMedium(int index, int value) {
        return back.setMedium(index, value);
    }

    @Override
    public ByteBuf setInt(int index, int value) {
        return back.setInt(index, value);
    }

    @Override
    public ByteBuf setLong(int index, long value) {
        return back.setLong(index, value);
    }

    @Override
    public ByteBuf setChar(int index, int value) {
        return back.setChar(index, value);
    }

    @Override
    public ByteBuf setFloat(int index, float value) {
        return back.setFloat(index, value);
    }

    @Override
    public ByteBuf setDouble(int index, double value) {
        return back.setDouble(index, value);
    }

    @Override
    public ByteBuf setBytes(int index, ByteBuf src) {
        return back.setBytes(index, src);
    }

    @Override
    public ByteBuf setBytes(int index, ByteBuf src, int length) {
        return back.setBytes(index, src, length);
    }

    @Override
    public ByteBuf setBytes(int index, ByteBuf src, int srcIndex, int length) {
        return back.setBytes(index, src, srcIndex, length);
    }

    @Override
    public ByteBuf setBytes(int index, byte[] src) {
        return back.setBytes(index, src);
    }

    @Override
    public ByteBuf setBytes(int index, byte[] src, int srcIndex, int length) {
        return back.setBytes(index, src, srcIndex, length);
    }

    @Override
    public ByteBuf setBytes(int index, ByteBuffer src) {
        return back.setBytes(index, src);
    }

    @Override
    public int setBytes(int index, InputStream in, int length) throws IOException {
        return back.setBytes(index, in, length);
    }

    @Override
    public int setBytes(int index, ScatteringByteChannel in, int length) throws IOException {
        return back.setBytes(index, in, length);
    }

    @Override
    public ByteBuf setZero(int index, int length) {
        return back.setZero(index, length);
    }

    @Override
    public boolean readBoolean() {
        return back.readBoolean();
    }

    @Override
    public byte readByte() {
        return back.readByte();
    }

    @Override
    public short readUnsignedByte() {
        return back.readUnsignedByte();
    }

    @Override
    public short readShort() {
        return back.readShort();
    }

    @Override
    public int readUnsignedShort() {
        return back.readUnsignedShort();
    }

    @Override
    public int readMedium() {
        return back.readMedium();
    }

    @Override
    public int readUnsignedMedium() {
        return back.readUnsignedMedium();
    }

    @Override
    public int readInt() {
        return back.readInt();
    }

    @Override
    public long readUnsignedInt() {
        return back.readUnsignedInt();
    }

    @Override
    public long readLong() {
        return back.readLong();
    }

    @Override
    public char readChar() {
        return back.readChar();
    }

    @Override
    public float readFloat() {
        return back.readFloat();
    }

    @Override
    public double readDouble() {
        return back.readDouble();
    }

    @Override
    public ByteBuf readBytes(int length) {
        return back.readBytes(length);
    }

    @Override
    public ByteBuf readSlice(int length) {
        return back.readSlice(length);
    }

    @Override
    public ByteBuf readBytes(ByteBuf dst) {
        return back.readBytes(dst);
    }

    @Override
    public ByteBuf readBytes(ByteBuf dst, int length) {
        return back.readBytes(dst, length);
    }

    @Override
    public ByteBuf readBytes(ByteBuf dst, int dstIndex, int length) {
        return back.readBytes(dst, dstIndex, length);
    }

    @Override
    public ByteBuf readBytes(byte[] dst) {
        return back.readBytes(dst);
    }

    @Override
    public ByteBuf readBytes(byte[] dst, int dstIndex, int length) {
        return back.readBytes(dst, dstIndex, length);
    }

    @Override
    public ByteBuf readBytes(ByteBuffer dst) {
        return back.readBytes(dst);
    }

    @Override
    public ByteBuf readBytes(OutputStream out, int length) throws IOException {
        return back.readBytes(out, length);
    }

    @Override
    public int readBytes(GatheringByteChannel out, int length) throws IOException {
        return back.readBytes(out, length);
    }

    @Override
    public ByteBuf skipBytes(int length) {
        return back.skipBytes(length);
    }

    @Override
    public ByteBuf writeBoolean(boolean value) {
        return back.writeBoolean(value);
    }

    @Override
    public ByteBuf writeByte(int value) {
        return back.writeByte(value);
    }

    @Override
    public ByteBuf writeShort(int value) {
        return back.writeShort(value);
    }

    @Override
    public ByteBuf writeMedium(int value) {
        return back.writeMedium(value);
    }

    @Override
    public ByteBuf writeInt(int value) {
        return back.writeInt(value);
    }

    @Override
    public ByteBuf writeLong(long value) {
        return back.writeLong(value);
    }

    @Override
    public ByteBuf writeChar(int value) {
        return back.writeChar(value);
    }

    @Override
    public ByteBuf writeFloat(float value) {
        return back.writeFloat(value);
    }

    @Override
    public ByteBuf writeDouble(double value) {
        return back.writeDouble(value);
    }

    @Override
    public ByteBuf writeBytes(ByteBuf src) {
        return back.writeBytes(src);
    }

    @Override
    public ByteBuf writeBytes(ByteBuf src, int length) {
        return back.writeBytes(src, length);
    }

    @Override
    public ByteBuf writeBytes(ByteBuf src, int srcIndex, int length) {
        return back.writeBytes(src, srcIndex, length);
    }

    @Override
    public ByteBuf writeBytes(byte[] src) {
        return back.writeBytes(src);
    }

    @Override
    public ByteBuf writeBytes(byte[] src, int srcIndex, int length) {
        return back.writeBytes(src, srcIndex, length);
    }

    @Override
    public ByteBuf writeBytes(ByteBuffer src) {
        return back.writeBytes(src);
    }

    @Override
    public int writeBytes(InputStream in, int length) throws IOException {
        return back.writeBytes(in, length);
    }

    @Override
    public int writeBytes(ScatteringByteChannel in, int length) throws IOException {
        return back.writeBytes(in, length);
    }

    @Override
    public ByteBuf writeZero(int length) {
        return back.writeZero(length);
    }

    @Override
    public int indexOf(int fromIndex, int toIndex, byte value) {
        return back.indexOf(fromIndex, toIndex, value);
    }

    @Override
    public int bytesBefore(byte value) {
        return back.bytesBefore(value);
    }

    @Override
    public int bytesBefore(int length, byte value) {
        return back.bytesBefore(length, value);
    }

    @Override
    public int bytesBefore(int index, int length, byte value) {
        return back.bytesBefore(index, length, value);
    }

    @Override
    public int forEachByte(ByteBufProcessor processor) {
        return back.forEachByte(processor);
    }

    @Override
    public int forEachByte(int index, int length, ByteBufProcessor processor) {
        return back.forEachByte(index, length, processor);
    }

    @Override
    public int forEachByteDesc(ByteBufProcessor processor) {
        return back.forEachByteDesc(processor);
    }

    @Override
    public int forEachByteDesc(int index, int length, ByteBufProcessor processor) {
        return back.forEachByteDesc(index, length, processor);
    }

    @Override
    public ByteBuf copy() {
        return back.copy();
    }

    @Override
    public ByteBuf copy(int index, int length) {
        return back.copy(index, length);
    }

    @Override
    public ByteBuf slice() {
        return back.slice();
    }

    @Override
    public ByteBuf slice(int index, int length) {
        return back.slice(index, length);
    }

    @Override
    public ByteBuf duplicate() {
        return back.duplicate();
    }

    @Override
    public int nioBufferCount() {
        return back.nioBufferCount();
    }

    @Override
    public ByteBuffer nioBuffer() {
        return back.nioBuffer();
    }

    @Override
    public ByteBuffer nioBuffer(int index, int length) {
        return back.nioBuffer(index, length);
    }

    @Override
    public ByteBuffer internalNioBuffer(int index, int length) {
        return back.internalNioBuffer(index, length);
    }

    @Override
    public ByteBuffer[] nioBuffers() {
        return back.nioBuffers();
    }

    @Override
    public ByteBuffer[] nioBuffers(int index, int length) {
        return back.nioBuffers(index, length);
    }

    @Override
    public boolean hasArray() {
        return back.hasArray();
    }

    @Override
    public byte[] array() {
        return back.array();
    }

    @Override
    public int arrayOffset() {
        return back.arrayOffset();
    }

    @Override
    public boolean hasMemoryAddress() {
        return back.hasMemoryAddress();
    }

    @Override
    public long memoryAddress() {
        return back.memoryAddress();
    }

    @Override
    public String toString(Charset charset) {
        return back.toString(charset);
    }

    @Override
    public String toString(int index, int length, Charset charset) {
        return back.toString(index, length, charset);
    }

    @Override
    public int hashCode() {
        return back.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return back.equals(obj);
    }

    @Override
    public int compareTo(ByteBuf buffer) {
        return back.compareTo(buffer);
    }

    @Override
    public String toString() {
        return back.toString();
    }

    @Override
    public ByteBuf retain(int increment) {
        return back.retain(increment);
    }

    @Override
    public ByteBuf retain() {
        return back.retain();
    }

    @Override
    public int refCnt() {
        return back.refCnt();
    }

    @Override
    public boolean release() {
        return back.release();
    }

    @Override
    public boolean release(int decrement) {
        return back.release(decrement);
    }
}
