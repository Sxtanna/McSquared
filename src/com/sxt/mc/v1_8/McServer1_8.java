package com.sxt.mc.v1_8;

import com.sxt.mc.base.Version;
import com.sxt.mc.log.LogProvider;
import com.sxt.mc.network.NetworkServer;
import com.sxt.mc.network.pipe.McChannel;
import com.sxt.mc.protocol.event.PacketBus;
import com.sxt.mc.server.level.Level;
import com.sxt.mc.session.SessionServer;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.config.server.ServerConfig;
import com.sxt.mc2.event.EventBus;

import org.slf4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

final class McServer1_8 extends Version {

    private final LogProvider logProvider;
    private final Logger logger;

    private final NetworkServer Network;
    private final SessionServer Sessions;

    private final EventBus eventBus;
    private final PacketBus packetBus;

    private final LinkedHashMap<UUID, Level> Levels = new LinkedHashMap<>();

    public McServer1_8(ServerConfig config) {
        super(config);

        this.logProvider = new LogProvider();
        this.logger = logProvider.get(name());

        Sessions = new SessionServer();
        Network = new NetworkServer(new McChannel(Sessions));

        eventBus = new EventBus();
        packetBus = new PacketBus();

        Network().setup();
    }

    @Override
    public String name() {
        return getClass().getSimpleName();
    }

    @Override
    public LogProvider LogProvider() {
        return logProvider;
    }

    @Override
    public Logger Logger() {
        return logger;
    }

    @Override
    public EventBus Events() {
        return eventBus;
    }

    @Override
    public PacketBus Packets() {
        return packetBus;
    }

    @Override
    public File ServerFolder() {
        return McSquared.ServerFolder();
    }

    @Override
    public File DataFolder() {
        return McSquared.DataFolder();
    }

    @Override
    public NetworkServer Network() {
        return Network;
    }

    @Override
    public SessionServer Sessions() {
        return Sessions;
    }

    @Override
    public Map<UUID, Level> Levels() {
        return Levels;
    }
}
