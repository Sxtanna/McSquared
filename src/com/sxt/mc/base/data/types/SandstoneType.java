package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum SandstoneType implements MaterialTypeData<SandstoneType> {

    Plain,
    Chiseled,
    Smooth;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public SandstoneType byData(int data) {
        if (data < 0 || data > 2) return Plain;
        return values()[data];
    }
}

