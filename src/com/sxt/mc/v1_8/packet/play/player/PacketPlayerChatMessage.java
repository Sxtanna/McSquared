package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayerChatMessage;

@PacketInfo(target = Target.Server, state = State.Play, id = 1, codec = CodecPlayerChatMessage.class)
public class PacketPlayerChatMessage extends Packet {

    private String message;

    public PacketPlayerChatMessage(String message) {
        this.message = message;
    }

    public PacketPlayerChatMessage() {}


    public boolean command() {
        return message().startsWith("/");
    }

    public String message() {
        return message;
    }

    public PacketPlayerChatMessage Message(String message) {
        this.message = message;
        return this;
    }
}
