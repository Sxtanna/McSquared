package com.sxt.mc.util;

import java.util.HashMap;
import java.util.Map;

public class Registry<O> {

    private final Map<Integer, O> IdMap = new HashMap<>();
    private final Map<O, Integer> ValueMap = new HashMap<>();

    public void register(int id, O value) {
        IdMap.put(id, value);
        ValueMap.put(value, id);
    }

    public O find(int id) {
        return IdMap.get(id);
    }

    public int find(O value) {
        Integer stored = ValueMap.get(value);
        return stored == null ? -1 : stored;
    }

    public Map<Integer, O> IdMap() {
        return IdMap;
    }

    public Map<O, Integer> ValueMap() {
        return ValueMap;
    }

    public void release() {
        IdMap().clear();
        ValueMap.clear();
    }
}
