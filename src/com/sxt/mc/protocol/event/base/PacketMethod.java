package com.sxt.mc.protocol.event.base;

import com.sxt.mc.event.subscribe.SubscribedMethod;

import java.lang.reflect.Method;

public class PacketMethod extends SubscribedMethod<PacketMethod> {

    public PacketMethod(Method method) {
        super(method);
    }

    @Override
    public int compareTo(PacketMethod o) {
        return 0;
    }
}
