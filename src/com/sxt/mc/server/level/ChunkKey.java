package com.sxt.mc.server.level;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ChunkKey {

    private final int x, z;

    public ChunkKey(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public int x() {
        return x;
    }

    public int z() {
        return z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChunkKey chunkKey = (ChunkKey) o;

        return new EqualsBuilder()
                .append(x, chunkKey.x)
                .append(z, chunkKey.z)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(x)
                .append(z)
                .toHashCode();
    }
}
