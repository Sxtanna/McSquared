package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.SandstoneType;

public class RedSandstone extends MaterialData<RedSandstone, SandstoneType> {

    public RedSandstone(int data) {
        super(Material.RedSandstone, data);
    }

    public RedSandstone() {
        super(Material.RedSandstone);
    }

    public RedSandstone(SandstoneType data) {
        super(Material.RedSandstone, data);
    }
}
