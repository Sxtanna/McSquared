package com.sxt.mc.network.handlers;

import com.sxt.mc.base.component.PlayerComponent;
import com.sxt.mc.base.component.PlayersComponent;
import com.sxt.mc.base.component.StatusComponent;
import com.sxt.mc.base.component.VersionComponent;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.event.base.PacketWatcher;
import com.sxt.mc.session.Session;
import com.sxt.mc.session.profile.Profile;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.event.base.EventPriority;
import com.sxt.mc2.event.base.EventWatcher;
import com.sxt.mc2.event.types.player.PlayerLoginEvent;
import com.sxt.mc2.event.types.player.PlayerLogoutEvent;
import com.sxt.mc2.protocol.packet.status.PacketStatusRequest;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;
import com.sxt.mc2.protocol.packet.status.ping.PacketPing;
import com.sxt.mc2.protocol.packet.status.ping.PacketPong;

import java.util.ArrayList;
import java.util.List;

public class PingHandler implements Logging {

    private static final List<PlayerComponent> Players = new ArrayList<>();

    @PacketWatcher
    public void onStatus(PacketStatusRequest packet) {
        Session session = packet.session();
        Protocol protocol = session.protocol();

        session.send(new PacketStatusResponse(currentStatus(protocol.name(), protocol.version(), McSquared.Config().motd())));
    }

    @PacketWatcher
    public void onPing(PacketPing packet) {
        Session session = packet.session();
        session.channel().flush();

        PacketPong response = new PacketPong(packet.time());
        session.send(response);
    }

    @EventWatcher(priority = EventPriority.Monitor)
    public void onLogin(PlayerLoginEvent e) {
        if (e.cancelled()) return;

        Profile profile = e.session().profile();
        Players.add(new PlayerComponent(profile.name(), profile.uuid().toString()));
    }

    @EventWatcher(priority = EventPriority.Monitor)
    public void onLogout(PlayerLogoutEvent e) {

        Profile profile = e.session().profile();
        Players.removeIf(playerComponent -> playerComponent.id().equals(profile.uuid().toString()));
    }

    public static StatusComponent currentStatus(String name, int protocol, String description) {
        VersionComponent version = new VersionComponent(name, protocol);
        PlayersComponent players = new PlayersComponent(Players.size() + 1, Players.size(), Players.size() > 10 ? Players.subList(0, 10) : Players);

        return new StatusComponent(version, players, description);
    }
}
