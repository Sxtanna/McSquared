package com.sxt.mc.base;

public enum Dimension {

    Nether,
    Overworld,
    End;

    public int id() {
        return ordinal() - 1;
    }

    public static Dimension fromId(int id) {
        if (id < -1 || id > 1) return null;
        return values()[id + 1];
    }
}
