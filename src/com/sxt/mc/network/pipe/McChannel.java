package com.sxt.mc.network.pipe;

import com.sxt.mc.network.codec.IOCoder;
import com.sxt.mc.network.codec.Identifier;
import com.sxt.mc.session.SessionServer;
import com.sxt.mc.util.type.Logging;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class McChannel extends ChannelInitializer<SocketChannel> implements Logging {

    private final NoopHandler Noop = new NoopHandler();
    private final Forwarder Forwarder;

    public McChannel(SessionServer sessions) {
        Forwarder = new Forwarder(sessions);
    }

    @Override
    public String name() {
        return "McChannel";
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline()
                .addLast("encryption", Noop)
                .addLast("identify", new Identifier())
                .addLast("compression", Noop)
                .addLast("iocoder", new IOCoder(Forwarder.Sessions()))
                .addLast("forwarder", Forwarder);
    }
}
