package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.LeafType;
import com.sxt.mc.util.type.MaterialTypeData;

public class Leaves extends MaterialData<Leaves, LeafType> {

    public Leaves(int data) {
        super(Material.Leaves, data);
    }

    public Leaves() {
        super(Material.Leaves);
    }

    public Leaves(LeafType data) {
        super(Material.Leaves, data);
    }
}