package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.StoneType;

public class Stone extends MaterialData<Stone, StoneType> {

    public Stone(int data) {
        super(Material.Stone, data);
    }

    public Stone() {
        super(Material.Stone);
    }

    public Stone(StoneType data) {
        super(Material.Stone, data);
    }
}
