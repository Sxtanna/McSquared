package com.sxt.mc.network.handlers;

import com.sxt.mc.base.ChatPosition;
import com.sxt.mc.base.PlayerInfoAction;
import com.sxt.mc.base.chat.TextComponent;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.protocol.base.packet.client.PacketKeepAlive;
import com.sxt.mc.protocol.event.base.PacketWatcher;
import com.sxt.mc.server.GameObject;
import com.sxt.mc.server.components.Camera;
import com.sxt.mc.server.components.Transform;
import com.sxt.mc.server.entity.Player;
import com.sxt.mc.server.level.Level;
import com.sxt.mc.session.Session;
import com.sxt.mc.session.profile.Profile;
import com.sxt.mc.util.F;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc.v1_8.packet.login.PacketLoginResponse;
import com.sxt.mc.v1_8.packet.play.PacketChatMessage;
import com.sxt.mc.v1_8.packet.play.PacketJoinGame;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerInfo;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerLook;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerPosition;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerPositionLook;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.event.base.EventPriority;
import com.sxt.mc2.event.base.EventWatcher;
import com.sxt.mc2.event.types.player.PlayerLoginEvent;
import com.sxt.mc2.task.McTask;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PlayHandler implements Runnable, Logging {

    public static final int Tps = 20;
    private static final List<WeakReference<Session>> Sessions = new ArrayList<>();

    @Override
    public String name() {
        return "Play";
    }

    @EventWatcher(priority = EventPriority.First)
    public void onPreLogin(PlayerLoginEvent e) {
        Profile profile = e.session().profile();
        info(profile.name() + " logged in with UUID " + profile.uuid());
    }

    @EventWatcher
    public void onLogin(PlayerLoginEvent e) {
        if (e.cancelled()) return;

        Session session = e.session();
        Sessions.add(new WeakReference<>(session));

        Profile profile = session.profile();

        session.send(new PacketLoginResponse(profile.uuid(), profile.name())).addListener(future -> {
            session.State(State.Play);

            Level level = McSquared.MainLevel();

            GameObject playerObject = new GameObject().Name(profile.name());
            playerObject.transform().Position(new Vector(0, 200, 0));
            playerObject.Parent(level.RootObject());

            Player player = playerObject.Add(Player.class).Profile(profile);
            Camera camera = playerObject.Add(Camera.class);

            session.attributes().set(Player.PlayerAttribute, playerObject);

            session.observe(new PacketJoinGame(player.entityId(), McSquared.Config().maxPlayers(), false, false, level.type(), player.gamemode(), level.dimension(), level.difficulty()));
            session.observe(new PacketPlayerInfo(PlayerInfoAction.Add, player));
            camera.Add(session);

            /*sessions().forEach(other -> {
                if (!other.equals(session)) {
                    player.start(other);

                    Player otherPlayer = other.attributes().get(Player.PlayerAttribute).component(Player.class);
                    if (otherPlayer != null) otherPlayer.start(session);
                }
            });*/

            //level.RootObject().observe(new PacketPlayerInfo(PlayerInfoAction.Add, player));
        });


    }

    public List<Session> sessions() {
        return F.map(Sessions, Reference::get).collect(Collectors.toList());
    }

    public void sendAll(Packet packet) {
        F.filter(sessions(), session -> session != null).forEach(session -> session.observe(packet));
    }

    @Override
    public void run() {
        sendAll(new PacketKeepAlive());
    }

    @PacketWatcher
    public void onLook(PacketPlayerLook look) {
        GameObject gameObject = look.session().attributes().get(Player.PlayerAttribute);
        Camera camera = gameObject.component(Camera.class);

        if (camera != null) camera.RemoteRotation(look.rotation());
    }

    @PacketWatcher
    public void onPosition(PacketPlayerPosition pos) {
        GameObject gameObject = pos.session().attributes().get(Player.PlayerAttribute);
        Camera camera = gameObject.component(Camera.class);

        if (camera != null) camera.RemotePosition(pos.position());

        Transform transform = gameObject.transform();
        Vector vec = transform.Position();
        Quaternion rot = transform.Rotation();
        camera.observe(new PacketChatMessage(new TextComponent(vec.x() + ", " + vec.y() + ", " + vec.z() + " [" + rot.pitch() + ", " + rot.yaw()+"]"), ChatPosition.Chat));
        //info("Updating Pos");
    }

    @PacketWatcher
    public void onPositionLook(PacketPlayerPositionLook positionLook) {
        GameObject gameObject = positionLook.session().attributes().get(Player.PlayerAttribute);
        Camera camera = gameObject.component(Camera.class);

        if (camera != null) {
            camera.RemotePosition(positionLook.position());
            camera.RemoteRotation(positionLook.rotation());
        }
    }
}
