package com.sxt.mc.base.meta;

import com.sxt.mc.base.BlockPosition;
import com.sxt.mc.base.ItemSlot;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.util.type.Logging;

public class Metadata implements Logging {

    private final Object[] values = new Object[31];

    public void Set(int index, Object value) {
        values[index] = value;
    }

    public void Byte(int index, int value) {
        Set(index, (byte) value);
    }

    public void Int(int index, int value) {
        Set(index, value);
    }

    public void Float(int index, float value) {
        Set(index, value);
    }

    public void String(int index, String value) {
        Set(index, value);
    }

    public void ItemSlot(int index, ItemSlot value) {
        Set(index, value);
    }

    public void BlockPosition(int index, BlockPosition value) {
        Set(index, value);
    }

    public void Rotation(int index, Vector value) {
        Set(index, value);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(int index) {
        return (T) values[index];
    }

    public int gByte(int index) {
        return this.<Byte>get(index);
    }

    public int gInt(int index) {
        return this.<Integer>get(index);
    }

    public String gString(int index) {
        return this.get(index);
    }

    public ItemSlot gItemSlot(int index) {
        return this.get(index);
    }

    public BlockPosition gBlockPosition(int index) {
        return this.get(index);
    }

    public Vector gRotation(int index) {
        return this.get(index);
    }

    public void write(Buffer buffer) {
        for (int i = 0; i < values.length; i++) {
            Object value = values[i];
            if (value == null) continue;

            MetaType type = typeOf(value);
            int item = i | type.ordinal() << 5;

            buffer.writeByte(item);
            type.write(buffer, value);
        }
        buffer.writeByte(127);
    }

    public Metadata read(Buffer buffer) {
        while (true) {
            int item = buffer.readUnsignedByte();
            if (item == 127) {
                break;
            }

            int index = item & 31;
            MetaType type = MetaType.fromId(item >> 5);

            values[index] = type.read(buffer);
        }
        return this;
    }

    private MetaType typeOf(Object value) {
        if (value instanceof Byte) {
            return MetaType.Byte;
        } else if (value instanceof Short) {
            return MetaType.Short;
        } else if (value instanceof Integer) {
            return MetaType.Int;
        } else if (value instanceof Float) {
            return MetaType.Float;
        } else if (value instanceof String) {
            return MetaType.String;
        } else if (value instanceof ItemSlot) {
            return MetaType.ItemSlot;
        } else if (value instanceof BlockPosition) {
            return MetaType.BlockPosition;
        } else if (value instanceof Vector) {
            return MetaType.Rotation;
        }
        return null;
    }
}
