package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum SandType implements MaterialTypeData<SandType> {

    White,
    Red;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public SandType byData(int data) {
        if (data < 0 || data > 1) return White;
        return values()[data];
    }
}
