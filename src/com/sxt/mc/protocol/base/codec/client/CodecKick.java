package com.sxt.mc.protocol.base.codec.client;

import com.sxt.mc.base.chat.ComponentSerializer;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc2.protocol.packet.login.PacketKick;

public class CodecKick implements Codec<PacketKick> {

    @Override
    public void encode(Buffer buffer, PacketKick packet) {
        buffer.writeString(ComponentSerializer.to(packet.message()));
    }

    @Override
    public void decode(Buffer buffer, PacketKick packet) {
        packet.Message(ComponentSerializer.from(buffer.readString()));
    }
}
