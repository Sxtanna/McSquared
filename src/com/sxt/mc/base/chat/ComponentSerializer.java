package com.sxt.mc.base.chat;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import com.sxt.mc2.McSquared;

import java.lang.reflect.Type;

public class ComponentSerializer implements JsonDeserializer<BaseComponent> {

    static {
        McSquared.GsonBuilder().registerTypeAdapter(BaseComponent.class, new ComponentSerializer());
    }

    @Override
    public BaseComponent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json.isJsonPrimitive()) return new TextComponent(json.getAsString());

        JsonObject object = json.getAsJsonObject();
        return context.deserialize(json, TextComponent.class);
    }

    public static String to(BaseComponent component) {
        return McSquared.Gson().toJson(component);
    }

    public static String to(BaseComponent... components) {
        return McSquared.Gson().toJson(new TextComponent(components));
    }

    public static BaseComponent from(String json) {
        if (json.startsWith("["))
            return new TextComponent(McSquared.Gson().fromJson(json, BaseComponent[].class));
        return McSquared.Gson().fromJson(json, BaseComponent.class);
    }
}
