package com.sxt.mc.protocol.event;

import com.sxt.mc.event.subscribe.SubscribeHandler;
import com.sxt.mc.protocol.event.base.PacketMethod;
import com.sxt.mc.protocol.event.base.PacketWatcher;

import java.lang.reflect.Method;

public class PacketSubHandler extends SubscribeHandler<PacketWatcher, PacketMethod> {

    public PacketSubHandler() {
        super(PacketWatcher.class);
    }

    @Override
    public PacketMethod from(Method method, PacketWatcher annotation) {
        return new PacketMethod(method);
    }
}
