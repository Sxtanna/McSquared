package com.sxt.mc.base;

public enum Color {

    /**
     * <p style="Color:#000000">
     * Represents The Color <b>Black</b>
     *
     * @see DyeColor#Black
     */
    Black        (DyeColor.Black, '0'),
    /**
     * <p style="Color:#0000be">
     * Represents The Color <b>Blue</b>
     *
     * @see DyeColor#Blue
     */
    Dark_Blue    (DyeColor.Blue, '1'),
    /**
     * <p style="Color:#009933">
     * Represents The Color <b>Green</b>
     *
     * @see DyeColor#Green
     */
    Dark_Green   (DyeColor.Green, '2'),
    /**
     * <p style="Color:#019292">
     * Represents The Color <b>Cyan</b>
     *
     * @see DyeColor#Cyan
     */
    Dark_Aqua    (DyeColor.Cyan, '3'),
    /**
     * <p style="Color:#be0000">
     * Represents The Color <b>Red</b>
     *
     * @see DyeColor#Red
     */
    Dark_Red     (DyeColor.Red, '4'),
    /**
     * <p style="Color:#be00be">
     * Represents The Color <b>Purple</b>
     *
     * @see DyeColor#Purple
     */
    Dark_Purple  (DyeColor.Purple, '5'),
    /**
     * <p style="Color:#d9a334">
     * Represents The Color <b>Gold</b>
     *
     * @see DyeColor#Orange
     */
    Gold         (DyeColor.Orange, '6'),
    /**
     * <p style="Color:#bebebe">
     * Represents The Color <b>Silver</b>
     *
     * @see DyeColor#Silver
     */
    Gray         (DyeColor.Silver, '7'),
    /**
     * <p style="Color:#3f3f3f">
     * Represents The Color <b>Dark Gray</b>
     *
     * @see DyeColor#Gray
     */
    Dark_Gray    (DyeColor.Gray, '8'),
    /**
     * <p style="Color:#3f3ffe">
     * Represents The Color <b>Cyan</b>
     *
     * @see DyeColor#Cyan
     */
    Blue         (DyeColor.Cyan, '9'),
    /**
     * <p style="Color:#3ffe3f">
     * Represents The Color <b>Lime</b>
     *
     * @see DyeColor#Lime
     */
    Green        (DyeColor.Lime, 'a'),
    /**
     * <p style="Color:#3ffefe">
     * Represents The Color <b>Aqua</b>
     *
     * @see DyeColor#Aqua
     */
    Aqua         (DyeColor.Aqua, 'b'),
    /**
     * <p style="Color:#fe3f3f">
     * Represents The Color <b>Pink</b>
     *
     * @see DyeColor#Pink
     */
    Red          (DyeColor.Pink, 'c'),
    /**
     * <p style="Color:#fe3ffe">
     * Represents The Color <b>Magenta</b>
     *
     * @see DyeColor#Magenta
     */
    Purple       (DyeColor.Magenta, 'd'),
    /**
     * <p style="Color:#fefe3f">
     * Represents The Color <b>Yellow</b>
     *
     * @see DyeColor#Yellow
     */
    Yellow       (DyeColor.Yellow, 'e'),
    /**
     * <p style="Color:#ffffff; background-Color:rgba(0,0,0,100)">
     * Represents The Color <b>White</b>
     *
     * @see DyeColor#White
     */
    White        (DyeColor.White, 'f'),

    Magic         ('k'),
    Bold          ('l'),
    Strikethrough ('m'),
    Underline     ('n'),
    Italic        ('o'),
    Reset         ('r');

    public static final char Char = '\u00A7';

    private char colorCode;
    private DyeColor dyeColor;

    Color(DyeColor dyeColor, char colorCode) {
        this.dyeColor = dyeColor;
        this.colorCode = colorCode;
    }

    Color(char colorCode) {
        this.dyeColor = DyeColor.White;
        this.colorCode = colorCode;
    }

    public DyeColor dye() {
        return dyeColor;
    }

    public char code() {
        return colorCode;
    }

    public String json() {
        return name().toLowerCase();
    }

    public boolean color() {
        return ordinal() <= 15;
    }

    public boolean format() {
        return !color();
    }


    @Override
    public String toString() {
        return new String(new char[]{Char, code()});
    }
}
