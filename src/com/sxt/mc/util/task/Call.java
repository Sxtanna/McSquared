package com.sxt.mc.util.task;

public interface Call<D> {

    void call(D data);

}
