package com.sxt.mc2.event.types.player;

import com.sxt.mc.session.Session;
import com.sxt.mc2.event.base.Cancellable;
import com.sxt.mc2.event.base.Event;

public class PlayerLoginEvent extends Event implements Cancellable<PlayerLoginEvent> {

    private final Session session;
    private boolean cancelled = false;

    private String kickMessage = "Disconnected";

    public PlayerLoginEvent(Session session) {
        this.session = session;
    }

    public Session session() {
        return session;
    }

    public String kickMessage() {
        return kickMessage;
    }

    public PlayerLoginEvent Cancelled(String reason) {
        this.kickMessage = reason;
        return Cancelled(true);
    }

    @Override
    public PlayerLoginEvent Cancelled(boolean state) {
        this.cancelled = state;
        return thisClass();
    }

    @Override
    public boolean cancelled() {
        return cancelled;
    }
}
