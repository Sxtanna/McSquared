package com.sxt.mc.base.component;

public class PlayerComponent {

    private String name, id;

    public PlayerComponent(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String name() {
        return name;
    }

    public PlayerComponent Name(String name) {
        this.name = name;
        return this;
    }

    public String id() {
        return id;
    }

    public PlayerComponent Id(String id) {
        this.id = id;
        return this;
    }
}
