package com.sxt.mc.v1_8.codec.play.player;
import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayer;

public class CodecPlayer implements Codec<PacketPlayer> {

    @Override
    public void encode(Buffer buffer, PacketPlayer packet) {
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketPlayer packet) {
        packet.OnGround(buffer.readBoolean());
    }
}
