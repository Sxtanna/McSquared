package com.sxt.mc.event.subscribe;

import com.sxt.mc.util.type.Logging;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public abstract class SubscribeHandler<A extends Annotation, S extends SubscribedMethod> implements Logging {

    private final Map<Class<? extends Subscribable>, Map<Object, TreeSet<S>>> SubscribedClasses = new HashMap<>();
    private final Class<A> annClass;

    public SubscribeHandler(Class<A> annClass) {
        this.annClass = annClass;
    }

    @SuppressWarnings("unchecked")
    public Map<Class<? extends Subscribable>, List<S>> scan(Class<?> subcribed) {
        Map<Class<? extends Subscribable>, List<S>> listMap = new HashMap<>();
        try {
            Method[] methods = subcribed.getMethods();

            for (Method method : methods) {
                A ann = method.getAnnotation(annClass);
                if (ann == null) continue;

                Parameter[] params = method.getParameters();
                if (params.length == 0) warn("Method " + named(method) + " has no parameters");
                else if (params.length > 1) warn("Method " + named(method) + " has more than 1 Argument");
                else {
                    Class<?> type = params[0].getType();

                    if (!validate(type) || !Subscribable.class.isAssignableFrom(type))
                        warn("Method " + named(method) + " failed validation");
                    else {
                        Class<? extends Subscribable> sub = ((Class<? extends Subscribable>) params[0].getType());
                        if (!listMap.containsKey(sub)) listMap.put(sub, new ArrayList<>());

                        listMap.get(sub).add(from(method, ann));
                    }
                }
            }
        } catch (Exception e) {
            error("Failed to get Subscribed methods");
        }
        return listMap;
    }

    @SuppressWarnings("unchecked")
    public <O> void store(O listener, Map<Class<? extends Subscribable>, List<S>> methods) {
        methods.forEach(((aClass, ses) -> {
            if (!SubscribedClasses.containsKey(aClass)) SubscribedClasses.put(aClass, new HashMap<>());
            TreeSet<S> methodSet = new TreeSet<>(S::compareTo);
            methodSet.addAll(ses);

            SubscribedClasses.get(aClass).put(listener, methodSet);
        }));
    }

    public <O> void clear(O listener, Class<? extends Subscribable> type) {
        Map<Object, TreeSet<S>> registered = SubscribedClasses.get(type);
        if (registered == null) return;

        TreeSet<S> forListener = registered.get(listener);
        if (forListener == null) return;

        forListener.removeIf(s -> s.isFor(type));
    }

    public <O> void clear(O listener) {
        SubscribedClasses.forEach((type, map) -> {
            TreeSet<S> ses = map.remove(listener);
            if (ses != null) ses.clear();
        });
    }

    public void clear(Class<? extends Subscribable> type) {
        Map<Object, TreeSet<S>> listeners = SubscribedClasses.remove(type);
        if (listeners == null) return;

        listeners.forEach(((o, ses) -> clear(o, type)));
    }

    public Map<Object, TreeSet<S>> listeners(Class<? extends Subscribable> event) {
        return SubscribedClasses.get(event);
    }

    private String named(Method method) {
        return method.getName() + ":" + method.getDeclaringClass().getSimpleName();
    }

    public boolean validate(Class<?> type) {
        return true;
    }

    public abstract S from(Method method, A annotation);
}
