package com.sxt.mc.v1_8.codec.play;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.PacketCamera;

public class CodecCamera implements Codec<PacketCamera> {

    @Override
    public void encode(Buffer buffer, PacketCamera packet) {
        buffer.writeVarInt(packet.entityId());
    }

    @Override
    public void decode(Buffer buffer, PacketCamera packet) {
        packet.EntityId(buffer.readVarInt());
    }
}
