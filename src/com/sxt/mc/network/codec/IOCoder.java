package com.sxt.mc.network.codec;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.protocol.base.codec.CodecRegistry;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.protocol.base.packet.PacketRegistry;
import com.sxt.mc.session.Session;
import com.sxt.mc.session.SessionServer;
import com.sxt.mc.util.C;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.event.types.packet.PacketOutEvent;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

public class IOCoder extends ByteToMessageCodec<Packet> implements Logging {

    private final SessionServer Sessions;

    public IOCoder(SessionServer sessions) {
        Sessions = sessions;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void encode(ChannelHandlerContext ctx, Packet packet, ByteBuf out) throws Exception {
        Session session = Sessions.get(ctx);
        State state = session.state();
        Protocol protocol = session.protocol();

        PacketRegistry packets = protocol.clientBoundPackets(state);
        CodecRegistry codecs = protocol.clientBoundCodecs(state);

        if (packets == null || codecs == null) {
            error("No ClientBound Packets/Codecs are registered for " + protocol.name() + "/" + state.name());
            return;
        }

        Class<? extends Packet> packetClass = packet.getClass();

        int id = packets.find(packetClass);
        Codec codec = codecs.find(id);

        if (codec == null) {
            error("Unregistered ClientBound Codec for " + protocol.name() + "/" + state.name() + ":" + packetClass.getSimpleName());
            return;
        }

        PacketOutEvent packetOutEvent = McSquared.Events().call(new PacketOutEvent(packet.Session(session)));
        if (packetOutEvent.cancelled()) return;

        Buffer buffer = new Buffer(out);
        buffer.writeVarInt(id);

        try {
            codec.encode(buffer, packet);
        } catch (Exception e) {
            warn("Failed to encode " + packetClass.getSimpleName() + " with " + protocol.name() + "/" + state.name() + " Codec [" + codec.name() + "]");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        Buffer buffer = new Buffer(in);
        int id = buffer.readVarInt();

        //info("Received packet ID " + id);

        Session session = Sessions.get(ctx);
        State state = session.state();
        Protocol protocol = session.protocol();

        PacketRegistry packets = protocol.serverBoundPackets(state);
        CodecRegistry codecs = protocol.serverBoundCodecs(state);

        if (packets == null || codecs == null) {
            error("No ServerBound Packets/Codecs are registered for " + protocol.name() + "/" + state.name());
            return;
        }

        Class<? extends Packet> packetClass = packets.find(id);
        Codec codec = codecs.find(id);

        if (packetClass == null) {
            warn("Unknown Packet by Id " + id);
            buffer.skipBytes(buffer.readableBytes());
            return;
        }

        if (codec == null) {
            error("Unregistered ServerBound Codec for " + protocol.name() + "/" + state.name() + ":" + packetClass.getSimpleName());
            buffer.skipBytes(buffer.readableBytes());
            return;
        }

        Packet packet = packetClass.newInstance();

        try {
            codec.decode(buffer, packet);
        } catch (Exception e) {
            warn("Failed to decode " + packetClass.getSimpleName() + " with " + protocol.name() + "/" + state.name() + " Codec [" + codec.name() + "] " + C.Line + e.getMessage());
        }

        if (buffer.readableBytes() > 0) {
            warn("Didn't read all bytes for " + protocol.name() + "/" + state.name() + " Codec [" + codec.name() + "]");
            buffer.skipBytes(buffer.readableBytes());
        }

        out.add(packet);
    }
}
