package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityHeadLook;

public class CodecEntityHeadLook implements Codec<PacketEntityHeadLook> {

    @Override
    public void encode(Buffer buffer, PacketEntityHeadLook packet) {
        buffer.writeVarInt(packet.entityId());
        buffer.writeAngle((float) packet.headYaw());
    }

    @Override
    public void decode(Buffer buffer, PacketEntityHeadLook packet) {
        packet.EntityId(buffer.readVarInt());
        packet.HeadYaw(buffer.readAngle());
    }
}
