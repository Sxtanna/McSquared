package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.StoneBrickType;

public class StoneBrick extends MaterialData<StoneBrick, StoneBrickType> {

    public StoneBrick(int data) {
        super(Material.StoneBrick, data);
    }

    public StoneBrick() {
        super(Material.StoneBrick);
    }

    public StoneBrick(StoneBrickType data) {
        super(Material.StoneBrick, data);
    }
}
