package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum DirtType implements MaterialTypeData<DirtType> {

    Plain,
    Coarse,
    Podzol;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public DirtType byData(int data) {
        if (data < 0 || data > 2) return Plain;
        return values()[data];
    }
}
