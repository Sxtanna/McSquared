package com.sxt.mc.server.entity;

import com.sxt.mc.util.math.Vector;

public abstract class LivingEntity<E extends LivingEntity> extends Entity<E> {

    private final Vector prevPosition = new Vector();
    private double prevPitch, prevYaw, prevHeadYaw;

    private double health;

    @Override
    public abstract void init();

    public Vector prevPosition() {
        return prevPosition;
    }

    public double prevPitch() {
        return prevPitch;
    }

    public LivingEntity PrevPitch(double prevPitch) {
        this.prevPitch = prevPitch;
        return this;
    }

    public double prevYaw() {
        return prevYaw;
    }

    public LivingEntity PrevYaw(double prevYaw) {
        this.prevYaw = prevYaw;
        return this;
    }

    public double prevHeadYaw() {
        return prevHeadYaw;
    }

    public LivingEntity PrevHeadYaw(double prevHeadYaw) {
        this.prevHeadYaw = prevHeadYaw;
        return this;
    }

    public double health() {
        return health;
    }

    public LivingEntity Health(double health) {
        this.health = health;
        return this;
    }
}
