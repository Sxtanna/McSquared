package com.sxt.mc2.database.base;

import com.sxt.mc.util.task.CallEx;
import com.sxt.mc.util.type.Builder;
import com.sxt.mc.util.type.Logging;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class Database<D extends Database, P, C extends AutoCloseable> implements Logging, Builder<D> {

    protected static final ExecutorService Executor = Executors.newCachedThreadPool();

    protected boolean loaded = false, connected = false;

    public D startup() {
        if (loaded) return thisClass();
        load();

        return thisClass();
    }

    public D shutdown() {
        try {
            poison();
        } catch (Exception e) {
            error("Error when Shutting down " + e.getMessage());
            e.printStackTrace();
        }
        return thisClass();
    }

    protected abstract void load();

    protected abstract void poison() throws Exception;

    public abstract C resource();

    public abstract P pool();

    public abstract void execute(CallEx<C, Exception> call);
}
