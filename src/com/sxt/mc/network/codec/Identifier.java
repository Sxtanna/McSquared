package com.sxt.mc.network.codec;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.util.type.Logging;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

public class Identifier extends ByteToMessageCodec<ByteBuf> implements Logging {

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) throws Exception {
        Buffer buffer = new Buffer(out);

        buffer.writeVarInt(msg.readableBytes());
        buffer.writeBytes(msg);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        in.markReaderIndex();

        if (!readableVarInt(in)) return;

        Buffer buffer = new Buffer(in);
        int length = buffer.readVarInt();

        if (in.readableBytes() < length) in.resetReaderIndex();
        else out.add(in.readBytes(length));
    }

    private boolean readableVarInt(ByteBuf buf) {
        if (buf.readableBytes() > 5) return true;

        int idx = buf.readerIndex();
        byte in;

        do {
            if (buf.readableBytes() < 1) {
                buf.readerIndex(idx);
                return false;
            }
            in = buf.readByte();
        } while ((in & 0x80) != 0);

        buf.readerIndex(idx);
        return true;
    }
}
