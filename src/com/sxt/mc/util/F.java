package com.sxt.mc.util;

import com.sxt.mc.base.Color;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class F {

    private static final String Bracket = C.BGray + "|" + C.Reset;
    private static final String Seperator = C.BGray + C.Strike + "---------------------------------------------" + C.Reset;
    private static final String Chars = "0123456789AaBbCcDdEeFfKkLlMmNnOoRr";
    private static final Pattern Strip_Regex = Pattern.compile("(?i)" + String.valueOf(Color.Char) + "[0-9A-FK-OR]");
    private static final Pattern Url_Regex = Pattern.compile("^(?:(?:https?|ftp):\\/\\/)(?:\\S+(?::\\S*)?@)?(?:(?!(?:10|127)(?:\\.\\d{1,3}){3})(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))\\.?)(?::\\d{2,5})?(?:[/?#]\\S*)?$");


    public static final List EmptyList = new ArrayList() {
        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Cannot use this EmptyList");
        }
    };
    public static final Map EmptyMap = new HashMap() {
        @Override
        public Object put(Object key, Object value) {
            throw new UnsupportedOperationException("Cannot use this EmptyMap");
        }
    };
    public static final Set EmptySet = new HashSet() {
        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException("Cannot use this EmptySet");
        }
    };


    /**
     * Create a Header from a String
     * <p>
     * <p>|{String}|</p>
     *
     * @param string The Original String
     * @return The Header String
     */
    public static String head(String string) {
        return color(Bracket + (C.White + C.Bold) + string + Bracket);
    }

    public static String surround(String message) {
        return color(Seperator + C.Line + message + C.Line + Seperator);
    }

    /**
     * Format a String using the '&' codes
     *
     * @param message The Original String
     * @return The Formatted String
     */
    public static String color(String message) {
        char[] chars = message.toCharArray();

        for (int i = 0; i < chars.length - 1; i++) {
            if (chars[i] == '&' && Chars.indexOf(chars[i + 1]) > -1) {
                chars[i] = Color.Char;
                chars[i + 1] = Character.toLowerCase(chars[i + 1]);
            }
        }
        return new String(chars);
    }

    /**
     * {@link #color(String)} Each provided String
     *
     * @param strings Input Strings
     * @return Formatted Strings
     */
    public static List<String> color(String... strings) {
        return map(strings, F::color).collect(Collectors.toList());
    }

    /**
     * Clear the Formatting of a String
     *
     * @param message The Original String
     * @return The Unformatted String
     */
    public static String clear(String message) {
        return Strip_Regex.matcher(message).replaceAll("");
    }

    /**
     * Show Colored True or False based on a Boolean
     *
     * @param input The Boolean
     * @return Green for True, Red for False
     */
    public static String trueFalse(boolean input) {
        return color((input ? C.Green + "True" : C.Red + "False"));
    }

    /**
     * Show Colored Enabled or Disabled based on a Boolean
     *
     * @param input The Boolean
     * @return Enabled if True, Disabled if False
     */
    public static String enDis(boolean input) {
        return color((input ? C.Green + "Enabled" : C.Red + "Disabled"));
    }

    /**
     * Properly Apostrophize a word
     *
     * @param word The word
     * @return The Apostrophized Word
     */
    public static String apost(String word) {
        return word + (word.toLowerCase().endsWith("s") ? "'" : "'s");
    }

    /**
     * Format a double to a certain number of places <tt>2 By Default</tt>
     *
     * @param number The Number
     * @param places The Amount of places
     * @return The formatted double
     */
    public static double format(double number, int... places) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(places.length > 0 ? places[0] : 2);
        return Double.parseDouble(nf.format(number).replace(",", ""));
    }

    /**
     * Format a float to a certain number of places <tt>2 By Default</tt>
     *
     * @param number The Number
     * @param places The Amount of places
     * @return The formatted float
     */
    public static float format(float number, int... places) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(places.length > 0 ? places[0] : 2);
        return Float.parseFloat(nf.format(number).replace(",", ""));
    }

    /**
     * Combine a list of Strings into a Single String
     * <p>With the possibility of a separator</p>
     *
     * @param strings The Strings
     * @param start   Whether the mod should be used at the start
     * @param mod     Possible Bracket
     * @return The Combined String
     */
    public static String combine(List<String> strings, boolean start, String... mod) {
        if (strings.size() == 0) return "[]";
        String sep = mod.length > 0 ? mod[0] : "", output = start ? sep : "";
        for (String string : strings) output += string + sep;
        return output.substring(0, output.length() - sep.length());
    }

    /**
     * Get the Levenshtein Distance between two Strings
     *
     * @param string  The String
     * @param oString The Other String
     * @return The distance
     */
    public static int distance(String string, String oString) {
        string = string.toLowerCase();
        oString = oString.toLowerCase();
        if (string.equals(oString)) return 0;
        if (string.length() == 0) return oString.length();
        if (oString.length() == 0) return string.length();

        int[] costs = new int[oString.length() + 1];
        for (int i = 1; i <= string.length(); i++) {
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= oString.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), string.charAt(i - 1) == oString.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[oString.length()];
    }

    /**
     * Check if a String is Numeric
     *
     * @param string The String
     * @return True if the String is a number
     */
    public static boolean numeric(String string) {
        try {
            Double.parseDouble(string);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * I promise this works, don't complain
     *
     * @param numText The Number in Text
     * @return A generic Number
     */
    public static Number number(String numText) {
        if (numText == null || numText.isEmpty()) return 0;

        Number num = null;

        try {
            num = NumberFormat.getInstance().parse(numText);
        } catch (Exception ignored) {
        }

        return null == num ? 0 : num;
    }

    /**
     * Get a boolean value from a string
     *
     * @param boolText The Boolean in Text
     * @return The value
     */
    public static Boolean bool(String boolText) {
        boolText = ((equals("yes", boolText) || equals("on", boolText)) ? "true" : (equals("no", boolText) || equals("off", boolText)) ? "false" : boolText);

        return Boolean.parseBoolean(boolText);
    }

    /**
     * Clamp a Value between a Max and a Min
     *
     * @param val The Value
     * @param min The Minimum
     * @param max The Maximum
     * @return The clamped Value
     */
    public static <N extends Comparable<N>> N clamp(N val, N min, N max) {
        return val.compareTo(min) < 0 ? min : val.compareTo(max) > 0 ? max : val;
    }

    /**
     * Better String split
     *
     * @param string The String
     * @param by     What char to split by
     * @return A List containing the split Strings
     */
    public static List<String> split(String string, char by) {
        int point = 0, max = string.length(), spaces = 0;
        while (point++ < max - 1) if (string.charAt(point) == by) spaces++;
        String[] words = new String[++spaces];
        point = spaces = 0;

        StringBuilder b = new StringBuilder(50);
        while (point < max) {
            char on = string.charAt(point++);
            if (on == by) {
                words[spaces++] = b.toString();
                b.setLength(0);
            } else b.append(on);
        }
        words[spaces] = b.toString();
        return list(words);
    }

    /**
     * Repeat a String a specified amount of times
     *
     * @param str    The String
     * @param repeat The amount of times
     * @return The repeated String
     */
    public static String repeat(String str, int repeat) {
        if (str == null) return null;
        if (repeat <= 0) return "";

        final int inputLength = str.length();
        if (repeat == 1 || inputLength == 0) return str;

        final int outputLength = inputLength * repeat;
        switch (inputLength) {
            case 1:
                return repeat(str.charAt(0), repeat);
            case 2:
                final char ch0 = str.charAt(0), ch1 = str.charAt(1);
                final char[] output2 = new char[outputLength];
                for (int i = repeat * 2 - 2; i >= 0; i--, i--) {
                    output2[i] = ch0;
                    output2[i + 1] = ch1;
                }
                return new String(output2);
            default:
                final StringBuilder buf = new StringBuilder(outputLength);
                for (int i = 0; i < repeat; i++) buf.append(str);
                return buf.toString();
        }
    }

    /**
     * Repeat a Character a specified amount of times
     *
     * @param ch     The Character
     * @param repeat The amount of times
     * @return The repeated String
     */
    public static String repeat(char ch, int repeat) {
        final char[] buf = new char[repeat];
        for (int i = repeat - 1; i >= 0; i--) buf[i] = ch;
        return new String(buf);
    }

    /**
     * Format a Name to start with UpperCase after every Space or UnderScore
     *
     * @param input Input String
     * @return Formatted String
     * @apiNote <tt>Ex. </tt>xxx -> Xxx | xxx_xxx -> Xxx Xxx
     */
    public static String name(String input) {
        if (input == null) return "null";

        StringJoiner joiner = new StringJoiner(" ");
        for (String part : split(input.toLowerCase().replace(' ', '_'), '_'))
            joiner.add(part.substring(0, 1).toUpperCase() + part.substring(1));
        return joiner.toString();
    }

    /**
     * Check if a String is a URL
     *
     * @param urlText The String
     * @return True if this String is a URL
     */
    public static boolean url(String urlText) {
        return Url_Regex.matcher(urlText).matches();
    }

    /**
     * Get a {@link UUID} from a String
     *
     * @param uuidString The UUID String
     * @return A UUID if the String represents a UUID, null otherwise
     */
    public static UUID uuid(String uuidString) {
        if (uuidString == null || uuidString.length() + 4 < 32) return null;

        if (uuidString.length() < 36) {
            BigInteger integer = new BigInteger(uuidString, 16);
            return new UUID(integer.shiftRight(64).longValue(), integer.longValue());
        }
        try {
            return UUID.fromString(uuidString);
        } catch (Exception ignored) {
        }
        return null;
    }

    /**
     * Get a String from a {@link UUID}
     *
     * @param uuid The UUID
     * @param flat Whether to removed the '-' chars from the String
     * @return The String representation of this UUID
     */
    public static String uuid(UUID uuid, boolean flat) {
        String string = uuid.toString();
        return flat ? string.replace("-", "") : string;
    }

    /**
     * Check if two Strings are the same, ignoring case
     *
     * @param word  The String to check Against
     * @param check The String you're checking
     * @return True if they both match, and aren't null
     */
    public static boolean equals(String word, String check) {
        return !(word == null || check == null || word.length() != check.length()) && word.toLowerCase().equals(check.toLowerCase());
    }

    /**
     * Literally just a wrapper for String.valueOf()
     *
     * @param obj The Object
     * @return The String value of this Object
     */
    public static String string(Object obj) {
        return String.valueOf(obj);
    }

    //region To Array / To List

    /**
     * Get an Array of {@link O} from a List of {@link O}
     *
     * @param oClass The Type Class of {@link O}
     * @param oList  The List of {@link O}
     * @param <O>    The Object Type
     * @return The Array
     */
    @SuppressWarnings("unchecked")
    public static <O> O[] array(Class<O> oClass, List<O> oList) {
        if (oList == null) return ((O[]) new Object[0]);

        O[] array = ((O[]) Array.newInstance(oClass, oList.size()));
        for (int i = 0; i < oList.size(); i++) array[i] = oList.get(i);
        return array;
    }

    /**
     * The opposite of {@link #array(Class, List)}
     *
     * @param oArray The Array of {@link O}
     * @param <O>    The Object Type
     * @return The List
     */
    @SafeVarargs
    public static <O> List<O> list(O... oArray) {
        return new ArrayList<>(Arrays.asList(oArray));
    }
    //endregion

    //region To Stream from Array / Collection / Enum (List, Set)

    /**
     * Create a Stream from an Array
     *
     * @param oArray The Array
     * @param <O>    The Object Type
     * @return The Stream
     */
    public static <O> Stream<O> stream(O[] oArray) {
        return list(oArray).stream();
    }

    /**
     * Create a Stream from a Collection
     *
     * @param oColl The Collection
     * @param <O>   The Object Type
     * @return The Stream
     */
    public static <O> Stream<O> stream(Collection<O> oColl) {
        return oColl.stream();
    }

    /**
     * Create a Stream from the values of an Enum
     *
     * @param enumClass The Enum class
     * @return The Stream
     */
    public static <E extends Enum> Stream<E> enumStream(Class<? extends E> enumClass) {
        return stream(enumClass.getEnumConstants());
    }
    //endregion

    //region Filter and return a Stream from Array / Collection (List, Set)

    /**
     * Get a Stream after filtering an Array
     *
     * @param oArray    The Array
     * @param predicate The Filter
     * @param <O>       The Object Type
     * @return The filtered Stream
     */
    public static <O> Stream<O> filter(O[] oArray, Predicate<? super O> predicate) {
        return stream(oArray).filter(predicate);
    }

    /**
     * Get a Stream after filtering a Collection
     *
     * @param oColl     The Collection
     * @param predicate The Filter
     * @param <O>       The Object Type
     * @return The filtered Stream
     */
    public static <O> Stream<O> filter(Collection<O> oColl, Predicate<? super O> predicate) {
        return stream(oColl).filter(predicate);
    }
    //endregion

    //region Map and return a Stream from Array / Collection (List, Set)

    /**
     * Get a Stream after Mapping an Array
     *
     * @param oArray The Array
     * @param mapper The Mapper
     * @param <O>    The Object Type
     * @param <R>    The Object Mapped Type
     * @return The Mapped Stream
     */
    public static <O, R> Stream<R> map(O[] oArray, Function<? super O, ? extends R> mapper) {
        return stream(oArray).map(mapper);
    }

    /**
     * Get a Stream after Mapping a Collection
     *
     * @param oColl  The Collection
     * @param mapper The Mapper
     * @param <O>    The Object Type
     * @param <R>    The Object Mapped Type
     * @return The Mapped Stream
     */
    public static <O, R> Stream<R> mapFilter(Collection<O> oColl, Predicate<? super O> predicate, Function<? super O, ? extends R> mapper) {
        return stream(oColl).filter(predicate).map(mapper);
    }

    /**
     * Get a Stream after Mapping a Collection
     *
     * @param oColl  The Collection
     * @param mapper The Mapper
     * @param <O>    The Object Type
     * @param <R>    The Object Mapped Type
     * @return The Mapped Stream
     */
    public static <O, R> Stream<R> map(Collection<O> oColl, Function<? super O, ? extends R> mapper) {
        return stream(oColl).map(mapper);
    }

    public static <O, R> Stream<R> mapFrom(O insert, Function<O, Stream<R>> mapper) {
        return mapper.apply(insert);
    }
    //endregion

    //region Check Sizes of Strings, Collections (List, Set), and Arrays

    /**
     * Check whether a String's length is between a size range
     *
     * @param input     The String
     * @param low       The Lowest Possible size
     * @param high      The Highest Possible size
     * @param inclusive If it should be inclusive "can be the lowest or highest"
     * @return True if the bounds match
     */
    public static boolean size(String input, int low, int high, boolean inclusive) {
        return inclusive ? input.length() >= low && input.length() <= high : input.length() > low && input.length() < high;
    }

    /**
     * Check if a String's length is this Size
     *
     * @param input The String
     * @param size  The Size
     * @return True if the size matches
     */
    public static boolean size(String input, int size) {
        return size(input, size, size, true);
    }

    /**
     * Check if a String is one of these sizes
     *
     * @param input The String
     * @param sizes The possible sizes
     * @return True if its one of these sizes
     */
    public static boolean sizes(String input, Integer... sizes) {
        return stream(sizes).anyMatch(size -> input.length() == size);
    }

    /**
     * Same as {@link #size(String, int, int, boolean)} but for Collection size
     */
    public static <O> boolean size(Collection<O> collection, int low, int high, boolean inclusive) {
        return null != collection && (inclusive ? collection.size() >= low && collection.size() <= high : collection.size() > low && collection.size() < high);
    }

    /**
     * Same as {@link #size(String, int)} but for Collection size
     */
    public static <O> boolean size(Collection<O> collection, int size) {
        return size(collection, size, size, true);
    }

    /**
     * Same as {@link #size(String, int, int, boolean)} but for Array size
     */
    public static <O> boolean size(O[] oArray, int low, int high, boolean inclusive) {
        return null != oArray && (inclusive ? oArray.length >= low && oArray.length <= high : oArray.length > low && oArray.length < high);
    }

    /**
     * Same as {@link #size(String, int)} but for Array size
     */
    public static <O> boolean size(O[] oArray, int size) {
        return size(oArray, size, size, true);
    }
    //endregion
}
