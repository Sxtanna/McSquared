package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum TallGrassType implements MaterialTypeData<TallGrassType> {

    Grass,
    Fern,
    DeadBush;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public TallGrassType byData(int data) {
        if (data < 0 || data > 2) return Grass;
        return values()[data];
    }
}
