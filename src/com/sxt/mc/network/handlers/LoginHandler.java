package com.sxt.mc.network.handlers;

import com.sxt.mc.base.chat.TextComponent;
import com.sxt.mc.network.auth.Authenticator;
import com.sxt.mc.protocol.event.base.PacketWatcher;
import com.sxt.mc.session.Session;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc.util.uU.CryptoU;
import com.sxt.mc.v1_8.packet.login.PacketLoginRequest;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionRequest;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionResponse;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.protocol.packet.login.PacketKick;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class LoginHandler implements Logging {

    private final CryptoU Crypto;

    private final byte[] PublicKey;
    private final PrivateKey PrivateKey;

    public LoginHandler() {
        Crypto = new CryptoU();

        KeyPair keyPair = McSquared.KeyPair();

        PublicKey = Crypto.toX509(keyPair.getPublic()).getEncoded();
        PrivateKey = keyPair.getPrivate();
    }

    @PacketWatcher
    public void onRequest(PacketLoginRequest packet) {
        Session session = packet.session();

        byte[] verifyToken = Crypto.generateToken(4);

        session.vUsername(packet.username());
        session.vToken(verifyToken);

        PacketEncryptionRequest response = new PacketEncryptionRequest(session.SessionId(), PublicKey, verifyToken);
        session.send(response);
    }

    @PacketWatcher
    public void onResponse(PacketEncryptionResponse packet) {
        Session session = packet.session();

        Cipher cipher;

        try {
            cipher = Cipher.getInstance("RSA");
        }
        catch (Exception e) {
            error("Could not get RSA Cipher");
            e.printStackTrace();

            session.disconnect(new PacketKick("Failed to initialize Authentication")); // Kick because Auth Init failed
            return;
        }

        SecretKey secretKey;

        try {
            cipher.init(Cipher.DECRYPT_MODE, PrivateKey);
            secretKey = new SecretKeySpec(cipher.doFinal(packet.sharedKey()), "AES");
        }
        catch (Exception e) {
            error("Failed to decrypt shared key");
            e.printStackTrace();

            session.disconnect(new PacketKick("Key Authentication Failed")); // Kick because SharedKey decrypt failed
            return;
        }

        byte[] vToken;

        try {
            cipher.init(Cipher.DECRYPT_MODE, PrivateKey);
            vToken = cipher.doFinal(packet.vToken());
        }
        catch (Exception e) {
            error("Failed to decrypt verifying token");
            e.printStackTrace();

            session.disconnect(new PacketKick(new TextComponent("Token Decrypt failed"))); // Kick because Verifying Token decrypt failed
            return;
        }

        if (!Arrays.equals(vToken, session.vToken())) session.disconnect(new PacketKick("Invalid Verifying Token")); // Kick because Invalid Verifying Token
        else {
            session.Encryption(secretKey);

            String hash;

            try {
                final MessageDigest digest = MessageDigest.getInstance("SHA-1");

                digest.update(session.SessionId().getBytes());
                digest.update(secretKey.getEncoded());
                digest.update(PublicKey);

                hash = new BigInteger(digest.digest()).toString(16);
            }
            catch (Exception e) {
                error("Failed to generate SHA-1 Hash");
                e.printStackTrace();

                session.disconnect(new PacketKick("Failed to hash SessionID")); // Kick because Session Hash failed
                return;
            }

            Thread authThread = new Thread(new Authenticator(session, hash));
            authThread.setName("Authenticator["+ session.vUsername() + "]");

            authThread.start();
        }
    }
}
