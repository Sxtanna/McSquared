package com.sxt.mc2.protocol.packet.status;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc2.protocol.codec.status.CodecStatusRequest;

@PacketInfo(target = Target.Server, state = State.Status, id = 0, codec = CodecStatusRequest.class)
public class PacketStatusRequest extends Packet {
}
