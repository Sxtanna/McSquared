package com.sxt.mc.util.task;

import com.sxt.mc.util.type.Logging;

public interface CallEx<D, E> extends Call<D>, Logging {

    @Override
    default void call(D data) {
        try {
            callEx(data);
        } catch (Exception e) {
            error("Failed to CallEx");
            e.printStackTrace();
        }
    }

    void callEx(D data) throws Exception;

    default void fail(E fail) {
    }
}
