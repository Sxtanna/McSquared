package com.sxt;

import com.sxt.mc.base.Version;
import com.sxt.mc.protocol.AdaptingProtocol;
import com.sxt.mc.protocol.base.ProtocolNameRegistry;
import com.sxt.mc.protocol.base.ProtocolRegistry;
import com.sxt.mc.util.F;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc.util.type.Server;
import com.sxt.mc.util.uU.ReflectionU;
import com.sxt.mc.v1_8.ProtocolV1_8;
import com.sxt.mc.v1_9.ProtocolV1_9;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.config.VersionRegistry;
import com.sxt.mc2.config.server.Property;
import com.sxt.mc2.config.server.ServerConfig;
import com.sxt.mc2.task.McTask;
import test.CustomEvents;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

@SuppressWarnings("unchecked")

//Random comment.

public class Main {

    public static final File RootFolder = new File("").getAbsoluteFile(), DataFolder = new File(RootFolder, "Data");

    private static final VersionRegistry Versions = new VersionRegistry();
    private static final ProtocolRegistry Protocols = new ProtocolRegistry();
    private static final ProtocolNameRegistry ProtocolNames = new ProtocolNameRegistry();

    static {
        try {
            Versions.register(47, ((Class<? extends Version>) Class.forName("com.sxt.mc.v1_8.McServer1_8")));
            Versions.register(110, ((Class<? extends Version>) Class.forName("com.sxt.mc.v1_9.McServer1_9")));

            Protocols.register(new ProtocolV1_8());
            Protocols.register(new ProtocolV1_9());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        ServerConfig config = ServerConfig.get();

        //region System Properties
        System.setProperty("org.slf4j.simpleLogger.levelInBrackets", "true");
        System.setProperty("org.slf4j.simpleLogger.showLogName", "false");
        System.setProperty("org.slf4j.simpleLogger.showThreadName", F.string(config.property(Property.ShowThread)));

        boolean showDateTime = true;
        String timeFormat = timeFormat(config);
        if (timeFormat == null) showDateTime = false;
        System.setProperty("org.slf4j.simpleLogger.showDateTime", F.string(showDateTime));
        System.setProperty("org.slf4j.simpleLogger.dateTimeFormat", timeFormat == null ? "" : timeFormat);
        //endregion

        if (config.defaultServer()) {
            final Class<? extends Server> serverClass = Versions.find(config.protocol());
            if (serverClass == null)
                System.out.println("Unknown server version '" + config.protocol() + "'");
            else {
                Server server = ReflectionU.newInstance(serverClass, config);
                if (server == null) System.out.println("Failed to create Server instance");
                else {
                    McSquared.Server(server);
                    McSquared.Server().Network().bind(config.port());

                    // TODO: 8/2/2016 Begin plugin loading
                    McSquared.Events().register(new CustomEvents());
                }
            }
        }

        McTask.Executor().execute(() -> {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                while (true) {
                    String input = in.readLine();

                    if (input.equalsIgnoreCase("exit")) {
                        Logging.logger().info("Console executed command: " + input);
                        System.exit(0);
                        return;
                    }
                }
            } catch (Exception ignored) {
            }
        });

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Server shutting down...");
            McSquared.Server().Network().shutdown();
            McTask.clear();

            Versions.release();
            Protocols.release();
            ProtocolNames.release();
            AdaptingProtocol.clear();
        }));
    }

    private static String timeFormat(ServerConfig config) {
        boolean showTimeValue = F.bool(F.string(config.property(Property.ShowTime))), showDateValue = F.bool(F.string(config.property(Property.ShowDate))), both = showTimeValue && showDateValue;

        String dateFormat = F.string(config.property(Property.DateFormat)), timeFormat = F.string(config.property(Property.TimeFormat));
        return both ? (dateFormat + " " + timeFormat) : showTimeValue ? timeFormat : showDateValue ? dateFormat : null;
    }

    public static VersionRegistry Versions() {
        return Versions;
    }

    public static ProtocolRegistry Protocols() {
        return Protocols;
    }

    public static ProtocolNameRegistry ProtocolNames() {
        return ProtocolNames;
    }
}
