package com.sxt.mc.server.components;

import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.Component;
import com.sxt.mc.server.GameObject;
import com.sxt.mc.server.base.AttributeKey;
import com.sxt.mc.server.base.Attributes;
import com.sxt.mc.server.base.Observer;
import com.sxt.mc.server.base.Viewable;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.packet.play.player.PacketPlayerTeleport;

import java.util.ArrayList;
import java.util.List;

public class Camera extends Component<Camera> implements Observer, Viewable {

    public static final AttributeKey<Camera> CameraAttribute = AttributeKey.create();
    private static final double range = 50, squareRange = range * range;


    private final List<Observer> Observers = new ArrayList<>();

    private final Vector     RemotePosition = new Vector<>(0, 0, 0);
    private final Quaternion RemoteRotation = Quaternion.identity();

    private Attributes attributes = new Attributes();


    public Camera RemotePosition(Vector remotePosition) {
        RemotePosition.Set(remotePosition);
        GameObject.transform().Position(remotePosition);
        return this;
    }

    public Camera RemoteRotation(Quaternion remoteRotation) {
        RemoteRotation.Set(remoteRotation);
        GameObject.transform().Rotation(remoteRotation);
        return this;
    }

    @Override
    public void Add(Observer observer) {
        Attributes storage = observer.attributes();

        Camera oldCamera = storage.get(CameraAttribute);
        if (oldCamera != null) oldCamera.Remove(observer);

        Observers.add(observer);
        storage.set(CameraAttribute, this);

        observer.observe(teleport());
    }

    @Override
    public void Remove(Observer observer) {
        if (Observers.remove(observer)) observer.attributes().set(CameraAttribute, null);
    }

    @Override
    public void observe(Packet packet) {
        Observers.forEach(observer -> observer.observe(packet));
    }

    @Override
    public Attributes attributes() {
        return attributes;
    }

    @Override
    public void update() {
        Transform transform = GameObject.transform();
        //info("Update");

        if (!transform.Position().equals(RemotePosition)) {
            info("Position isnt the same");
            transform.Position().Set(RemotePosition);
        }

        if (!transform.Rotation().equals(RemoteRotation)) {
            info("Rotation isnt the same");
            transform.Rotation().Set(RemoteRotation);
        }

        //info("Pos = " + transform.Position().to());
        //info("Rot = " + transform.Rotation().to());
    }

    private PacketPlayerTeleport teleport() {
        PacketPlayerTeleport packet = new PacketPlayerTeleport();

        Transform transform = GameObject.transform();
        packet.Position(transform.Position());
        packet.Rotation(transform.Rotation());

        return packet;
    }

    private void update(GameObject root) {
        if (root == null) return;

        double distance = GameObject.transform().distanceSquared(root.transform());
        boolean observing = root.hasObserver(this);

        if (!observing && distance <= squareRange) root.start(this);
        else if (observing && distance >= squareRange) root.stop(this);

        root.forEachChild(this::update);
    }

}
