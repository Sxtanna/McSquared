package com.sxt.mc.base;

import com.sxt.Main;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.util.type.Server;
import com.sxt.mc2.config.server.ServerConfig;

public abstract class Version implements Server {

    private final ServerConfig Config;
    private final Protocol Protocol;

    public Version(ServerConfig config) {
        Config = config;
        Protocol = Main.Protocols().find(config.protocol());
    }

    @Override
    public ServerConfig Config() {
        return Config;
    }

    @Override
    public Protocol Protocol() {
        return Protocol;
    }
}
