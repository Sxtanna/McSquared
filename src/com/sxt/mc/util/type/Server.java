package com.sxt.mc.util.type;

import com.sxt.mc.log.LogProvider;
import com.sxt.mc.network.NetworkServer;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.event.PacketBus;
import com.sxt.mc.server.level.Level;
import com.sxt.mc.session.SessionServer;
import com.sxt.mc2.config.server.ServerConfig;
import com.sxt.mc2.event.EventBus;

import org.slf4j.Logger;

import java.io.File;
import java.util.Map;
import java.util.UUID;

public interface Server extends Named {

    LogProvider LogProvider();

    Logger Logger();

    EventBus Events();

    PacketBus Packets();

    File ServerFolder();

    File DataFolder();

    ServerConfig Config();

    NetworkServer Network();

    SessionServer Sessions();

    Protocol Protocol();

    Map<UUID, Level> Levels();
}
