package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.base.PlayerInfo;
import com.sxt.mc.base.PlayerInfoAction;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.entity.Player;
import com.sxt.mc.util.F;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayerInfo;

import java.util.List;
import java.util.stream.Collectors;

@PacketInfo(target = Target.Client, state = State.Play, id = 56, codec = CodecPlayerInfo.class)
public class PacketPlayerInfo extends Packet {

    private PlayerInfoAction action;
    private List<PlayerInfo> infos;

    public PacketPlayerInfo(PlayerInfoAction action, List<PlayerInfo> infos) {
        this.action = action;
        this.infos = infos;
    }

    public PacketPlayerInfo(PlayerInfoAction action, PlayerInfo... infos) {
        this.action = action;
        this.infos = F.list(infos);
    }

    public PacketPlayerInfo(PlayerInfoAction action, Player... player) {
        this.action = action;
        this.infos = F.map(player, PlayerInfo::new).collect(Collectors.toList());;
    }

    public PacketPlayerInfo() {}


    public PlayerInfoAction action() {
        return action;
    }

    public PacketPlayerInfo Action(PlayerInfoAction action) {
        this.action = action;
        return this;
    }

    public List<PlayerInfo> infos() {
        return infos;
    }

    public PacketPlayerInfo Infos(List<PlayerInfo> infos) {
        this.infos = infos;
        return this;
    }
}
