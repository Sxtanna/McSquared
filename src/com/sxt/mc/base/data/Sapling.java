package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;

public class Sapling extends MaterialData<Sapling, WoodType> {

    public Sapling(int data) {
        super(Material.Sapling, data);
    }

    public Sapling() {
        super(Material.Sapling);
    }

    public Sapling(WoodType data) {
        super(Material.Sapling, data);
    }

}
