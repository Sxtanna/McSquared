package com.sxt.mc.util;

import org.apache.commons.lang3.Validate;

import java.util.Arrays;

public final class NibbleArray {

    private final byte[] Data;

    /**
     * Construct a new NibbleArray with the given size in nibbles.
     *
     * @param size The number of nibbles in the array.
     * @throws IllegalArgumentException If size is not positive and even.
     */
    public NibbleArray(int size) {
        Validate.isTrue(size > 0 && size % 2 == 0, "size must be positive even number, not " + size);
        Data = new byte[size / 2];
    }

    /**
     * Construct a new NibbleArray using the given underlying bytes. No copy
     * is created.
     *
     * @param data The raw Data to use.
     */
    public NibbleArray(byte[] data) {
        this.Data = data;
    }

    /**
     * Get the size in nibbles.
     *
     * @return The size in nibbles.
     */
    public int size() {
        return 2 * Data.length;
    }

    /**
     * Get the size in bytes, one-half the size in nibbles.
     *
     * @return The size in bytes.
     */
    public int byteSize() {
        return Data.length;
    }

    /**
     * Get the nibble at the given index.
     *
     * @param index The nibble index.
     * @return The value of the nibble at that index.
     */
    public byte get(int index) {
        byte val = Data[index / 2];
        return index % 2 == 0 ? (byte) (val & 0x0f) : (byte) ((val & 0xf0) >> 4);
    }

    /**
     * Set the nibble at the given index to the given value.
     *
     * @param index The nibble index.
     * @param value The new value to store.
     */
    public void set(int index, byte value) {
        value &= 0xf;
        int half = index / 2;
        byte previous = Data[half];
        if (index % 2 == 0) {
            Data[half] = (byte) ((previous & 0xf0) | value);
        } else {
            Data[half] = (byte) ((previous & 0x0f) | (value << 4));
        }
    }

    /**
     * Fill the nibble array with the specified value.
     *
     * @param value The value nibble to fill with.
     */
    public void fill(byte value) {
        value &= 0xf;
        Arrays.fill(Data, (byte) ((value << 4) | value));
    }

    /**
     * Get the raw bytes of this nibble array. Modifying the returned array
     * will modify the internal representation of this nibble array.
     *
     * @return The raw bytes.
     */
    public byte[] rawData() {
        return Data;
    }

    /**
     * Copies into the raw bytes of this nibble array from the given source.
     *
     * @param source The array to copy from.
     * @throws IllegalArgumentException If source is not the correct length.
     */
    public void RawData(byte[] source) {
        Validate.isTrue(source.length == Data.length, "expected byte array of length " + Data.length + ", not " + source.length);
        System.arraycopy(source, 0, Data, 0, source.length);
    }

    /**
     * Take a snapshot of this NibbleArray which will not reflect changes.
     *
     * @return The snapshot NibbleArray.
     */
    public NibbleArray snapshot() {
        return new NibbleArray(Data.clone());
    }
}
