package com.sxt.mc2.event.base;

public enum EventPriority {

    First,
    Normal,
    Last,
    Monitor

}
