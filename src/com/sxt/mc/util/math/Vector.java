package com.sxt.mc.util.math;

import com.sxt.mc.util.type.Builder;
import com.sxt.mc.util.type.Gsonable;

public class Vector<V extends Vector> implements Gsonable<Vector>, Builder<V> {

    protected double x, y, z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector() {}

    public Vector(V other) {
        Set(other.x(), other.y(), other.z());
    }

    public double distance(Vector other) {
        final double dx = this.x - other.x(), dy = this.y - other.y(), dz = this.z - other.z();
        return length(dx, dy, dz);
    }

    public double distanceSquared(Vector other) {
        final double dx = this.x - other.x, dy = this.y - other.y, dz = this.z - other.z;
        return lengthSquared(dx, dy, dz);
    }

    public Vector subtract(Vector other) {
        return new Vector<>(this.x - other.x(), this.y - other.y(), this.z - other.z());
    }

    public V Set(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return thisClass();
    }

    public V Set(V other) {
        return Set(other.x(), other.y(), other.z());
    }

    public double x() {
        return x;
    }

    public V X(double x) {
        this.x = x;
        return thisClass();
    }

    public double y() {
        return y;
    }

    public V Y(double y) {
        this.y = y;
        return thisClass();
    }

    public double z() {
        return z;
    }

    public V Z(double z) {
        this.z = z;
        return thisClass();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Vector) {
            Vector vector = (Vector) other;

            return (Math.abs(this.x - vector.x) < 0.001 &&
                    Math.abs(this.y - vector.y) < 0.001 &&
                    Math.abs(this.z - vector.z) < 0.001);
        }
        return false;
    }

    @Override
    public String toString() {
        return to();
    }

    @Override
    public Class<? extends Vector> type() {
        return Vector.class;
    }

    public static Vector back() {
        return new Vector<>(0, 0, -1);
    }

    public static Vector forward() {
        return new Vector<>(0, 0, 1);
    }

    public static Vector down() {
        return new Vector<>(0, -1, 0);
    }

    public static Vector up() {
        return new Vector<>(0, 1, 0);
    }

    public static Vector left() {
        return new Vector<>(-1, 0, 0);
    }

    public static Vector right() {
        return new Vector<>(1, 0, 0);
    }

    public static Vector zero() {
        return new Vector<>(0, 0, 0);
    }

    public static Vector one() {
        return new Vector<>(1, 1, 1);
    }

    public static double length(double x, double y, double z) {
        return Math.sqrt(lengthSquared(x, y, z));
    }

    public static double lengthSquared(double x, double y, double z) {
        return x * x + y * y + z * z;
    }
}
