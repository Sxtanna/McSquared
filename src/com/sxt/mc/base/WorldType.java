package com.sxt.mc.base;

public class WorldType {

    private Dimension dimension;
    private LevelType levelType;

    public WorldType(Dimension dimension, LevelType levelType) {
        this.dimension = dimension;
        this.levelType = levelType;
    }

    public Dimension dimension() {
        return dimension;
    }

    public LevelType levelType() {
        return levelType;
    }
}
