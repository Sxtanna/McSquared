package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;

public class Log extends MaterialData<Sapling, WoodType> {

    public Log(int data) {
        super(Material.Log, data);
    }

    public Log() {
        super(Material.Log);
    }

    public Log(WoodType data) {
        super(Material.Log, data);
    }

}
