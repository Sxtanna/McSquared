package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayer;

@PacketInfo(target = Target.Server, state = State.Play, id = 3, codec = CodecPlayer.class)
public class PacketPlayer extends Packet {

    private boolean onGround;

    public PacketPlayer(boolean onGround) {
        this.onGround = onGround;
    }

    public PacketPlayer() {}


    public boolean onGround() {
        return onGround;
    }

    public PacketPlayer OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }
}
