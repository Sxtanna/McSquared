package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityLook;

public class CodecEntityLook implements Codec<PacketEntityLook> {

    @Override
    public void encode(Buffer buffer, PacketEntityLook packet) {
        buffer.writeVarInt(packet.entityId());
        buffer.writeAngle(packet.yaw());
        buffer.writeAngle(packet.pitch());
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketEntityLook packet) {
        packet.EntityId(buffer.readVarInt());
        packet.Yaw(buffer.readAngle());
        packet.Pitch(buffer.readAngle());
        packet.OnGround(buffer.readBoolean());
    }
}
