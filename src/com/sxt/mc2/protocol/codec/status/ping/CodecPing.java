package com.sxt.mc2.protocol.codec.status.ping;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc2.protocol.packet.status.ping.PacketPing;

public class CodecPing implements Codec<PacketPing> {

    @Override
    public void encode(Buffer buffer, PacketPing packet) {
        buffer.writeLong(packet.time());
    }

    @Override
    public void decode(Buffer buffer, PacketPing packet) {
        packet.Time(buffer.readLong());
    }
}
