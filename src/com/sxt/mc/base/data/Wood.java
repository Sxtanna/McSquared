package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;

public class Wood extends MaterialData<Wood, WoodType> {

    public Wood(int data) {
        super(Material.Wood, data);
    }

    public Wood() {
        super(Material.Wood);
    }

    public Wood(WoodType data) {
        super(Material.Wood, data);
    }

}
