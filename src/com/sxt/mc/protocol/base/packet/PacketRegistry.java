package com.sxt.mc.protocol.base.packet;

import com.sxt.mc.protocol.base.State;
import com.sxt.mc.util.Registry;

public class PacketRegistry extends Registry<Class<? extends Packet>> {

    private final State state;

    public PacketRegistry(State state) {
        this.state = state;
    }

    public State state() {
        return state;
    }
}
