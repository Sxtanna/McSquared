package com.sxt.mc.v1_8.packet.play.entity;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.entity.Entity;
import com.sxt.mc.v1_8.codec.play.entity.CodecEntity;

@PacketInfo(target = Target.Client, state = State.Play, id = 20, codec = CodecEntity.class)
public class PacketEntity extends Packet {

    private int entityId;

    public PacketEntity(int entityId) {
        this.entityId = entityId;
    }

    public PacketEntity(Entity entity) {
        this.entityId = entity.entityId();
    }

    public PacketEntity() {
    }


    public int entityId() {
        return entityId;
    }

    public PacketEntity EntityId(int entityId) {
        this.entityId = entityId;
        return this;
    }
}
