package com.sxt.mc.session;

import com.sxt.mc.base.chat.TextComponent;
import com.sxt.mc.network.handlers.EncryptionHandler;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.server.base.Attributes;
import com.sxt.mc.server.base.Observer;
import com.sxt.mc.session.profile.Profile;
import com.sxt.mc.util.F;
import com.sxt.mc.util.uU.RandomU;
import com.sxt.mc2.protocol.packet.login.PacketKick;

import javax.crypto.SecretKey;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;

public class Session implements Observer {

    private State state = State.HandShake;

    private Channel channel;
    private Protocol protocol;

    private Profile profile;

    private final String SessionId = Long.toString(RandomU.Random.nextLong(), 16).trim();
    private final Attributes Attributes;

    private String vUsername;
    private byte[] vToken;

    private String disconnectReason;

    public Session(Channel channel, Protocol protocol) {
        this.channel = channel;
        this.protocol = protocol;

        Attributes = new Attributes();
    }

    public Channel channel() {
        return channel;
    }

    public State state() {
        return state;
    }

    public Session State(State state) {
        this.state = state;
        return this;
    }

    public Protocol protocol() {
        return protocol;
    }

    public Session Protocol(Protocol protocol) {
        this.protocol = protocol;
        return this;
    }

    public Profile profile() {
        return profile;
    }

    public Session Profile(Profile profile) {
        this.profile = profile;
        return this;
    }


    public String SessionId() {
        return SessionId;
    }

    public String vUsername() {
        return vUsername;
    }

    public Session vUsername(String vUsername) {
        this.vUsername = vUsername;
        return this;
    }

    public byte[] vToken() {
        return vToken;
    }

    public Session vToken(byte[] vToken) {
        this.vToken = vToken;
        return this;
    }

    public void Encryption(SecretKey secretKey) {
        updatePipe("encryption", new EncryptionHandler(secretKey));
    }

    private void updatePipe(String key, ChannelHandler handler) {
        channel.pipeline().replace(key, key, handler);
    }

    public void disconnect() {
        channel.close();
    }

    public void disconnect(Packet packet) {
        send(packet).addListener(future -> {
            if (packet instanceof PacketKick)
                disconnectReason = F.clear(((TextComponent) ((PacketKick) packet).message()).text());
            channel.close();
        });
    }

    public String disconnectReason() {
        return disconnectReason;
    }

    public ChannelFuture send(Packet packet) {
        return channel.writeAndFlush(packet);
    }

    @Override
    public void observe(Packet packet) {
        send(packet);
    }

    @Override
    public Attributes attributes() {
        return Attributes;
    }
}
