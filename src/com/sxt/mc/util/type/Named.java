package com.sxt.mc.util.type;

public interface Named {

    String name();
}
