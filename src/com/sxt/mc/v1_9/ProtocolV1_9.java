package com.sxt.mc.v1_9;

import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.base.packet.client.PacketKeepAlive;
import com.sxt.mc.v1_8.packet.login.PacketLoginRequest;
import com.sxt.mc.v1_8.packet.login.PacketLoginResponse;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionRequest;
import com.sxt.mc.v1_8.packet.login.encrypt.PacketEncryptionResponse;
import com.sxt.mc2.protocol.packet.status.PacketStatusRequest;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;
import com.sxt.mc2.protocol.packet.status.ping.PacketPing;
import com.sxt.mc2.protocol.packet.status.ping.PacketPong;

public class ProtocolV1_9 extends Protocol {

    @Override
    public int version() {
        return 110;
    }

    @Override
    public String name() {
        return "v1.9";
    }

    @Override
    protected void register() {
        super.register();

        // Status
        Register(PacketStatusRequest.class, PacketStatusResponse.class);
        Register(PacketPing.class, PacketPong.class);

        // Login
        Register(PacketLoginRequest.class, PacketLoginResponse.class);
        Register(PacketEncryptionRequest.class, PacketEncryptionResponse.class);

        // Play
        Register(PacketKeepAlive.class, 31);
    }
}
