package com.sxt.mc.util.uU;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class SortU {

    public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortByValue(Map<K, V> map) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>();

        TreeSet<Map.Entry<K, V>> entries = new TreeSet<>(new GenericValueCompare<>());
        entries.addAll(map.entrySet());

        for (Map.Entry<K, V> entry : entries) result.put(entry.getKey(), entry.getValue());
        return result;
    }

    public static <K extends Comparable<? super K>, V> LinkedHashMap<K, V> sortByKey(Map<K, V> map) {
        LinkedHashMap<K, V> result = new LinkedHashMap<>();

        TreeSet<Map.Entry<K, V>> entries = new TreeSet<>(new GenericKeyCompare<>());
        entries.addAll(map.entrySet());

        for (Map.Entry<K, V> entry : entries) result.put(entry.getKey(), entry.getValue());
        return result;
    }

    public static <O extends Comparable<? super O>> LinkedList<O> sort(List<O> list) {
        list.sort(new GenericObjectCompare<>());
        return new LinkedList<>(list);
    }

    public static <S extends Comparable<? super S>> Comparator<S> sorter(Class<S> sClass) {
        return new GenericObjectCompare<>();
    }

    private static class GenericKeyCompare<K extends Comparable<? super K>, V> implements Comparator<Map.Entry<K, V>> {

        public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
            return (o2.getKey()).compareTo(o1.getKey());
        }
    }

    private static class GenericValueCompare<K, V extends Comparable<? super V>> implements Comparator<Map.Entry<K, V>> {

        public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
            return (o2.getValue()).compareTo(o1.getValue());
        }
    }

    private static class GenericObjectCompare<O extends Comparable<? super O>> implements Comparator<O> {

        @Override
        public int compare(O o1, O o2) {
            return o2.compareTo(o1);
        }
    }

}
