package com.sxt.mc2.event.types.packet;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc2.event.base.Cancellable;
import com.sxt.mc2.event.base.Event;

public abstract class PacketEvent<E extends PacketEvent> extends Event implements Cancellable<E> {

    private final Packet packet;

    public PacketEvent(Packet packet) {
        this.packet = packet;
    }

    public Packet packet() {
        return packet;
    }

    public Class<? extends Packet> packetType() {
        return packet.getClass();
    }

    public boolean is(Class<? extends Packet> packet) {
        return packetType().isAssignableFrom(packet);
    }

    public <P extends Packet> P get(Class<P> packet) {
        return packet.cast(packet());
    }

    public PacketInfo packetInfo() {
        return packetType().getAnnotation(PacketInfo.class);
    }

    public Class<? extends Codec<? extends Packet>> codecType() {
        return packetInfo().codec();
    }

    public int packetID() {
        return packetInfo().id();
    }

    public State state() {
        return packetInfo().state();
    }
}
