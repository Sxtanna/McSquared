package com.sxt.mc.util;

import com.sxt.mc.util.type.Gsonable;
import com.sxt.mc.util.type.Logging;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class JsonFile implements Logging {

    private transient File file;

    public JsonFile(File file) {
        if (!file.getName().endsWith(".json"))
            this.file = new File(file.getParentFile(), file.getName() + ".json");
        else this.file = file;
    }

    public JsonFile(File file, Gsonable defaultFile) {
        this(file);
        if (!exists()) save(defaultFile);
    }

    public File file() {
        return file;
    }

    protected JsonFile File(File file) {
        this.file = file;
        return this;
    }

    @Override
    public String name() {
        return "JsonFile:" + file().getName();
    }

    public boolean exists() {
        return file.exists();
    }

    public String load() {
        StringBuilder builder = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file()));
            String output;
            while ((output = reader.readLine()) != null) builder.append(output);

            reader.close();
        } catch (Exception e) {
            error("Failed to read File " + file().getName());
            e.printStackTrace();
            builder = null;
        }
        return null == builder ? "null" : builder.toString();
    }

    public <G extends Gsonable> G load(Class<G> gClass) {
        String gString = load();

        return gString.equals("null") ? null : Gsonable.from(load(), gClass);
    }

    public File save(Gsonable gsonable) {
        return save(gsonable.to());
    }

    public File save(String json) {
        try {
            if (file.getParentFile() != null && !file.getParentFile().exists())
                file.getParentFile().mkdirs();
            boolean created = file.createNewFile();

            if (!created && !file().exists()) throw new Exception("Could not create File");
            else {
                FileWriter writer = new FileWriter(file());
                writer.write(json);

                writer.flush();
                writer.close();
            }
        } catch (Exception e) {
            error("Failed to save File " + file().getAbsolutePath());
            e.printStackTrace();
        }
        return file;
    }

}
