package com.sxt.mc.protocol;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.protocol.base.codec.CodecRegistry;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.protocol.base.packet.PacketRegistry;
import com.sxt.mc.protocol.base.packet.client.PacketHandShake;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc2.protocol.packet.login.PacketKick;
import com.sxt.mc2.protocol.packet.status.PacketStatusRequest;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;
import com.sxt.mc2.protocol.packet.status.ping.PacketPing;
import com.sxt.mc2.protocol.packet.status.ping.PacketPong;

import java.util.EnumMap;
import java.util.Map;

public abstract class Protocol implements Logging {

    private final Map<State, PacketRegistry> ServerBoundPackets = new EnumMap<>(State.class), ClientBoundPackets = new EnumMap<>(State.class);
    private final Map<State, CodecRegistry> ServerBoundCodecs = new EnumMap<>(State.class), ClientBoundCodecs = new EnumMap<>(State.class);

    public Protocol() {
        register();
    }

    public abstract int version();

    @Override
    public abstract String name();

    protected void register() {
        Register(PacketKick.class);
        Register(PacketHandShake.class);

        Register(PacketPing.class);
        Register(PacketPong.class);

        Register(PacketStatusRequest.class);
        Register(PacketStatusResponse.class);
    }

    @SafeVarargs
    protected final void Register(Class<? extends Packet>... packets) {
        for (Class<? extends Packet> packet : packets) Register(packet, null);
    }

    protected <P extends Packet> void Register(Class<P> packet, Integer id) {
        PacketInfo info = packet.getAnnotation(PacketInfo.class);
        if (info == null) warn("No annotation found for packet " + packet.getSimpleName());
        else {
            try {
                Codec<P> codec = ((Codec<P>) info.codec().newInstance());
                Target[] targets = info.target();

                add(info.state(), id == null ? info.id() : id, packet, codec, targets[0]);
                if (targets.length > 1) add(info.state(), id == null ? info.id() : id, packet, codec, targets[1]);
            } catch (Exception e) {
                error("Failed to register packet " + packet.getSimpleName() + " " + e.getMessage());
            }
        }
    }

    private <P extends Packet> void add(State state, int id, Class<P> packet, Codec<P> codec, Target target) {
        (target == Target.Client ? ClientBoundPackets : ServerBoundPackets).computeIfAbsent(state, PacketRegistry::new).register(id, packet);
        (target == Target.Client ? ClientBoundCodecs : ServerBoundCodecs).computeIfAbsent(state, CodecRegistry::new).register(id, codec);
    }

    public PacketRegistry serverBoundPackets(State state) {
        return ServerBoundPackets.get(state);
    }

    public PacketRegistry clientBoundPackets(State state) {
        return ClientBoundPackets.get(state);
    }

    public CodecRegistry serverBoundCodecs(State state) {
        return ServerBoundCodecs.get(state);
    }

    public CodecRegistry clientBoundCodecs(State state) {
        return ClientBoundCodecs.get(state);
    }
}
