package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayerLook;

@PacketInfo(target = Target.Server, state = State.Play, id = 5, codec = CodecPlayerLook.class)
public class PacketPlayerLook extends Packet {

    private float yaw, pitch;
    private boolean onGround;

    public PacketPlayerLook(float yaw, float pitch, boolean onGround) {
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    public PacketPlayerLook() {}


    public float yaw() {
        return yaw;
    }

    public PacketPlayerLook Yaw(float yaw) {
        this.yaw = yaw;
        return this;
    }

    public float pitch() {
        return pitch;
    }

    public PacketPlayerLook Pitch(float pitch) {
        this.pitch = pitch;
        return this;
    }

    public boolean onGround() {
        return onGround;
    }

    public PacketPlayerLook OnGround(boolean onGround) {
        this.onGround = onGround;
        return this;
    }

    public Quaternion rotation() {
        return Quaternion.fromEuler(pitch, yaw, 0);
    }
    
    
}
