package com.sxt.mc2.event.base;

import com.sxt.mc.event.subscribe.SubscribedMethod;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.lang.reflect.Method;

public final class EventMethod extends SubscribedMethod<EventMethod> {

    private EventPriority priority;

    public EventMethod(Method method, EventPriority priority) {
        super(method);
        this.priority = priority;
    }

    public EventPriority priority() {
        return priority;
    }

    public String eventName() {
        return method().getParameterTypes()[0].getSimpleName();
    }

    @Override
    public int compareTo(EventMethod o) {
        int event = eventName().compareTo(o.eventName()), method = method().getName().compareTo(o.method().getName()), priority = priority().compareTo(o.priority());
        return event == 0 ? priority == 0 ? method : priority : event;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        EventMethod that = (EventMethod) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(priority, that.priority)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(priority)
                .toHashCode();
    }
}
