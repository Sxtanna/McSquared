package com.sxt.mc.base;

public enum Gamemode {

    Survival,
    Creative,
    Adventure,
    Spectator;

    public int id() {
        return ordinal();
    }

    public static Gamemode fromId(int id) {
        if (id < 0 || id >= values().length) return null;
        return values()[id];
    }
}
