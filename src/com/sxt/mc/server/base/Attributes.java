package com.sxt.mc.server.base;

import java.util.HashMap;
import java.util.Map;

public class Attributes {

    private final Map<AttributeKey<?>, Object> attributes = new HashMap<>();

    @SuppressWarnings("unchecked")
    public <O> O get(AttributeKey<O> key) {
        return ((O) attributes.get(key));
    }

    public <O> Attributes set(AttributeKey<O> key, O value) {
        attributes.put(key, value);
        return this;
    }
}
