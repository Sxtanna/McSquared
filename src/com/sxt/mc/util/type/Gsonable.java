package com.sxt.mc.util.type;

import com.sxt.mc2.McSquared;

public interface Gsonable<T extends Gsonable> {

    Class<? extends T> type();

    default String to() {
        return McSquared.Gson().toJson(this, type());
    }

    static <T extends Gsonable> T from(String string, Class<T> typeClass) {
        return McSquared.Gson().fromJson(string, typeClass);
    }

}
