package com.sxt.mc.network.pipe;

import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.session.Session;
import com.sxt.mc.session.SessionServer;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.event.types.packet.PacketInEvent;
import com.sxt.mc2.event.types.player.PlayerLogoutEvent;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

@ChannelHandler.Sharable
public class Forwarder extends SimpleChannelInboundHandler<Packet> implements Logging {

    private final SessionServer Sessions;

    public Forwarder(SessionServer sessions) {
        Sessions = sessions;
    }

    public SessionServer Sessions() {
        return Sessions;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet msg) throws Exception {
        Session session = Sessions.get(ctx);

        PacketInEvent packetInEvent = McSquared.Events().call(new PacketInEvent(msg.Session(session)));
        if (packetInEvent.cancelled()) return;

        McSquared.Server().Packets().call(msg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Session session = Sessions.get(ctx);

        if (session == null) return;
        McSquared.Events().call(new PlayerLogoutEvent(session, session.disconnectReason()));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        error("Pipeline Error " + cause.getMessage());
        cause.printStackTrace();
    }
}
