package com.sxt.mc2.protocol.codec.status.ping;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc2.protocol.packet.status.ping.PacketPong;

public class CodecPong implements Codec<PacketPong> {

    @Override
    public void encode(Buffer buffer, PacketPong packet) {
        buffer.writeLong(packet.time());
    }

    @Override
    public void decode(Buffer buffer, PacketPong packet) {
        packet.Time(buffer.readLong());
    }
}
