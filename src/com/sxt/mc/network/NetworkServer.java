package com.sxt.mc.network;

import com.sxt.mc.network.pipe.McChannel;
import com.sxt.mc.util.type.Logging;

import java.net.InetSocketAddress;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NetworkServer implements Logging {

    private final McChannel McChannel;

    private final ServerBootstrap BootStrap = new ServerBootstrap();
    private final EventLoopGroup Parent = new NioEventLoopGroup(), Child = new NioEventLoopGroup();

    public NetworkServer(McChannel mcChannel) {
        McChannel = mcChannel;
    }

    @Override
    public String name() {
        return "NetServer";
    }

    public void setup() {
        BootStrap.group(Parent, Child)
                .channel(NioServerSocketChannel.class)
                .childHandler(McChannel)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
    }

    public ChannelFuture bind(int port) {
        return bind(new InetSocketAddress(port));
    }

    public void shutdown() {
        Parent.shutdownGracefully();
    }

    private ChannelFuture bind(InetSocketAddress address) {
        return BootStrap.bind(address).addListener(future -> {
            if (future.isSuccess()) successfulBind(address);
            else failedBind(address, future.cause());
        });
    }

    private void successfulBind(InetSocketAddress address) {
        info("Successfully bound to port " + address.getPort());
    }

    private void failedBind(InetSocketAddress address, Throwable cause) {
        error("Failed to bind to port " + address.getPort() + " " + cause.getMessage());
    }
}
