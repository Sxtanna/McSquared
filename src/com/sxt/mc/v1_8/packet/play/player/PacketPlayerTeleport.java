package com.sxt.mc.v1_8.packet.play.player;
import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.util.math.Quaternion;
import com.sxt.mc.util.math.Vector;
import com.sxt.mc.v1_8.codec.play.player.CodecPlayerTeleport;

@PacketInfo(target = Target.Client, state = State.Play, id = 8, codec = CodecPlayerTeleport.class)
public class PacketPlayerTeleport extends Packet {

    private double x, y, z, yaw, pitch;
    private int flags;

    public PacketPlayerTeleport(double x, double y, double z, double yaw, double pitch, int flags) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
        this.flags = flags;
    }

    public PacketPlayerTeleport() {}


    public double x() {
        return x;
    }

    public PacketPlayerTeleport X(double x) {
        this.x = x;
        return this;
    }

    public double y() {
        return y;
    }

    public PacketPlayerTeleport Y(double y) {
        this.y = y;
        return this;
    }

    public double z() {
        return z;
    }

    public PacketPlayerTeleport Z(double z) {
        this.z = z;
        return this;
    }

    public double yaw() {
        return yaw;
    }

    public PacketPlayerTeleport Yaw(double yaw) {
        this.yaw = yaw;
        return this;
    }

    public double pitch() {
        return pitch;
    }

    public PacketPlayerTeleport Pitch(double pitch) {
        this.pitch = pitch;
        return this;
    }

    public int flags() {
        return flags;
    }

    public PacketPlayerTeleport Flags(int flags) {
        this.flags = flags;
        return this;
    }

    public Vector position() {
        return new Vector(x(), y(), z());
    }

    public PacketPlayerTeleport Position(double x, double y, double z) {
        return X(x).Y(y).Z(z);
    }

    public PacketPlayerTeleport Position(Vector position) {
        return X(position.x()).Y(position.y()).Z(position.z());
    }

    public PacketPlayerTeleport Rotation(double yaw, double pitch) {
        return Yaw(yaw).Pitch(pitch);
    }

    public PacketPlayerTeleport Rotation(Quaternion rotation) {
        Vector euler = rotation.euler();
        return Rotation(euler.y(), euler.x());
    }
    
    
}
