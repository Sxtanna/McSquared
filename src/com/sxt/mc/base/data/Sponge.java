package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.SpongeType;

public class Sponge extends MaterialData<Sponge, SpongeType> {

    public Sponge(int data) {
        super(Material.Sponge, data);
    }

    public Sponge() {
        super(Material.Sponge);
    }

    public Sponge(SpongeType data) {
        super(Material.Sponge, data);
    }

}
