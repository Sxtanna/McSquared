package com.sxt.mc.util.type;

public interface Builder<B> {

    @SuppressWarnings("unchecked")
    default B thisClass() {
        return ((B) this);
    }

}
