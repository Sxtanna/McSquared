package com.sxt.mc.protocol.base.packet;

import com.sxt.mc.event.subscribe.Subscribable;
import com.sxt.mc.session.Session;

public class Packet implements Subscribable {

    private Session session;

    @Override
    public String name() {
        return getClass().getSimpleName();
    }

    public Session session() {
        return session;
    }

    public Packet Session(Session session) {
        this.session = session;
        return this;
    }
}
