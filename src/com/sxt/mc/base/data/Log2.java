package com.sxt.mc.base.data;

import com.sxt.mc.base.Material;
import com.sxt.mc.base.MaterialData;
import com.sxt.mc.base.data.types.WoodType;
import com.sxt.mc.util.F;

public class Log2 extends MaterialData<Sapling, WoodType> {

    public Log2(int data) {
        super(Material.Log2, F.clamp(data, 0,  1));
    }

    public Log2() {
        super(Material.Log2);
    }

    public Log2(WoodType data) {
        super(Material.Log2, data);
    }

}
