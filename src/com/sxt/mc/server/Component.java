package com.sxt.mc.server;

import com.sxt.mc.server.base.Observer;
import com.sxt.mc.util.type.Builder;
import com.sxt.mc.util.type.Logging;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Component<C extends Component> implements Builder<C>, Logging {

    protected GameObject GameObject;

    public C GameObject(GameObject gameObject) {
        if (GameObject != null) return thisClass();
        GameObject = gameObject;
        return thisClass();
    }

    public void init(){}

    public void update(){}

    public void start(Observer observer) {}

    public void stop(Observer observer) {}

    public void Parent(GameObject parent) {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Component<?> component = (Component<?>) o;

        return new EqualsBuilder()
                .append(GameObject, component.GameObject)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(GameObject)
                .toHashCode();
    }
}
