package com.sxt.mc.network.handlers;

import com.sxt.Main;
import com.sxt.mc.protocol.AdaptingProtocol;
import com.sxt.mc.protocol.Protocol;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.packet.client.PacketHandShake;
import com.sxt.mc.protocol.event.base.PacketWatcher;
import com.sxt.mc.session.Session;
import com.sxt.mc.util.C;
import com.sxt.mc.util.F;
import com.sxt.mc.util.type.Logging;
import com.sxt.mc2.McSquared;
import com.sxt.mc2.protocol.packet.login.PacketKick;
import com.sxt.mc2.protocol.packet.status.PacketStatusResponse;

import java.util.EnumSet;

public class HandShakeHandler implements Logging {

    private static final EnumSet<State> States = EnumSet.of(State.Status, State.Login);

    @PacketWatcher
    public void onHandShake(PacketHandShake packet) {
        Session session = packet.session();

        State state = packet.state();
        if (!States.contains(state)) session.disconnect();
        else {
            session.State(state);
            Protocol protocol = Main.Protocols().find(packet.version());

            if (protocol != null) session.Protocol(protocol);
            else {
                session.Protocol(new AdaptingProtocol(packet.version()));
                session.send(statusPacket(packet.version())).addListener(future -> session.disconnect(kickPacket(packet.version())));
            }
        }
    }

    private String kickUnsupported(boolean serverOld) {
        return F.color(C.Gray + "Unsupported Version " + (serverOld ? C.Red + "Outdated Server" : C.DAqua + "Outdated Client"));
    }

    private PacketKick kickPacket(int version) {
        return new PacketKick(kickUnsupported(version > McSquared.Server().Protocol().version()));
    }

    private PacketStatusResponse statusPacket(int version) {
        boolean serverOld = version > McSquared.Server().Protocol().version();
        return new PacketStatusResponse(PingHandler.currentStatus("Client isn't supported", version + (serverOld ? -1 : 1), kickUnsupported(serverOld)));
    }
}
