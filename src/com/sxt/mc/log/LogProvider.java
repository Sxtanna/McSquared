package com.sxt.mc.log;

import org.slf4j.Logger;
import org.slf4j.impl.SimpleLoggerFactory;

public class LogProvider {

    private static final SimpleLoggerFactory Factory;

    static {
        Factory = new SimpleLoggerFactory();
    }

    public Logger get(String name) {
        return Factory.getLogger(name);
    }

}
