package com.sxt.mc.base.chat;

import com.sxt.mc.base.Color;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseComponent {

    private BaseComponent parent;

    private Color color;
    private Boolean bold, italic, underlined, strikethrough, obfuscated;

    private List<BaseComponent> extra;

    public BaseComponent(BaseComponent old) {
        Color(old.color());
        Bold(old.bold());
        Italic(old.italic());
        Underlined(old.underlined());
        Strikethrough(old.strikethrough());
        Obfuscated(old.obfuscated());
        // TODO: 7/26/2016 Click and Hover Events

        if (old.extra() != null) for (BaseComponent component : old.extra()) Extra(component.copy());
    }

    public BaseComponent() {}

    public abstract BaseComponent copy();

    public Color color() {
        if (color == null) return parent == null ? Color.White : parent.color();
        return color;
    }

    public BaseComponent Color(Color color) {
        this.color = color;
        return this;
    }

    public Boolean bold() {
        if (bold == null) return parent != null && parent.bold();
        return bold;
    }

    public BaseComponent Bold(Boolean bold) {
        this.bold = bold;
        return this;
    }

    public Boolean italic() {
        if (italic == null) return parent != null && parent.italic();
        return italic;
    }

    public BaseComponent Italic(Boolean italic) {
        this.italic = italic;
        return this;
    }

    public Boolean underlined() {
        if (underlined == null) return parent != null && parent.underlined();
        return underlined;
    }

    public BaseComponent Underlined(Boolean underlined) {
        this.underlined = underlined;
        return this;
    }

    public Boolean strikethrough() {
        if (strikethrough == null) return parent != null && parent.strikethrough();
        return strikethrough;
    }

    public BaseComponent Strikethrough(Boolean strikethrough) {
        this.strikethrough = strikethrough;
        return this;
    }

    public Boolean obfuscated() {
        if (obfuscated == null) return parent != null && parent.obfuscated();
        return obfuscated;
    }

    public BaseComponent Obfuscated(Boolean obfuscated) {
        this.obfuscated = obfuscated;
        return this;
    }

    public List<BaseComponent> extra() {
        return extra;
    }

    public BaseComponent Extra(List<BaseComponent> extra) {
        for (BaseComponent component : extra) component.parent = this;
        this.extra = extra;

        return this;
    }

    public BaseComponent Extra(BaseComponent component) {
        if (extra == null) extra = new ArrayList<>();

        component.parent = this;
        extra.add(component);

        return this;
    }

    public BaseComponent Extra(String text) {
        return Extra(new TextComponent(text));
    }
}
