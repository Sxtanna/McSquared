package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum SpongeType implements MaterialTypeData<SpongeType> {

    Dry,
    Wet;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public SpongeType byData(int data) {
        if (data < 0 || data > 1) return Dry;
        return values()[data];
    }
}
