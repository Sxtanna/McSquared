package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityTeleport;

public class CodecEntityTeleport implements Codec<PacketEntityTeleport> {

    @Override
    public void encode(Buffer buffer, PacketEntityTeleport packet) {
        buffer.writeVarInt(packet.entityId());
        buffer.writeFixedPointInt(packet.x());
        buffer.writeFixedPointInt(packet.y());
        buffer.writeFixedPointInt(packet.z());
        buffer.writeAngle((float) packet.yaw());
        buffer.writeAngle((float) packet.pitch());
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketEntityTeleport packet) {
        packet.EntityId(buffer.readVarInt());
        packet.X(buffer.readFixedPointInt());
        packet.Y(buffer.readFixedPointInt());
        packet.Z(buffer.readFixedPointInt());
        packet.Yaw(buffer.readAngle());
        packet.Pitch(buffer.readAngle());
        packet.OnGround(buffer.readBoolean());
    }
}
