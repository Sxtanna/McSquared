package com.sxt.mc.protocol;

import com.sxt.mc.protocol.base.ProtocolRegistry;

public class AdaptingProtocol extends Protocol {

    private final static ProtocolRegistry AdaptedVersions = new ProtocolRegistry();

    private int version;

    public AdaptingProtocol(int version) {
        super();
        this.version = version;
    }

    @Override
    public int version() {
        return version;
    }

    @Override
    public String name() {
        return "Adapt";
    }

    public static Protocol get(int version) {
        Protocol protocol = AdaptedVersions.find(version);
        if (protocol == null)
            AdaptedVersions.register(version, protocol = new AdaptingProtocol(version));

        return protocol;
    }

    public static void clear() {
        AdaptedVersions.release();
    }
}
