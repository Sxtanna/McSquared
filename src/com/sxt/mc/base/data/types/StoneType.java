package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum  StoneType implements MaterialTypeData<StoneType> {

    Plain,
    Granite,
    PolishedGranite,
    Diorite,
    PolishedDiorite,
    Andesite,
    PolishedAndesite;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public StoneType byData(int data) {
        if (data < 0 || data > 6) return Plain;
        return values()[data];
    }
}
