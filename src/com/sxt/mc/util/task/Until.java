package com.sxt.mc.util.task;

public interface Until {

    boolean check();
}
