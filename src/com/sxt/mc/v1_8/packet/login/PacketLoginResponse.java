package com.sxt.mc.v1_8.packet.login;

import com.sxt.mc.protocol.base.PacketInfo;
import com.sxt.mc.protocol.base.State;
import com.sxt.mc.protocol.base.Target;
import com.sxt.mc.protocol.base.packet.Packet;
import com.sxt.mc.v1_8.codec.login.CodecLoginResponse;

import java.util.UUID;

@PacketInfo(target = Target.Client, state = State.Login, id = 2, codec = CodecLoginResponse.class)
public class PacketLoginResponse extends Packet {

    private UUID uuid;
    private String username;

    public PacketLoginResponse(UUID uuid, String username) {
        this.uuid = uuid;
        this.username = username;
    }

    public PacketLoginResponse() {
    }


    public UUID uuid() {
        return uuid;
    }

    public PacketLoginResponse Uuid(UUID uuid) {
        this.uuid = uuid;
        return this;
    }

    public String username() {
        return username;
    }

    public PacketLoginResponse Username(String username) {
        this.username = username;
        return this;
    }
}
