package com.sxt.mc.base.data.types;

import com.sxt.mc.util.type.MaterialTypeData;

public enum MonsterEggsType implements MaterialTypeData<MonsterEggsType> {

    Stone,
    Cobblestone,
    StoneBrick,
    MossyStone,
    CrackedStoneBrick,
    ChiseledStoneBrick;

    @Override
    public int data() {
        return ordinal();
    }

    @Override
    public MonsterEggsType byData(int data) {
        if (data < 0 || data > 5) return Stone;
        return values()[data];
    }
}
