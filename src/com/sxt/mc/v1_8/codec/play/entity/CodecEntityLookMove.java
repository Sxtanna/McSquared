package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityLookMove;

public class CodecEntityLookMove implements Codec<PacketEntityLookMove> {

    @Override
    public void encode(Buffer buffer, PacketEntityLookMove packet) {
        buffer.writeVarInt(packet.entityId());
        buffer.writeFixedPointByte(packet.deltaX());
        buffer.writeFixedPointByte(packet.deltaY());
        buffer.writeFixedPointByte(packet.deltaZ());
        buffer.writeAngle((float) packet.yaw());
        buffer.writeAngle((float) packet.pitch());
        buffer.writeBoolean(packet.onGround());
    }

    @Override
    public void decode(Buffer buffer, PacketEntityLookMove packet) {
        packet.EntityId(buffer.readVarInt());
        packet.DeltaX(buffer.readFixedPointByte());
        packet.DeltaY(buffer.readFixedPointByte());
        packet.DeltaZ(buffer.readFixedPointByte());
        packet.Yaw(buffer.readAngle());
        packet.Pitch(buffer.readAngle());
        packet.OnGround(buffer.readBoolean());
    }
}
