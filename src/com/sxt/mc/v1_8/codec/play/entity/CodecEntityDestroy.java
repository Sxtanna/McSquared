package com.sxt.mc.v1_8.codec.play.entity;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.util.F;
import com.sxt.mc.v1_8.packet.play.entity.PacketEntityDestroy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CodecEntityDestroy implements Codec<PacketEntityDestroy> {

    @Override
    public void encode(Buffer buffer, PacketEntityDestroy packet) {
        buffer.writeVarInt(packet.entityIds().size());
        packet.entityIds().forEach(buffer::writeVarInt);
    }

    @Override
    public void decode(Buffer buffer, PacketEntityDestroy packet) {
        int count = buffer.readVarInt();

        packet.EntityIds(F.mapFrom(buffer, (buf) -> {
            List<Integer> ids = new ArrayList<>(count);
            for (int i = 0; i < count; i++) ids.add(buf.readVarInt());
            return ids.stream();
        }).collect(Collectors.toList()));
    }
}
