package com.sxt.mc.base;

import com.sxt.mc.util.F;

public enum LevelType {

    Default,
    Flat,
    Default_1_1,
    LargeBiomes,
    Amplified,
    Customized;

    public String levelName() {
        return name().toLowerCase();
    }

    public static LevelType byName(String name) {
        for (LevelType type : values()) if (F.equals(type.name(), name)) return type;
        return null;
    }

}
