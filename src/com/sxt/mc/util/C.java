package com.sxt.mc.util;

import java.util.HashMap;
import java.util.Map;

public class C {

    private static final Map<String, String> Custom = new HashMap<>();

    public static final String Bold = "&l";
    public static final String Uline = "&n";
    public static final String Strike = "&m";
    public static final String Magic = "&k";
    public static final String Italic = "&o";
    public static final String Reset = "&r";


    public static final String Black = "&0";
    public static final String DBlue = "&1";
    public static final String DGreen = "&2";
    public static final String DAqua = "&3";
    public static final String DRed = "&4";
    public static final String DPurple = "&5";
    public static final String Gold = "&6";
    public static final String Gray = "&7";
    public static final String DGray = "&8";
    public static final String Blue = "&9";
    public static final String Green = "&a";
    public static final String Aqua = "&b";
    public static final String Red = "&c";
    public static final String Purple = "&d";
    public static final String Yellow = "&e";
    public static final String White = "&f";


    public static final String Value = Yellow;
    public static final String Error = Red;
    public static final String Fatal = DRed + Bold;
    public static final String Msg = Aqua;
    public static final String BGray = DGray + Bold;
    public static final String BWhite = White + Bold;

    public static final String Line = "\n";
    public static final String Empty = "";
    public static final String Space = " ";

    public static void Register(String name, String value) {
        Custom.put(name, value);
    }

    public static void Clear() {
        Custom.clear();
    }

    public static String R(String name) {
        String mapped = Custom.get(name);
        return null == mapped ? Empty : mapped;
    }

    public static String Line(int amount) {
        return F.repeat(Line, amount);
    }

    public static String Space(int amount) {
        return F.repeat(Space, amount);
    }
}
