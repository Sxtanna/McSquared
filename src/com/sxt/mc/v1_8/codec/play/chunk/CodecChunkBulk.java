package com.sxt.mc.v1_8.codec.play.chunk;

import com.sxt.mc.network.base.Buffer;
import com.sxt.mc.protocol.base.codec.Codec;
import com.sxt.mc.v1_8.packet.play.chunk.PacketChunk;
import com.sxt.mc.v1_8.packet.play.chunk.PacketChunkBulk;

import java.util.ArrayList;
import java.util.List;

public class CodecChunkBulk implements Codec<PacketChunkBulk> {

    @Override
    public void encode(Buffer buffer, PacketChunkBulk packet) {
        final List<PacketChunk> chunks = packet.chunks();

        buffer.writeBoolean(packet.skylight());
        buffer.writeVarInt(chunks.size());

        for (PacketChunk chunk : chunks) {
            buffer.writeInt(chunk.x());
            buffer.writeInt(chunk.z());
            buffer.writeShort(chunk.mask());
        }

        for (PacketChunk chunk : chunks) buffer.writeBytes(chunk.data());
    }

    @Override
    public void decode(Buffer buffer, PacketChunkBulk packet) {
        packet.Skylight(buffer.readBoolean());

        int chunksSize = buffer.readVarInt();
        final List<PacketChunk> chunks = new ArrayList<>(chunksSize);

        for (int i = 0; i < chunksSize; i++) {
            PacketChunk chunk = new PacketChunk();

            chunk.X(buffer.readInt());
            chunk.Z(buffer.readInt());
            chunk.Mask(buffer.readUnsignedShort());
            chunk.Continuous(true);

            chunks.add(chunk);
        }

        for (PacketChunk chunk : chunks) {

            int bitmask = chunk.mask();

            int sectionCount = 0;
            for (int i = 0; i < 16; i++) if ((bitmask & (1 << i)) > 0) sectionCount++;

            int byteCount = 0;

            byteCount += 8192 * sectionCount;
            byteCount += 2048 * sectionCount;

            if (packet.skylight()) byteCount += 2048 * sectionCount;

            byteCount += 256;
            byte[] bytes = new byte[byteCount];

            buffer.readBytes(bytes);
            chunk.Data(bytes);
        }

        packet.Chunks(chunks);
    }
}
