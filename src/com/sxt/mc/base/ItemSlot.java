package com.sxt.mc.base;

import com.sxt.mc.network.base.Buffer;

public class ItemSlot {

    private int itemId, count, damage;

    public ItemSlot(int itemId, int count, int damage) {
        this.itemId = itemId;
        this.count = count;
        this.damage = damage;
    }

    public ItemSlot() {}


    public void write(Buffer buffer) {
        buffer.writeShort(itemId);
        buffer.writeByte(count);
        buffer.writeShort(damage);
        buffer.writeByte(0);
    }

    public ItemSlot read(Buffer buffer) {
        itemId = buffer.readShort();
        count = buffer.readByte();
        damage = buffer.readShort();
        buffer.readByte();
        return this;
    }
}
